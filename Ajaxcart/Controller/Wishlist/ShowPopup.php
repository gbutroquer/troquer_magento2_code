<?php
/** @noinspection PhpDeprecationInspection */
/** @noinspection PhpUndefinedMethodInspection */
/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedClassInspection */

namespace Troquer\Ajaxcart\Controller\Wishlist;

use \Exception;
use \Magento\Catalog\Helper\Product;
use \Magento\Catalog\Model\Product\Exception as ProductException;
use \Magento\Checkout\Model\Cart;
use \Magento\Framework\App\Action;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\Result\Redirect;
use \Magento\Framework\Controller\ResultFactory;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Escaper;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Registry;
use \Magento\Wishlist\Controller\AbstractIndex;
use \Magento\Wishlist\Controller\WishlistProviderInterface;
use \Magento\Wishlist\Model\Item;
use \Magento\Wishlist\Model\Item\OptionFactory;
use \Magento\Wishlist\Model\ItemFactory;
use \Magento\Wishlist\Model\LocaleQuantityProcessor;
use \Troquer\Ajaxcart\Helper\Data;

class ShowPopup extends AbstractIndex
{
    /**
     * @var WishlistProviderInterface
     */
    protected WishlistProviderInterface $_wishlistProvider;

    /**
     * @var LocaleQuantityProcessor
     */
    protected LocaleQuantityProcessor $_quantityProcessor;

    /**
     * @var ItemFactory
     */
    protected ItemFactory $_itemFactory;

    /**
     * @var Cart
     */
    protected Cart $_cart;

    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    protected \Magento\Checkout\Helper\Cart $_cartHelper;

    /**
     * @var Product
     */
    protected Product $_productHelper;

    /**
     * @var Escaper
     */
    protected Escaper $_escaper;

    /**
     * @var \Magento\Wishlist\Helper\Data
     */
    protected \Magento\Wishlist\Helper\Data $_helper;

    /**
     * Core registry
     *
     * @var Registry|null
     */
    protected ?Registry $_coreRegistry = null;

    /**
     * @var Data
     */
    protected Data $_ajaxcartData;

    /**
     * @var OptionFactory
     */
    protected OptionFactory $_optionFactory;

    /**
     * @param Action\Context $context
     * @param WishlistProviderInterface $wishlistProvider
     * @param LocaleQuantityProcessor $quantityProcessor
     * @param ItemFactory $itemFactory
     * @param Cart $cart
     * @param OptionFactory $optionFactory
     * @param Product $productHelper
     * @param Escaper $escaper
     * @param \Magento\Wishlist\Helper\Data $helper
     * @param \Magento\Checkout\Helper\Cart $cartHelper
     * @param Registry $registry
     * @param Data $ajaxcartData
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Action\Context $context,
        WishlistProviderInterface $wishlistProvider,
        LocaleQuantityProcessor $quantityProcessor,
        ItemFactory $itemFactory,
        Cart $cart,
        OptionFactory $optionFactory,
        Product $productHelper,
        Escaper $escaper,
        \Magento\Wishlist\Helper\Data $helper,
        \Magento\Checkout\Helper\Cart $cartHelper,
        Registry $registry,
        Data $ajaxcartData
    )
    {
        $this->_wishlistProvider = $wishlistProvider;
        $this->_quantityProcessor = $quantityProcessor;
        $this->_itemFactory = $itemFactory;
        $this->_cart = $cart;
        $this->_optionFactory = $optionFactory;
        $this->_productHelper = $productHelper;
        $this->_escaper = $escaper;
        $this->_helper = $helper;
        $this->_cartHelper = $cartHelper;
        $this->_ajaxcartData = $ajaxcartData;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * Add product to shopping cart from wishlist action
     *
     * @return ResponseInterface|Redirect|ResultInterface|void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();


        try {
            $itemId = (int)$params['item'];

            /** @var Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            /* @var $item Item */
            $item = $this->_itemFactory->create()->load($itemId);
            $product = $item->getProduct();

            if (!empty($params['ajaxcart_error'])) {
                $this->_coreRegistry->register('product', $product);
                $this->_coreRegistry->register('current_product', $product);

                $htmlPopup = $this->_ajaxcartData->getErrorHtml();
                $result['error'] = true;
                $result['html_popup'] = $htmlPopup;
                $result['item'] = $itemId;

                $this->getResponse()->representJson(
                    $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
                );

                return;
            }

            if (!$item->getId()) {
                $resultRedirect->setPath('*/*');
                return $resultRedirect;
            }
            $wishlist = $this->_wishlistProvider->getWishlist($item->getWishlistId());
            if (!$wishlist) {
                $resultRedirect->setPath('*/*');
                return $resultRedirect;
            }


            if (!$product) {
                $resultRedirect->setPath('*/*');
                return $resultRedirect;
            }

            if (!empty($params['ajaxcart_success'])) {
                $item->delete();
                $wishlist->save();

                $this->_coreRegistry->register('product', $product);
                $this->_coreRegistry->register('current_product', $product);
                $htmlPopup = $this->_ajaxcartData->getSuccessHtml();
                $result['success'] = true;
                $result['html_popup'] = $htmlPopup;
                $result['item'] = $itemId;

                $this->getResponse()->representJson(
                    $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
                );

                return;
            }

            /* return options popup content when product type is grouped */
            if ($product->getHasOptions()
                || ($product->getTypeId() == 'grouped' && !isset($params['super_group']))
                || ($product->getTypeId() == 'configurable' && !isset($params['super_attribute']))
                || $product->getTypeId() == 'bundle'
            ) {
                $options = $this->_optionFactory->create()->getCollection()->addItemFilter([$itemId]);
                $item->setOptions($options->getOptionsByItem($itemId));

                $buyRequest = $this->_productHelper->addParamsToBuyRequest(
                    $this->getRequest()->getParams(),
                    ['current_config' => $item->getBuyRequest()]
                );
                $supperAttribute = $item->getBuyRequest()->getData('super_attribute');
                if (!empty($supperAttribute)) {
                    $item->mergeBuyRequest($buyRequest);
                    $item->addToCart($this->_cart, true);
                    $this->_cart->save()->getQuote()->collectTotals();
                    $item->delete();
                    $wishlist->save();

                    $this->_coreRegistry->register('product', $product);
                    $this->_coreRegistry->register('current_product', $product);
                    $htmlPopup = $this->_ajaxcartData->getSuccessHtml();
                    $result['success'] = true;
                    $result['html_popup'] = $htmlPopup;
                    $result['item'] = $itemId;
                    $result['addto'] = true;

                    $this->getResponse()->representJson(
                        $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
                    );
                    return;
                } else {
                    $this->_coreRegistry->register('product', $product);
                    $this->_coreRegistry->register('current_product', $product);

                    $htmlPopup = $this->_ajaxcartData->getOptionsPopupHtml();
                    $result['success'] = true;
                    $result['html_popup'] = $htmlPopup;
                    $result['item'] = $itemId;

                    $this->getResponse()->representJson(
                        $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
                    );

                    return;
                }

            } else {
                $params['product'] = $product->getId();

                $this->getResponse()->representJson(
                    $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($params)
                );

                $this->_forward(
                    'add',
                    'cart',
                    'checkout',
                    $params);

                return;
            }
        } catch (ProductException $e) {
            $this->messageManager->addError(__('This product(s) is out of stock.'));
        } catch (LocalizedException $e) {
            $this->messageManager->addNotice($e->getMessage());
        } catch (Exception $e) {
            $this->messageManager->addException($e, __('We can\'t add the item to the cart right now.'));
        }

        $this->_helper->calculate();
    }
}
