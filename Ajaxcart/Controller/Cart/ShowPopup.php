<?php
/** @noinspection PhpDeprecationInspection */
/** @noinspection PhpUndefinedMethodInspection */
/** @noinspection PhpUnused */

namespace Troquer\Ajaxcart\Controller\Cart;

use \Exception;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Checkout\Controller\Cart\Add;
use \Magento\Checkout\Model\Cart as CustomerCart;
use \Magento\Customer\Model\Session;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Controller\Result\Redirect;
use \Magento\Framework\Data\Form\FormKey\Validator;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Locale\ResolverInterface;
use \Magento\Framework\Registry;
use \Magento\Store\Model\StoreManagerInterface;
use \Troquer\Ajaxcart\Helper\Data as AjaxCartData;
use \Zend_Filter_LocalizedToNormalized;

class ShowPopup extends Add
{
    /**
     * Core registry
     *
     * @var Registry|null
     */
    protected ?Registry $_coreRegistry = null;

    /**
     * @var AjaxCartData Cart Data
     */
    protected AjaxCartData $_ajaxCartData;

    /**
     * @var Session
     */
    protected Session $_customerSession;

    /**
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param StoreManagerInterface $storeManager
     * @param Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param ProductRepositoryInterface $productRepository
     * @param Registry $registry
     * @param AjaxCartData $ajaxcartData
     * @param Session $customerSession
     * @codeCoverageIgnore
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        StoreManagerInterface $storeManager,
        Validator $formKeyValidator,
        CustomerCart $cart,
        ProductRepositoryInterface $productRepository,
        Registry $registry,
        AjaxCartData $ajaxcartData,
        Session $customerSession
    )
    {
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart,
            $productRepository
        );
        $this->_ajaxCartData = $ajaxcartData;
        $this->_coreRegistry = $registry;
        $this->_customerSession = $customerSession;
    }

    /**
     * Add product to shopping cart action
     *
     * @return Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();

        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    [
                        'locale' => $this->_objectManager->get(
                            ResolverInterface::class
                        )->getLocale()
                    ]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();

            /**
             * Check product availability
             */
            if (!$product) {
                return $this->goBack();
            }

            if (!empty($params['ajaxcart_error'])) {
                $this->_coreRegistry->register('product', $product);
                $this->_coreRegistry->register('current_product', $product);

                $htmlPopup = $this->_ajaxCartData->getErrorHtml();
                $result['error'] = true;
                $result['html_popup'] = $htmlPopup;

                return $this->getResponse()->representJson(
                    $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
                );
            }

            if (!empty($params['ajaxcart_success'])) {
                if (!empty($this->_customerSession->getDealQtyOver())) {
                    $this->_coreRegistry->register('product', $product);
                    $this->_coreRegistry->register('current_product', $product);

                    $htmlPopup = $this->_ajaxCartData->getErrorHtml();
                    $result['success'] = true;
                    $result['html_popup'] = $htmlPopup;

                    return $this->getResponse()->representJson(
                        $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
                    );
                } else {
                    $this->_coreRegistry->register('product', $product);
                    $this->_coreRegistry->register('current_product', $product);

                    $htmlPopup = $this->_ajaxCartData->getSuccessHtml();
                    $result['success'] = true;
                    $result['html_popup'] = $htmlPopup;

                    return $this->getResponse()->representJson(
                        $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
                    );
                }

            }

            /* return options popup content when product type is grouped */
            if ($product->getHasOptions()
                || ($product->getTypeId() == 'grouped' && !empty($params['super_group']))
                || ($product->getTypeId() == 'configurable' && !empty($params['super_attribute']))
                || $product->getTypeId() == 'bundle'
                || $product->getTypeId() == 'downloadable'
            ) {
                $this->_coreRegistry->register('product', $product);
                $this->_coreRegistry->register('current_product', $product);

                $htmlPopup = $this->_ajaxCartData->getOptionsPopupHtml();
                $result['success'] = true;
                $result['html_popup'] = $htmlPopup;

                return $this->getResponse()->representJson(
                    $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
                );
            } else {
                $this->_forward('add', 'cart', 'checkout', $params);
            }
        } catch (LocalizedException $e) {
            if ($this->_checkoutSession->getUseNotice(true)) {
                $this->messageManager->addNotice(
                    $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())
                );
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->messageManager->addError(
                        $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($message)
                    );
                }
            }

            $url = $this->_checkoutSession->getRedirectUrl(true);

            if (!$url) {
                $cartUrl = $this->_objectManager->get('Magento\Checkout\Helper\Cart')->getCartUrl();
                $url = $this->_redirect->getRedirectUrl($cartUrl);
            }

            return $this->goBack($url);

        } catch (Exception $e) {
            $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            return $this->goBack();
        }

        return $this->goBack();
    }
}
