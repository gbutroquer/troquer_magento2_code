let config = {
    map: {
        '*': {
            'troquer/swatchRenderer': 'Troquer_Ajaxcart/js/swatch-renderer',
            'sidebar': 'Troquer_Ajaxcart/js/sidebar',
            'troquer/ajaxToCart': 'Troquer_Ajaxcart/js/ajax-to-cart',
            shoppingCart1: 'Troquer_Ajaxcart/js/shopping-cart',
            qtyButton: 'Troquer_Ajaxcart/js/qty-button',
        }
    }
};

