define([
    'jquery',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Customer/js/customer-data',
    'Magento_Ui/js/modal/alert',
    'mage/translate',
    'jquery/ui'
], function ($, getTotalsAction, customerData, alert, $t) {
    'use strict';

    $.widget('troquer.shoppingCart1', {
        /** @inheritdoc */
        _create: function () {
            let items, i, eventFired = 0;
            let self = this;
            $(self.options.emptyCartButton).on('click', $.proxy(function () {
                $(self.options.emptyCartButton).attr('name', 'update_cart_action_temp');
                $(self.options.updateCartActionContainer)
                    .attr('name', 'update_cart_action').attr('value', 'empty_cart');
            }, self));
            items = $.find('[data-role="cart-item-qty"]');

            for (i = 0; i < items.length; i++) {
                $(items[i]).on('keypress', $.proxy(function (event) { //eslint-disable-line no-loop-func
                    let keyCode = event.keyCode ? event.keyCode : event.which;

                    if (keyCode === 13) { //eslint-disable-line eqeqeq
                        $(self.options.emptyCartButton).attr('name', 'update_cart_action_temp');
                        $(self.options.updateCartActionContainer)
                            .attr('name', 'update_cart_action').attr('value', 'update_qty');

                    }
                }, self));
            }
            $(self.options.continueShoppingButton).on('click', $.proxy(function () {
                location.href = self.options.continueShoppingUrl;
            }, self));

            $(window).on("load", function () {
                $('.quantity-controls.quantity-plus').attr('disabled', true);
                $('.quantity-controls.quantity-minus').attr('disabled', true);
            });

            let plus_button = $('.quantity-controls.quantity-plus');
            let minus_button = $('.quantity-controls.quantity-minus');
            plus_button.removeAttr('disabled');
            minus_button.removeAttr('disabled');

            minus_button.on("click", function (e) {
                e.preventDefault();
                let qty = parseInt($(this).parent().find('input[name$="[qty]"]').val()),
                    form = $('form#form-validate');
                if (!qty || qty === 0) qty = 1;
                if (qty > 1) {
                    $(this).parent().find('input[name$="[qty]"]').val(qty - 1);
                }
                //ajax submit
                self._ajaxUpdateHiddenField(form)
                self._ajaxCartUpdate(form);
            });

            plus_button.on("click", function (e) {
                e.preventDefault();
                let qty = parseInt($(this).parent().find('input[name$="[qty]"]').val()),
                    form = $('form#form-validate');
                if (!qty || qty === 0) qty = 1;
                $(this).parent().find('input[name$="[qty]"]').val(qty + 1);
                //ajax submit
                self._ajaxUpdateHiddenField(form)
                self._ajaxCartUpdate(form);
            });

            let qty_input = $('input[name$="[qty]"]');
            qty_input.on("change", function () {
                if (eventFired > 0) {
                    return;
                }
                let form = $('form#form-validate'),
                    qtyOrdered = $(this).attr('data-cart-ordered'),
                    productName = $(this).closest('.cart.item').find('.product-item-name a').html();

                if (self._checkStockState(this)) {
                    self._showPopupErrorQty(productName);
                    $(this).val(qtyOrdered);
                    return;
                }
                self._ajaxUpdateHiddenField(form);
                self._ajaxCartUpdate(form);
            });

            qty_input.on("keyup", function (e) {
                let key = e.keyCode ? e.keyCode : e.which,
                    qtyOrdered = $(this).attr('data-cart-ordered'),
                    productName = $(this).closest('.cart.item').find('.product-item-name a').html(),
                    form = $('form#form-validate');
                eventFired = 0;
                if (49 <= key && key <= 57 || 96 <= key && key <= 105) {
                    if (self._checkStockState(this)) {
                        self._showPopupErrorQty(productName);
                        $(this).val(qtyOrdered);
                        return;
                    }
                    self._ajaxUpdateHiddenField(form);
                    self._ajaxCartUpdate(form);
                    eventFired += 1;
                }
            });

        },

        _ajaxCartUpdate(form) {
            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                showLoader: true,
                success: function (res) {
                    if (res.error === true) {
                        alert({
                            title: 'Hello',
                            content: res.message,
                            actions: {
                                always: function () {
                                }
                            }
                        });

                    } else {
                        let parsedResponse = $.parseHTML(res);
                        let result = $(parsedResponse).find("#form-validate");
                        $("#form-validate").replaceWith(result);

                        customerData.reload(['cart'], true);

                        let deferred = $.Deferred();
                        getTotalsAction([], deferred);
                    }
                },
                error: function (xhr, status, error) {
                    let err = eval("(" + xhr.responseText + ")");
                }

            });
        },

        _ajaxUpdateHiddenField(form){
            let inputSelector = $('#additional_input_data'),
                html = inputSelector.html();
            if (form.length > 0) {
                inputSelector.remove();
                form.append(html);
            }
        },

        _checkStockState(qtySelector){
            let stockState = parseInt($(qtySelector).attr('data-cart-max')),
                currentQty = parseInt($(qtySelector).val());
            return stockState < currentQty;
        },

        _showPopupErrorQty(productName){
            alert({
                content: $t('We don\'t have as many %1 as you requested').replace('%1', productName),
                actions: {
                    always: function () {
                    }
                }
            });
        }

    });

    return $.troquer.shoppingCart1;
});
