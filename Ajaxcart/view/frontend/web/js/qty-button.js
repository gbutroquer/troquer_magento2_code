define([
    'jquery',
    'jquery/ui'
], function ($) {
    'use strict';

    $.widget('troquer.qtyButton', {
        _create: function () {
            this._bind();
        },

        _bind: function () {
            $(window).on("load", function () {
                $('.quantity-controls.quantity-plus').attr('disabled', true);
                $('.quantity-controls.quantity-minus').attr('disabled', true);
            });
            let plus_button = $('.quantity-controls.quantity-plus');
            let minus_button = $('.quantity-controls.quantity-minus');
            plus_button.removeAttr('disabled');
            minus_button.removeAttr('disabled');

            minus_button.on("click", function (e) {
                e.preventDefault();
                let qty = parseInt($(this).parent().find('input[name$="[qty]"]').val());
                if (!qty || qty === 0) qty = 1;
                if (qty > 1) {
                    $(this).parent().find('input[name$="[qty]"]').val(qty - 1);
                }
            });

            plus_button.on("click", function (e) {
                e.preventDefault();
                let qty = parseInt($(this).parent().find('input[name$="[qty]"]').val());
                if (!qty || qty === 0) qty = 1;
                $(this).parent().find('input[name$="[qty]"]').val(qty + 1);
            });
        },
    });
    return $.troquer.qtyButton;
});
