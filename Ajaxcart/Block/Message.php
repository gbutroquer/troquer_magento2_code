<?php

namespace Troquer\Ajaxcart\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Troquer\Ajaxcart\Helper\Data;

class Message extends Template
{
    /**
     * @var Data
     */
    protected Data $_ajaxcartHelper;

    /**
     * Message constructor.
     * @param Context $context
     * @param Data $ajaxcartHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $ajaxcartHelper,
        array $data
    )
    {
        parent::__construct($context, $data);
        $this->_ajaxcartHelper = $ajaxcartHelper;
    }

    /**
     * @return mixed|string
     */
    public function getMessage()
    {
        $message = $this->_ajaxcartHelper->getScopeConfig('ajaxcart/general/message');
        if (!$message) {
            $message = 'You have recently added this product to your Cart';
        }
        return $message;
    }
}
