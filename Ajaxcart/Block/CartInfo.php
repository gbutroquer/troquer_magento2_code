<?php
/** @noinspection PhpUnused */

/** @noinspection PhpDeprecationInspection */

namespace Troquer\Ajaxcart\Block;

use \Magento\Checkout\Model\Cart;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;

class CartInfo extends Template
{
    /**
     * @var Cart
     */
    protected Cart $_cart;

    /**
     * CartInfo constructor.
     * @param Context $context
     * @param Cart $cart
     * @param array $data
     */
    public function __construct(
        Context $context,
        Cart $cart,
        array $data
    )
    {
        parent::__construct($context, $data);
        $this->_cart = $cart;
    }

    /**
     * @return int
     */
    public function getItemsCount()
    {
        return $this->_cart->getItemsCount();
    }

    /**
     * @return float|int
     */
    public function getItemsQty()
    {
        return $this->_cart->getItemsQty();
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->_cart->getQuote()->getSubtotal();
    }
}
