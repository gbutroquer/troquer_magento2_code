<?php
/** @noinspection PhpUnused */

namespace Troquer\Ajaxcart\Block;

use \Magento\Framework\Data\Form\FormKey;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Troquer\Ajaxcart\Helper\Data;

class Js extends Template
{
    /**
     * @var Data
     */
    protected Data $_ajaxcartHelper;

    /**
     * @var FormKey
     */
    protected FormKey $_formKey;

    /**
     * Js constructor.
     * @param Context $context
     * @param Data $ajaxcartHelper
     * @param FormKey $formKey
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $ajaxcartHelper,
        FormKey $formKey,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_ajaxcartHelper = $ajaxcartHelper;
        $this->_formKey = $formKey;
        $this->_template = 'js/main.phtml';
    }

    /**
     * @return string
     */
    public function getAjaxCartInitOptions()
    {
        return $this->_ajaxcartHelper->getAjaxCartInitOptions();
    }

    /**
     * @return string
     */
    public function getAjaxSidebarInitOptions()
    {
        $icon = $this->getViewFileUrl('images/loader-1.gif');
        return $this->_ajaxcartHelper->getAjaxSidebarInitOptions($icon);
    }

}
