<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\Ajaxcart\Block\Product;

use \Magento\Catalog\Block\Product\Context;
use \Magento\Downloadable\Block\Catalog\Product\Links;
use \Magento\Framework\Json\EncoderInterface;
use \Magento\Framework\Pricing\Helper\Data;

class Link extends Links
{
    public function __construct(Context $context, Data $pricingHelper, EncoderInterface $encoder, array $data = [])
    {
        parent::__construct($context, $pricingHelper, $encoder, $data);
    }
}
