<?php
/** @noinspection PhpUnused */

namespace Troquer\Notifications\Block;

use \Exception;
use \Magento\Customer\Api\Data\CustomerInterface;
use \Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Framework\Data\Collection;
use \Magento\Framework\Data\CollectionFactory as InboundProductCollectionFactory;
use \Magento\Framework\DataObject;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Troquer\Inbound\Helper\Data;
use \Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class History extends Template
{
    /**
     * @var CurrentCustomer
     */
    protected CurrentCustomer $_currentCustomer;

    /**
     * @var InboundProductCollectionFactory
     */
    protected InboundProductCollectionFactory $_inboundCollectionFactory;

    /**
     * @var Collection|null
     */
    protected ?Collection $_notifications = null;

    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var TimezoneInterface
     */
    protected TimezoneInterface $_timezone;

    /**
     * @param Context $context
     * @param CurrentCustomer $currentCustomer
     * @param InboundProductCollectionFactory $inboundCollectionFactory
     * @param Data $helper
     * @param TimezoneInterface $timezone
     * @param array $data
     */
    public function __construct(
        Context $context,
        CurrentCustomer $currentCustomer,
        InboundProductCollectionFactory $inboundCollectionFactory,
        Data $helper,
        TimezoneInterface $timezone,
        array $data = []
    )
    {
        $this->_currentCustomer = $currentCustomer;
        $this->_inboundCollectionFactory = $inboundCollectionFactory;
        $this->_helper = $helper;
        $this->_timezone = $timezone;
        parent::__construct($context, $data);
    }

    /**
     * Returns the Magento Customer Model for this block
     *
     * @return CustomerInterface|null
     */
    public function getCustomer(): ?CustomerInterface
    {
        return $this->_currentCustomer->getCustomer();
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Appointments'));
    }

    /**
     * @return History
     */
    protected function _prepareLayout(): History
    {
        return parent::_prepareLayout();
    }

    /**
     * Get Pager child block output
     *
     * @return string
     */
    public function getPagerHtml(): string
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @param $notification
     * @return false|string
     */
    public function getNotificationAge($notification)
    {
        $start = strtotime($notification->getData('created_at'));
        $end = strtotime($this->_timezone->date()->format('Y-m-d H:i:s'));
        $datediff = $end - $start;

        $minutes = round($datediff / (60));

        if ($minutes > 60) {
            $hours = round($datediff / (60 * 60));
            if ($hours > 24) {
                $days = round($datediff / (60 * 60 * 24));
                if ($days > 30) {
                    return date("j M", $start);
                } else {
                    return "Hace " . $days . " d";
                }
            } else {
                return "Hace " . $hours . " h";
            }
        } else {
            return "Hace " . $minutes . " min";
        }
    }

    /**
     * @param $notification
     * @return false|string|null
     */
    public function getTimeGroup($notification)
    {
        $start = strtotime($notification->getData('created_at'));
        $end = strtotime($this->_timezone->date()->format('Y-m-d H:i:s'));

        if (date('m-Y', $start) == date('m-Y', $end)) {
            return null;
        } else {
            return date('F Y', $start);
        }
    }

    /**
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getNotifications(): ?Collection
    {
        $this->_notifications = $this->_inboundCollectionFactory->create();
        $requestData = [
            "email" => $this->getCustomer()->getEmail()
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/notifications/get");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);

        $notifications = json_decode($result);
        foreach (array_reverse($notifications[1]) as $notification) {
            $varienObject = new DataObject();
            $varienObject->addData((array)$notification);
            try {
                $this->_notifications->addItem($varienObject);
            } catch (Exception $e) {
            }
        }
        return $this->_notifications;
    }
}
