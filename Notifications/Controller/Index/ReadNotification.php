<?php

namespace Troquer\Notifications\Controller\Index;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Framework\App\Action\HttpPostActionInterface;
use \Magento\Framework\App\Http\Context as HttpContext;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\Result\Redirect;
use \Magento\Framework\Controller\Result\RedirectFactory;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\View\Result\Page;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Webapi\Rest\Request;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Framework\Serialize\Serializer\Json;
use \Troquer\Inbound\Helper\Data;
use \Zend_Http_Client;
use \Magento\Customer\Model\Context;
use \Troquer\Sales\Block\Sale\SelfServe;

class ReadNotification implements HttpPostActionInterface
{
    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var PageFactory
     */
    protected PageFactory $_resultPageFactory;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * @var Zend_Http_Client
     */
    protected Zend_Http_Client $_zendClient;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var SelfServe
     */
    protected SelfServe $_selfServe;

    /**
     * @var HttpContext
     */
    protected HttpContext $_httpContext;

    /**
     * @var RedirectFactory
     */
    private RedirectFactory $_redirectFactory;

    /**
     * @var CurrentCustomer
     */
    protected CurrentCustomer $_currentCustomer;

    /**
     * @param Request $request
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encrytor
     * @param Json $json
     * @param Zend_Http_Client $zendClient
     * @param PageFactory $resultPageFactory
     * @param Data $helper
     * @param SelfServe $selfServe
     * @param HttpContext $httpContext
     * @param CurrentCustomer $currentCustomer
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Request $request,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encrytor,
        Json $json,
        Zend_Http_Client $zendClient,
        PageFactory $resultPageFactory,
        Data $helper,
        SelfServe $selfServe,
        HttpContext $httpContext,
        CurrentCustomer $currentCustomer,
        RedirectFactory $redirectFactory
    )
    {
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $encrytor;
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_selfServe = $selfServe;
        $this->_helper = $helper;
        $this->_httpContext = $httpContext;
        $this->_currentCustomer = $currentCustomer;
        $this->_redirectFactory = $redirectFactory;
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface|Page
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        if (!(bool)$this->_httpContext->getValue(Context::CONTEXT_AUTH)) {
            $resultPage = $this->_redirectFactory->create();
            $resultPage->setPath('customer/account');
        } else {
            $this->deleteNotification($this->_request->getParams());
            $resultPage = $this->_resultPageFactory->create();
        }

        return $resultPage;
    }

    /**
     * Retrieve the Customer Data using the customer Id from the customer session.
     *
     * @return CustomerInterface
     */
    protected function getCustomer()
    {
        return $this->_currentCustomer->getCustomer();
    }

    /**
     * @param $notification
     * @return mixed
     * @throws NoSuchEntityException
     */
    protected function deleteNotification($notification)
    {
        $requestData = [
            "notification_id" => $notification["notification_id"],
            "email" => $this->getCustomer()->getEmail()
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/notification/read");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }
}
