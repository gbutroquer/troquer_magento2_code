<?php
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUndefinedMethodInspection */
/** @noinspection PhpUnused */

namespace Troquer\Notifications\Controller\Index;

use \Magento\Customer\Model\Customer;
use \Magento\Framework\App\Action\HttpPostActionInterface;
use \Magento\Framework\App\Http\Context as HttpContext;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\Result\Raw;
use \Magento\Framework\Controller\Result\RawFactory;
use \Magento\Framework\Controller\Result\Redirect;
use \Magento\Framework\Controller\Result\RedirectFactory;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\View\Result\PageFactory;
use \Troquer\Inbound\Helper\Data;
use \Magento\Customer\Model\Context;
use \Magento\Customer\Model\SessionFactory as SessionFactory;

class GetUnreadNotifications implements HttpPostActionInterface
{
    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var PageFactory
     */
    protected PageFactory $_resultPageFactory;

    /**
     * @var HttpContext
     */
    protected HttpContext $_httpContext;

    /**
     * @var RedirectFactory
     */
    private RedirectFactory $_redirectFactory;

    /**
     * @var RawFactory
     */
    protected RawFactory $_resultRawFactory;

    /**
     * @var SessionFactory
     */
    protected SessionFactory $_sessionFactory;

    /**
     * @param PageFactory $resultPageFactory
     * @param Data $helper
     * @param HttpContext $httpContext
     * @param RawFactory $resultRawFactory
     * @param SessionFactory $sessionFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        PageFactory $resultPageFactory,
        Data $helper,
        HttpContext $httpContext,
        RawFactory $resultRawFactory,
        SessionFactory $sessionFactory,
        RedirectFactory $redirectFactory
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_helper = $helper;
        $this->_httpContext = $httpContext;
        $this->_resultRawFactory = $resultRawFactory;
        $this->_sessionFactory = $sessionFactory;
        $this->_redirectFactory = $redirectFactory;
    }

    /**
     * @return ResponseInterface|Raw|Redirect|ResultInterface
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        if (!(bool)$this->_httpContext->getValue(Context::CONTEXT_AUTH)) {
            $resultPage = $this->_redirectFactory->create();
            $resultPage->setPath('customer/account');
            return $resultPage;
        } else {
            $result = $this->_resultRawFactory->create();
            return $result->setContents($this->getUnreadNotifications());
        }
    }

    /**
     * Returns the Magento Customer Model for this block
     *
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->_sessionFactory->create()->getCustomer();
    }

    /**
     * @return mixed
     * @throws NoSuchEntityException
     */
    protected function getUnreadNotifications()
    {
        $requestData = [
            "email" => $this->getCustomer()->getEmail()
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/notifications/unread");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        return curl_exec($ch);
    }
}
