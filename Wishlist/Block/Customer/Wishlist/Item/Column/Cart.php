<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\Wishlist\Block\Customer\Wishlist\Item\Column;

use \Magento\Catalog\Block\Product\View;
use \Magento\Catalog\Model\Product\Image\UrlBuilder;
use \Magento\Framework\App\Http\Context;
use \Magento\Framework\Exception\InputException;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\View\ConfigInterface;
use \Magento\InventorySalesApi\Api\Data\SalesChannelInterface;
use \Magento\InventorySalesApi\Api\StockResolverInterface;
use \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use \Magento\Store\Model\StoreManagerInterface;

class Cart extends \Magento\Wishlist\Block\Customer\Wishlist\Item\Column\Cart
{
    /**
     * @var GetProductSalableQtyInterface
     */
    private GetProductSalableQtyInterface $_productSalableQty;

    /**
     * @var StockResolverInterface
     */
    private StockResolverInterface $_stockResolver;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param Context $httpContext
     * @param StoreManagerInterface $storeManager
     * @param GetProductSalableQtyInterface $productSalableQty
     * @param StockResolverInterface $stockResolver
     * @param array $data
     * @param ConfigInterface|null $config
     * @param UrlBuilder|null $urlBuilder
     * @param View|null $productView
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        Context $httpContext,
        StoreManagerInterface $storeManager,
        GetProductSalableQtyInterface $productSalableQty,
        StockResolverInterface $stockResolver,
        array $data = [],
        ?ConfigInterface $config = null,
        ?UrlBuilder $urlBuilder = null,
        ?View $productView = null
    )
    {
        $this->_storeManager = $storeManager;
        $this->_productSalableQty = $productSalableQty;
        $this->_stockResolver = $stockResolver;
        parent::__construct(
            $context,
            $httpContext,
            $data,
            $config,
            $urlBuilder,
            $productView
        );
    }

    /**
     * @param $sku
     * @return mixed
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getProductStock($sku)
    {
        $stockId = $this->_stockResolver->execute(SalesChannelInterface::TYPE_WEBSITE, $this->getWebsiteCode())->getStockId();
        return $this->_productSalableQty->execute($sku, $stockId);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getWebsiteCode()
    {
        return $this->_storeManager->getWebsite()->getCode();
    }
}
