<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Ajaxsuite\Model\Config\Source;

use \Magento\Framework\Option\ArrayInterface;

class Animation implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'fade', 'label' => __('Fade In')],
            ['value' => 'slide_top', 'label' => __('Slide from Top')],
            ['value' => 'slide_bottom', 'label' => __('Slide from Bottom')],
            ['value' => 'slide_left', 'label' => __('Slide from Left')],
            ['value' => 'slide_right', 'label' => __('Slide from Right')]
        ];
    }
}
