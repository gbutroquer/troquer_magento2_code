<?php
/** @noinspection PhpUnused */

namespace Troquer\Ajaxsuite\Block;

use \Magento\Framework\Data\Form\FormKey;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Troquer\Ajaxsuite\Helper\Data;

class Js extends Template
{
    /**
     * @var Data
     */
    protected Data $_ajaxsuiteHelper;

    /**
     * @var FormKey
     */
    protected FormKey $_formKey;

    /**
     * Js constructor.
     * @param Context $context
     * @param FormKey $formKey
     * @param Data $ajaxsuiteHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        FormKey $formKey,
        Data $ajaxsuiteHelper,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_formKey = $formKey;
        $this->_ajaxsuiteHelper = $ajaxsuiteHelper;
        $this->_template = 'js/main.phtml';
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getFormKey()
    {
        return $this->_formKey->getFormKey();
    }

    /**
     * @return string
     */
    public function getAjaxLoginUrl()
    {
        return $this->getUrl('ajaxsuite/login');
    }

    /**
     * @return string
     */
    public function getAjaxWishlistUrl()
    {
        return $this->getUrl('ajaxsuite/wishlist');
    }

    /**
     * @return string
     */
    public function getAjaxCompareUrl()
    {
        return $this->getUrl('ajaxsuite/compare');
    }

    /**
     * @return string
     */
    public function getAjaxSuiteInitOptions()
    {
        return $this->_ajaxsuiteHelper->getAjaxSuiteInitOptions();
    }
}
