<?php

namespace Troquer\Ajaxsuite\Block\Popup;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;

class Wrapper extends Template
{
    /**
     * Wrapper constructor.
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }
}
