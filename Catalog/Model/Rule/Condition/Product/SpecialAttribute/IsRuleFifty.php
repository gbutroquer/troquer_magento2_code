<?php
/** @noinspection PhpUnused */

namespace Troquer\Catalog\Model\Rule\Condition\Product\SpecialAttribute;

use \DateInterval;
use \Magento\Config\Model\Config\Source\Yesno;
use \Magento\Framework\Phrase;
use \Magento\Framework\Stdlib\DateTime;
use \Smile\ElasticsuiteCatalogRule\Api\Rule\Condition\Product\SpecialAttributeInterface;
use \Smile\ElasticsuiteCore\Search\Request\Query\QueryFactory;
use \Smile\ElasticsuiteCore\Search\Request\QueryInterface;

class IsRuleFifty implements SpecialAttributeInterface
{
    /**
     * @var QueryFactory
     */
    protected QueryFactory $_queryFactory;

    /**
     * @var Yesno
     */
    protected Yesno $_booleanSource;

    /**
     * Image constructor.
     *
     * @param QueryFactory $queryFactory Query Factory
     * @param Yesno $booleanSource Boolean Source
     */
    public function __construct(QueryFactory $queryFactory, Yesno $booleanSource)
    {
        $this->_queryFactory = $queryFactory;
        $this->_booleanSource = $booleanSource;
    }

    /**
     * @return string
     */
    public function getAttributeCode()
    {
        return 'is_rule_fifty';
    }

    /**
     * @return QueryInterface
     */
    public function getSearchQuery()
    {
        $day = (new \DateTime())->format(  'd');
        if ($day < 10) {
            $date = (new \DateTime())->sub(new DateInterval('P1M'));
        } else {
            $date = (new \DateTime());
        }
        $month = $date->format(  'm');
        $year = $date->format(  'Y');
        $date1 = (new \DateTime())->setDate($year, $month, 15)->setTime(23, 59, 59);
        $date3 = (new \DateTime())->setDate($year, $month, 15)->setTime(23, 59, 59);

        $fromBA = $date1->sub(new DateInterval('P7M'))->format(DateTime::DATETIME_PHP_FORMAT);
        $fromCS = $date3->sub(new DateInterval('P9M'))->format(DateTime::DATETIME_PHP_FORMAT);
        $clauses = [];

        $bounds = array_filter(['gte' => 2250, 'lt' => 20000]);
        $priceCondition = $this->_queryFactory->create(
            QueryInterface::TYPE_NESTED,
            [
                'path'  => 'price',
                'query' => $this->_queryFactory->create(
                    QueryInterface::TYPE_BOOL,
                    [
                        'must' => [
                            $this->_queryFactory->create(
                                QueryInterface::TYPE_RANGE,
                                ['field' => 'price.max_price', 'bounds' => $bounds]
                            ),
                        ],
                    ]
                ),
            ]
        );

        $publicationDateToBA = $this->_queryFactory->create(
            QueryInterface::TYPE_RANGE,
            ['field' => 'publication_date', 'bounds' => ['lte' => $fromBA]]
        );

        $bagOrAccesory = $this->_queryFactory->create(
            QueryInterface::TYPE_TERMS,
            ['field' => 'attribute_set_id', 'values' => [10, 11, 12, 13, 14, 15]]
        );

        $publicationDateToCS = $this->_queryFactory->create(
            QueryInterface::TYPE_RANGE,
            ['field' => 'publication_date', 'bounds' => ['lte' => $fromCS]]
        );

        $clotheOrShoe = $this->_queryFactory->create(
            QueryInterface::TYPE_TERMS,
            ['field' => 'attribute_set_id', 'values' => [16, 17, 18, 19, 20, 4]]
        );

        $existsPublicationDate = $this->_queryFactory->create(QueryInterface::TYPE_EXISTS, ['field' => 'publication_date']);

        $clauses[] = $this->_queryFactory->create(
            QueryInterface::TYPE_BOOL,
            ['must' => [$priceCondition, $existsPublicationDate, $publicationDateToBA, $bagOrAccesory]]
        );

        $clauses[] = $this->_queryFactory->create(
            QueryInterface::TYPE_BOOL,
            ['must' => [$priceCondition, $existsPublicationDate, $publicationDateToCS, $clotheOrShoe]]
        );

        return $this->_queryFactory->create(QueryInterface::TYPE_BOOL, ['should' => $clauses]);
    }

    /**
     * @return string
     */
    public function getOperatorName()
    {
        return ' ';
    }

    /**
     * @return string
     */
    public function getInputType()
    {
        return 'select';
    }

    /**
     * @return string
     */
    public function getValueElementType()
    {
        return 'hidden';
    }

    /**
     * @return string
     */
    public function getValueName()
    {
        return ' ';
    }

    /**
     * @return int|mixed
     */
    public function getValue()
    {
        return 1;
    }

    /**
     * @return array|array[]
     */
    public function getValueOptions()
    {
        return $this->_booleanSource->toOptionArray();
    }

    /**
     * @return Phrase|string
     */
    public function getLabel()
    {
        return __('Only 50% disccount products');
    }
}
