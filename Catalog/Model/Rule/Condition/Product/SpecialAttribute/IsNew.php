<?php
/** @noinspection PhpUnused */

namespace Troquer\Catalog\Model\Rule\Condition\Product\SpecialAttribute;

use \Magento\Config\Model\Config\Source\Yesno;
use \Magento\Framework\Phrase;
use \Magento\Framework\Stdlib\DateTime;
use \Smile\ElasticsuiteCatalogRule\Api\Rule\Condition\Product\SpecialAttributeInterface;
use \Smile\ElasticsuiteCore\Search\Request\Query\QueryFactory;
use \Smile\ElasticsuiteCore\Search\Request\QueryInterface;
use \DateInterval;

class IsNew implements SpecialAttributeInterface
{
    /**
     * @var QueryFactory
     */
    protected QueryFactory $_queryFactory;

    /**
     * @var Yesno
     */
    protected Yesno $_booleanSource;

    /**
     * Image constructor.
     *
     * @param QueryFactory $queryFactory Query Factory
     * @param Yesno $booleanSource Boolean Source
     */
    public function __construct(QueryFactory $queryFactory, Yesno $booleanSource)
    {
        $this->_queryFactory = $queryFactory;
        $this->_booleanSource = $booleanSource;
    }

    /**
     * @return string
     */
    public function getAttributeCode()
    {
        return 'is_product_new';
    }

    /**
     * @return QueryInterface
     */
    public function getSearchQuery()
    {
        $now = (new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT);
        $before = (new \DateTime())->sub(new DateInterval('P15D'))->format(DateTime::DATETIME_PHP_FORMAT);

        $clauses = [];

        $newFromDateEarlier = $this->_queryFactory->create(
            QueryInterface::TYPE_RANGE,
            ['field' => 'publication_date', 'bounds' => ['lte' => $now]]
        );

        $newsToDateLater = $this->_queryFactory->create(
            QueryInterface::TYPE_RANGE,
            ['field' => 'publication_date', 'bounds' => ['gte' => $before]]
        );

        $missingNewsFromDate = $this->_queryFactory->create(QueryInterface::TYPE_MISSING, ['field' => 'publication_date']);

        $clauses[] = $this->_queryFactory->create(
            QueryInterface::TYPE_BOOL,
            ['must' => [$newFromDateEarlier, $missingNewsFromDate]]
        );

        $clauses[] = $this->_queryFactory->create(
            QueryInterface::TYPE_BOOL,
            ['must' => [$missingNewsFromDate, $newsToDateLater]]
        );

        $clauses[] = $this->_queryFactory->create(
            QueryInterface::TYPE_BOOL,
            ['must' => [$newFromDateEarlier, $newsToDateLater]]
        );

        return $this->_queryFactory->create(QueryInterface::TYPE_BOOL, ['should' => $clauses]);
    }

    /**
     * @return string
     */
    public function getOperatorName()
    {
        return ' ';
    }

    /**
     * @return string
     */
    public function getInputType()
    {
        return 'select';
    }

    /**
     * @return string
     */
    public function getValueElementType()
    {
        return 'hidden';
    }

    /**
     * @return string
     */
    public function getValueName()
    {
        return ' ';
    }

    /**
     * @return int|mixed
     */
    public function getValue()
    {
        return 1;
    }

    /**
     * @return array|array[]
     */
    public function getValueOptions()
    {
        return $this->_booleanSource->toOptionArray();
    }

    /**
     * @return Phrase|string
     */
    public function getLabel()
    {
        return __('Only new products');
    }
}
