<?php
/** @noinspection PhpDeprecationInspection */
/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedMethodInspection */

namespace Troquer\Catalog\Block\Home;

use \Magento\Catalog\Api\CategoryRepositoryInterface;
use \Magento\Catalog\Block\Product\ListProduct;
use \Magento\Catalog\Model\Layer\Resolver;
use \Magento\Catalog\Model\ResourceModel\Product\Collection;
use \Magento\Eav\Model\Entity\Collection\AbstractCollection;
use \Magento\Framework\App\ResourceConnection;
use \Magento\Framework\Data\Helper\PostHelper;
use \Magento\Framework\Exception\InputException;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Stdlib\DateTime\DateTime;
use \Magento\Framework\Url\Helper\Data;
use \Magento\Catalog\Block\Product\Context;
use \Magento\InventorySalesApi\Api\Data\SalesChannelInterface;
use \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use \Magento\InventorySalesApi\Api\StockResolverInterface;
use \Magento\Store\Model\ScopeInterface;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use \Magento\Catalog\Model\CategoryFactory;

class LatestList extends ListProduct
{
    /**
     * @var ResourceConnection
     */
    protected ResourceConnection $_resource;

    /**
     * @var Collection
     */
    protected Collection $_collection;

    /**
     * @var GetProductSalableQtyInterface
     */
    protected GetProductSalableQtyInterface $_productSalableQty;

    /**
     * @var StockResolverInterface
     */
    protected StockResolverInterface $_stockResolver;

    /**
     * @var Resolver
     */
    protected Resolver $_layerResolver;

    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $_collectionFactory;

    /**
     * @var CategoryFactory
     */
    protected CategoryFactory $_categoryFactory;

    /**
     * @var DateTime
     */
    protected DateTime $_date;

    /**
     * LatestList constructor.
     * @param Context $context
     * @param PostHelper $postDataHelper
     * @param Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Data $urlHelper
     * @param Collection $collection
     * @param ResourceConnection $resource
     * @param StoreManagerInterface $storeManager
     * @param GetProductSalableQtyInterface $productSalableQty
     * @param StockResolverInterface $stockResolver
     * @param CollectionFactory $collectionFactory
     * @param CategoryFactory $categoryFactory
     * @param DateTime $date
     * @param array $data
     */
    public function __construct(
        Context $context,
        PostHelper $postDataHelper,
        Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data $urlHelper,
        Collection $collection,
        ResourceConnection $resource,
        StoreManagerInterface $storeManager,
        GetProductSalableQtyInterface $productSalableQty,
        StockResolverInterface $stockResolver,
        CollectionFactory $collectionFactory,
        CategoryFactory $categoryFactory,
        DateTime $date,
        array $data = []
    )
    {
        $this->_collection = $collection;
        $this->_resource = $resource;
        $this->_storeManager = $storeManager;
        $this->_productSalableQty = $productSalableQty;
        $this->_stockResolver = $stockResolver;
        $this->_layerResolver = $layerResolver;
        $this->_collectionFactory = $collectionFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_date = $date;

        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);
    }

    /**
     * @return Collection|AbstractCollection
     * @throws NoSuchEntityException
     */
    protected function _getProductCollection()
    {
        if ($this->_productCollection === null) {
            $this->_productCollection = $this->initializeProductCollection();
        }

        return $this->_productCollection;
    }

    /**
     * @return Collection|AbstractCollection
     * @throws NoSuchEntityException
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();
    }

    /**
     * @return Collection
     * @throws NoSuchEntityException
     */
    protected function initializeProductCollection()
    {
        $count = $this->getProductCount();
        $category_id = $this->getData("category_id");

        if (!$category_id) {
            $category_id = $this->_storeManager->getStore()->getRootCategoryId();
        }

        $category = $this->_categoryFactory->create()->load($category_id);
        $collection = $this->_collectionFactory->create();
        if ($category->getIsVirtualCategory() && $category->getVirtualRule()) {
            $queryFilter = $category->getVirtualRule()->getCategorySearchQuery($category);
            $collection->addQueryFilter($queryFilter);
        } else {
            $collection->addCategoryFilter($category);
        }

        $collection->addAttributeToSelect('*')
            ->setPageSize($count)
            ->setCurPage(1)
            ->addAttributeToSort('position', 'asc');

        return $collection;
    }

    /**
     * @return array|int|mixed
     */
    public function getProductCount()
    {
        $limit = $this->getData("product_count");
        if (!$limit)
            $limit = 10;
        return $limit;
    }

    /**
     * @param $sku
     * @return int
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getProductStock($sku)
    {
        $stockId = $this->_stockResolver->execute(SalesChannelInterface::TYPE_WEBSITE, $this->getWebsiteCode())->getStockId();
        return (int)$this->_productSalableQty->execute($sku, $stockId);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getWebsiteCode()
    {
        return $this->_storeManager->getWebsite()->getCode();
    }

    /**
     * @return array|null
     */
    public function getTagData()
    {
        $result = [
            "start_date" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/start_date", ScopeInterface::SCOPE_STORE),
            "end_date" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/end_date", ScopeInterface::SCOPE_STORE),
            "tag_text" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/tag_text", ScopeInterface::SCOPE_STORE),
            "tag_text_color" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/tag_text_color", ScopeInterface::SCOPE_STORE),
            "tag_background_color" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/tag_background_color", ScopeInterface::SCOPE_STORE),
            "image_src" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/image_src", ScopeInterface::SCOPE_STORE)
        ];

        $start_date = strtotime($result["start_date"]);
        $end_date = strtotime($result["end_date"]);

        if($start_date && $end_date) {
            $now = strtotime($this->_date->gmtDate());
            if($now >= $start_date && $now <= $end_date) {
                return $result;
            }
        }

        return null;
    }

    /**
     * @param $product
     * @return bool
     */
    public function isProductNew($product) {
        $now = strtotime($this->_date->gmtDate());
        $before = strtotime('-15 day', strtotime($this->_date->gmtDate()));
        $publication_date = strtotime($product->getData('publication_date'));

        if($publication_date >= $before && $publication_date <= $now) {
            return true;
        }

        return false;
    }
}
