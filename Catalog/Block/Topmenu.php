<?php

namespace Troquer\Catalog\Block;

use \Magento\Catalog\Helper\Category;
use \Magento\Catalog\Model\CategoryFactory;
use \Magento\Catalog\Model\Indexer\Category\Flat\State;
use \Magento\Cms\Model\BlockFactory;
use \Magento\Cms\Model\Template\FilterProvider;
use \Magento\Framework\App\Http\Context as HttpContext;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\VersionsCms\Model\Hierarchy\NodeFactory;
use \Smartwave\Megamenu\Helper\Data;

class Topmenu extends \Smartwave\Megamenu\Block\Topmenu
{
    /**
     * @var HttpContext
     */
    protected HttpContext $_httpContext;

    /**
     * Topmenu constructor.
     * @param Context $context
     * @param Category $categoryHelper
     * @param Data $helper
     * @param State $categoryFlatState
     * @param CategoryFactory $categoryFactory
     * @param \Magento\Theme\Block\Html\Topmenu $topMenu
     * @param FilterProvider $filterProvider
     * @param NodeFactory $hierarchyNodeFactory
     * @param BlockFactory $blockFactory
     * @param HttpContext $httpContext
     */
    public function __construct(
        Context $context,
        Category $categoryHelper,
        Data $helper,
        State $categoryFlatState,
        CategoryFactory $categoryFactory,
        \Magento\Theme\Block\Html\Topmenu $topMenu,
        FilterProvider $filterProvider,
        NodeFactory $hierarchyNodeFactory,
        BlockFactory $blockFactory,
        HttpContext $httpContext
    )
    {
        $this->_httpContext = $httpContext;
        parent::__construct($context, $categoryHelper, $helper, $categoryFlatState, $categoryFactory, $topMenu, $filterProvider, $hierarchyNodeFactory, $blockFactory);
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return (bool)$this->_httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }
}
