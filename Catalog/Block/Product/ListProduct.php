<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\Catalog\Block\Product;

use \Magento\Catalog\Api\CategoryRepositoryInterface;
use \Magento\Catalog\Block\Product\Context;
use \Magento\Catalog\Block\Product\ListProduct as MagentoListProduct;
use \Magento\Catalog\Model\Layer\Resolver;
use \Magento\Framework\Data\Helper\PostHelper;
use \Magento\Framework\Exception\InputException;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Stdlib\DateTime\DateTime;
use \Magento\Framework\Url\Helper\Data;
use \Magento\InventorySalesApi\Api\Data\SalesChannelInterface;
use \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use \Magento\InventorySalesApi\Api\StockResolverInterface;
use \Magento\Catalog\Model\Product\Gallery\ReadHandler as GalleryReadHandler;
use \Magento\Store\Model\ScopeInterface;

/**
 * Product list
 * @api
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 100.0.2
 */
class ListProduct extends MagentoListProduct
{
    /**
     * @var GetProductSalableQtyInterface
     */
    protected GetProductSalableQtyInterface $_productSalableQty;

    /**
     * @var StockResolverInterface
     */
    protected StockResolverInterface $_stockResolver;

    /**
     * @var GalleryReadHandler
     */
    private GalleryReadHandler $_galleryReadHandler;

    /**
     * @var DateTime
     */
    protected DateTime $_date;

    /**
     * @param Context $context
     * @param PostHelper $postDataHelper
     * @param Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Data $urlHelper
     * @param GetProductSalableQtyInterface $productSalableQty
     * @param StockResolverInterface $stockResolver
     * @param GalleryReadHandler $galleryReadHandler
     * @param DateTime $date
     * @param array $data
     */
    public function __construct(
        Context $context,
        PostHelper $postDataHelper,
        Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data $urlHelper,
        GetProductSalableQtyInterface $productSalableQty,
        StockResolverInterface $stockResolver,
        GalleryReadHandler $galleryReadHandler,
        DateTime $date,
        array $data = []
    )
    {
        $this->_galleryReadHandler = $galleryReadHandler;
        $this->_productSalableQty = $productSalableQty;
        $this->_stockResolver = $stockResolver;
        $this->_date = $date;
        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper,
            $data
        );
    }

    /**
     * @param $sku
     * @return mixed
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getProductStock($sku)
    {
        $stockId = $this->_stockResolver->execute(SalesChannelInterface::TYPE_WEBSITE, $this->getWebsiteCode())->getStockId();
        return $this->_productSalableQty->execute($sku, $stockId);
    }

    /**
     * @param $product
     */
    public function addGallery($product) {
        $this->_galleryReadHandler->execute($product);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getWebsiteCode()
    {
        return $this->_storeManager->getWebsite()->getCode();
    }

    /**
     * @return array|null
     */
    public function getTagData()
    {
        $result = [
            "start_date" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/start_date", ScopeInterface::SCOPE_STORE),
            "end_date" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/end_date", ScopeInterface::SCOPE_STORE),
            "tag_text" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/tag_text", ScopeInterface::SCOPE_STORE),
            "tag_text_color" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/tag_text_color", ScopeInterface::SCOPE_STORE),
            "tag_background_color" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/tag_background_color", ScopeInterface::SCOPE_STORE),
            "image_src" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/image_src", ScopeInterface::SCOPE_STORE)
        ];

        $start_date = strtotime($result["start_date"]);
        $end_date = strtotime($result["end_date"]);

        if($start_date && $end_date) {
            $now = strtotime($this->_date->gmtDate());
            if($now >= $start_date && $now <= $end_date) {
                return $result;
            }
        }

        return null;
    }

    /**
     * @param $product
     * @return bool
     */
    public function isProductNew($product) {
        $now = strtotime($this->_date->gmtDate());
        $before = strtotime('-15 day', strtotime($this->_date->gmtDate()));
        $publication_date = strtotime($product->getData()["document_source"]["publication_date"]["0"]);

        if($publication_date >= $before && $publication_date <= $now) {
            return true;
        }

        return false;
    }
}
