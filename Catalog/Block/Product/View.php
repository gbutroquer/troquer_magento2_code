<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\Catalog\Block\Product;

use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Catalog\Block\Product\Context;
use \Magento\Catalog\Helper\Product;
use \Magento\Catalog\Model\ProductTypes\ConfigInterface;
use \Magento\Customer\Model\Session;
use \Magento\Framework\Exception\InputException;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Json\EncoderInterface;
use \Magento\Framework\Locale\FormatInterface;
use \Magento\Framework\Pricing\PriceCurrencyInterface;
use \Magento\Framework\Stdlib\StringUtils;
use \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use \Magento\InventorySalesApi\Api\StockResolverInterface;
use \Magento\InventorySalesApi\Api\Data\SalesChannelInterface;

class View extends \Magento\Catalog\Block\Product\View
{
    /**
     * @var GetProductSalableQtyInterface
     */
    protected GetProductSalableQtyInterface $_productSalableQty;

    /**
     * @var StockResolverInterface
     */
    protected StockResolverInterface $_stockResolver;

    /**
     * @param Context $context
     * @param \Magento\Framework\Url\EncoderInterface $urlEncoder
     * @param EncoderInterface $jsonEncoder
     * @param StringUtils $string
     * @param Product $productHelper
     * @param ConfigInterface $productTypeConfig
     * @param FormatInterface $localeFormat
     * @param Session $customerSession
     * @param ProductRepositoryInterface|PriceCurrencyInterface $productRepository
     * @param PriceCurrencyInterface $priceCurrency
     * @param GetProductSalableQtyInterface $productSalableQty
     * @param StockResolverInterface $stockResolver
     * @param array $data
     * @codingStandardsIgnoreStart
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        EncoderInterface $jsonEncoder,
        StringUtils $string,
        Product $productHelper,
        ConfigInterface $productTypeConfig,
        FormatInterface $localeFormat,
        Session $customerSession,
        ProductRepositoryInterface $productRepository,
        PriceCurrencyInterface $priceCurrency,
        GetProductSalableQtyInterface $productSalableQty,
        StockResolverInterface $stockResolver,
        array $data = []
    )
    {
        $this->_productSalableQty = $productSalableQty;
        $this->_stockResolver = $stockResolver;
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data
        );
    }

    /**
     * @param $sku
     * @return mixed
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getProductStock($sku)
    {
        $stockId = $this->_stockResolver->execute(SalesChannelInterface::TYPE_WEBSITE, $this->getWebsiteCode())->getStockId();
        return $this->_productSalableQty->execute($sku, $stockId);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getWebsiteCode()
    {
        return $this->_storeManager->getWebsite()->getCode();
    }
}
