<?php
/** @noinspection PhpDeprecationInspection */
/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedMethodInspection */

namespace Troquer\Catalog\Block\Product\ProductList;

use \Magento\Catalog\Block\Product\AbstractProduct;
use \Magento\Catalog\Block\Product\Context;
use \Magento\Catalog\Model\Product;
use \Magento\Catalog\Model\Product\Visibility as ProductVisibility;
use \Magento\Catalog\Model\ResourceModel\Product\Collection;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use \Magento\Framework\DataObject\IdentityInterface;
use \Magento\Framework\Module\Manager;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Store\Model\ScopeInterface;

class Related extends AbstractProduct implements IdentityInterface
{
    /**
     * @var Collection|null
     */
    protected ?Collection $_itemCollection = null;

    /**
     * Catalog product visibility
     *
     * @var ProductVisibility
     */
    protected ProductVisibility $_catalogProductVisibility;

    /**
     * @var Manager
     */
    protected Manager $moduleManager;

    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $_productCollectionFactory;

    /**
     * @var DateTime
     */
    protected DateTime $_date;

    /**
     * @param Context $context
     * @param ProductVisibility $catalogProductVisibility
     * @param Manager $moduleManager
     * @param CollectionFactory $productCollectionFactory
     * @param DateTime $date
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProductVisibility $catalogProductVisibility,
        Manager $moduleManager,
        CollectionFactory $productCollectionFactory,
        DateTime $date,
        array $data = []
    )
    {
        $this->_catalogProductVisibility = $catalogProductVisibility;
        $this->moduleManager = $moduleManager;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_date = $date;
        parent::__construct($context, $data);
    }

    /**
     * Prepare data
     *
     * @return $this
     */
    protected function _prepareData()
    {
        $product = $this->getProduct();

        $category = $product->getData('garment_category');
        switch ($category) {
            case 6900:
                $this->_itemCollection = $this->accesoriesProductsRelated($product);
                break;
            case 6901:
                $manufacturer = $product->getData('manufacturer');
                if ($manufacturer == 1386 || $manufacturer == 1476 || $manufacturer == 1555 || $manufacturer == 1607 || $manufacturer == 2257 || $manufacturer == 2597 || $manufacturer == 2718 || $manufacturer == 3592 || $manufacturer == 4745 || $manufacturer == 508) {
                    $this->_itemCollection = $this->bagsProductsRelatedPremium($product);
                } else {
                    $this->_itemCollection = $this->bagsProductsRelatedGeneric($product);
                }
                break;
            case 6902:
                $this->_itemCollection = $this->clothingProductsRelated($product);
                break;
            case 6903:
                $this->_itemCollection = $this->jewelryProductsRelated($product);
                break;
            default:
                $this->_itemCollection = $this->shoesProductsRelated($product);
                break;
        }

        $this->_itemCollection
            ->addAttributeToFilter(
                'sku',
                ['neq' => $product->getSku()]
            )
            ->addAttributeToSort('created_at', 'desc')
            ->setPage(1, 20);

        if ($this->moduleManager->isEnabled('Magento_Checkout')) {
            $this->_addProductAttributesAndPrices($this->_itemCollection);
        }
        $this->_itemCollection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());

        $this->_itemCollection->load();

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }

    /**
     * Before to html handler
     *
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $this->_prepareData();
        return parent::_beforeToHtml();
    }

    /**
     * Get collection items
     *
     * @return Collection
     */
    public function getItems()
    {
        /**
         * getIdentities() depends on _itemCollection populated, but it can be empty if the block is hidden
         * @see https://github.com/magento/magento2/issues/5897
         */
        if ($this->_itemCollection === null) {
            $this->_prepareData();
        }
        return $this->_itemCollection;
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        $identities = [[]];
        foreach ($this->getItems() as $item) {
            $identities[] = $item->getIdentities();
        }
        return array_merge(...$identities);
    }

    /**
     * Find out if some products can be easy added to cart
     *
     * @return bool
     */
    public function canItemsAddToCart()
    {
        foreach ($this->getItems() as $item) {
            if (!$item->isComposite() && $item->isSaleable() && !$item->getRequiredOptions()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param Product $product
     * @return Collection
     */
    protected function clothingProductsRelated(Product $product)
    {
        return $this->_productCollectionFactory->create()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('garment_type')
            ->addAttributeToSelect('color')
            ->addAttributeToSelect('gender')
            ->addAttributeToSelect('publication_date')
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('size_clothes_alpha')
            ->addAttributeToFilter(
                'gender',
                $product->getData('gender')
            )
            ->addAttributeToFilter(
                'color',
                $product->getData('color')
            )
            ->addAttributeToFilter(
                'size_clothes_alpha',
                $product->getData('size_clothes_alpha')
            )
            ->addAttributeToFilter(
                'garment_type',
                $product->getData('garment_type')
            )
            ->addAttributeToFilter(
                'price',
                array('from' => 2000, 'to' => 20000)
            );
    }

    /**
     * @param Product $product
     * @return Collection
     */
    protected function shoesProductsRelated(Product $product)
    {
        return $this->_productCollectionFactory->create()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('garment_type')
            ->addAttributeToSelect('gender')
            ->addAttributeToSelect('publication_date')
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('size_shoes_alpha')
            ->addAttributeToFilter(
                'gender',
                $product->getData('gender')
            )
            ->addAttributeToFilter(
                'garment_type',
                $product->getData('garment_type')
            )
            ->addAttributeToFilter(
                'size_shoes_alpha',
                $product->getData('size_shoes_alpha')
            )
            ->addAttributeToFilter(
                'price',
                array('from' => 3000, 'to' => 20000)
            );
    }

    /**
     * @param Product $product
     * @return Collection
     */
    protected function bagsProductsRelatedGeneric(Product $product)
    {
        return $this->_productCollectionFactory->create()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('garment_type')
            ->addAttributeToSelect('gender')
            ->addAttributeToSelect('publication_date')
            ->addAttributeToSelect('price')
            ->addAttributeToFilter(
                'gender',
                $product->getData('gender')
            )
            ->addAttributeToFilter(
                'garment_type',
                $product->getData('garment_type')
            )
            ->addAttributeToFilter(
                'price',
                array('from' => 4000, 'to' => 20000)
            );
    }

    /**
     * @param Product $product
     * @return Collection
     */
    protected function bagsProductsRelatedPremium(Product $product)
    {
        return $this->_productCollectionFactory->create()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('garment_type')
            ->addAttributeToSelect('gender')
            ->addAttributeToSelect('publication_date')
            ->addAttributeToSelect('manufacturer')
            ->addAttributeToFilter(
                'gender',
                $product->getData('gender')
            )
            ->addAttributeToFilter(
                'garment_type',
                $product->getData('garment_type')
            )
            ->addAttributeToFilter(
                'manufacturer',
                $product->getData('manufacturer')
            );
    }

    /**
     * @param Product $product
     * @return Collection
     */
    protected function accesoriesProductsRelated(Product $product)
    {
        return $this->_productCollectionFactory->create()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('garment_type')
            ->addAttributeToSelect('gender')
            ->addAttributeToSelect('publication_date')
            ->addAttributeToFilter(
                'gender',
                $product->getData('gender')
            )
            ->addAttributeToFilter(
                'garment_type',
                $product->getData('garment_type')
            );
    }

    /**
     * @param Product $product
     * @return Collection
     */
    protected function jewelryProductsRelated(Product $product)
    {
        return $this->_productCollectionFactory->create()
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('garment_type')
            ->addAttributeToSelect('gender')
            ->addAttributeToSelect('publication_date')
            ->addAttributeToFilter(
                'gender',
                $product->getData('gender')
            )
            ->addAttributeToFilter(
                'garment_type',
                $product->getData('garment_type')
            );
    }

    /**
     * @return array|null
     */
    public function getTagData()
    {
        $result = [
            "start_date" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/start_date", ScopeInterface::SCOPE_STORE),
            "end_date" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/end_date", ScopeInterface::SCOPE_STORE),
            "tag_text" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/tag_text", ScopeInterface::SCOPE_STORE),
            "tag_text_color" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/tag_text_color", ScopeInterface::SCOPE_STORE),
            "tag_background_color" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/tag_background_color", ScopeInterface::SCOPE_STORE),
            "image_src" => $this->_scopeConfig->getValue("troquer_catalog/tag_configuration/image_src", ScopeInterface::SCOPE_STORE)
        ];

        $start_date = strtotime($result["start_date"]);
        $end_date = strtotime($result["end_date"]);

        if($start_date && $end_date) {
            $now = strtotime($this->_date->gmtDate());
            if($now >= $start_date && $now <= $end_date) {
                return $result;
            }
        }

        return null;
    }

    /**
     * @param $product
     * @return bool
     */
    public function isProductNew($product) {
        $now = strtotime($this->_date->gmtDate());
        $before = strtotime('-15 day', strtotime($this->_date->gmtDate()));
        $publication_date = strtotime($product->getData('publication_date'));

        if($publication_date >= $before && $publication_date <= $now) {
            return true;
        }

        return false;
    }
}
