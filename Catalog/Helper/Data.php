<?php
/** @noinspection PhpUnused */

namespace Troquer\Catalog\Helper;

use \Magento\Catalog\Model\Product;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\Stdlib\DateTime\Timezone\Interceptor;
use \Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    /**
     * @var Interceptor
     */
    protected Interceptor $_timezoneInterceptor;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Interceptor $timezoneInterceptor
     */
    public function __construct(
        Context $context,
        Interceptor $timezoneInterceptor
    )
    {
        $this->_timezoneInterceptor = $timezoneInterceptor;
        parent::__construct($context);
    }

    /**
     * @param Product $product
     * @return bool
     */
    public function isProductNew(Product $product): bool
    {
        $newsFromDate = $product->getData('news_from_date');
        $newsToDate = $product->getData('news_to_date');
        if (!$newsFromDate && !$newsToDate) {
            return false;
        }

        return $this->_timezoneInterceptor->isScopeDateInInterval(
            $product->getStore(),
            $newsFromDate,
            $newsToDate
        );
    }
}
