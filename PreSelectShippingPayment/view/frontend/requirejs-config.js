let config = {
    config: {
        mixins: {
            'Magento_Checkout/js/model/checkout-data-resolver': {
                'Troquer_PreSelectShippingPayment/js/model/checkout-data-resolver-mixin': true
            }
        }
    }
};
