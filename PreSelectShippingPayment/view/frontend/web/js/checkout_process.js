options.positionShipping = undefined;
options.defaultShipping = undefined;
options.defaultPayment = undefined;
options.positionPayment = undefined;
define([
    'jquery'
], function ($) {
    'use strict';
    $.widget('bss.bss_checkout_process', {
        _create: function () {
            let options = this.options;

            let defaultShipping = options.defaultShipping;
            let defaultPayment = options.defaultPayment;

            let positionPayment = options.positionPayment;
            let positionShipping = options.positionShipping;

            $(document).ready(function () {
                $(".box-shipping-method .methods-shipping").each(function () {
                    let checkRadioShipping = false;
                    let countObjectShipping = 0;
                    let countShipping = 0;

                    $(this).find("input[type=radio]").each(function (index) {
                        let shippingCode = $(this).attr("value");

                        if (shippingCode === defaultShipping) {
                            $(this).attr('checked', true);
                            checkRadioShipping = true;
                        }
                        countObjectShipping = index;

                        if (index === countObjectShipping && positionShipping === 'last' && checkRadioShipping === false) {
                            $(this).attr('checked', true);
                            return false;
                        }
                        if (index === 0 && positionShipping === 'first' && checkRadioShipping === false) {
                            countShipping++;
                            if (countShipping === 1) {
                                $(this).prop("checked", true);
                            }
                        }
                    });
                });

                let checkRadioPayment = false;
                let countObjectPayment = 0;
                let countPayment = 0;
                let paymentRadioButtons = $(".methods-payment input[type=radio]");

                paymentRadioButtons.each(function (index) {
                    let paymentCode = $(this).attr("value");
                    if (paymentCode === defaultPayment) {
                        $(this).attr('checked', true);

                        checkRadioPayment = true;
                    }
                    countObjectPayment = index;

                    if (index === countObjectPayment && positionPayment === 'last' && checkRadioPayment === false) {
                        $(this).attr('checked', true);
                        return false;
                    }

                    if (index === 0 && positionPayment === 'first' && checkRadioPayment === false) {
                        countPayment++;
                        if (countPayment === 1) {
                            $(this).prop("checked", true);
                        }
                    }
                });
            });
        }
    });

    return $.bss.bss_checkout_process;
});
