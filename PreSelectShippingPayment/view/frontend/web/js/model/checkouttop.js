require([
    'jquery'
], function ($) {

    $('.payment-method > .payment-method-title > .label').on('click', function () {
        $('html,body').animate({
            scrollTop: $(this).parent().parent().parent().offset().top
        }, 'fast');
    });
})
