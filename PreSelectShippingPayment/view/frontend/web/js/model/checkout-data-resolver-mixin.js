checkoutConfig.bssAspConfig = undefined;
define([
    'underscore',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/payment-service',
    'Magento_Checkout/js/action/select-shipping-method',
    'Magento_Checkout/js/action/select-payment-method'
], function (_, checkoutData, paymentService, selectShippingMethodAction, selectPaymentMethodAction) {

    'use strict';

    return function (checkoutDataResolver) {
        let checkoutDataResolverShipping = checkoutDataResolver.resolveShippingRates,
            checkoutDataResolverPayment = checkoutDataResolver.resolvePaymentMethod,
            checkoutConfig = window.checkoutConfig;

        return _.extend(checkoutDataResolver, {
            /**
             * @inheritdoc
             */
            resolveShippingRates: function (ratesData) {
                let selectedShippingRate = checkoutData.getSelectedShippingRate();
                if (!_.isUndefined(checkoutConfig.bssAspConfig) &&
                    !_.isUndefined(checkoutConfig.bssAspConfig.shipping) &&
                    !selectedShippingRate &&
                    ratesData &&
                    ratesData.length > 1
                ) {
                    let defaultShipping = checkoutConfig.bssAspConfig.shipping.default,
                        positionShipping = checkoutConfig.bssAspConfig.shipping.position;
                    this._autoSelect(defaultShipping, positionShipping, ratesData, 'shipping');
                }

                return checkoutDataResolverShipping.apply(this, arguments);
            },

            /**
             * @inheritdoc
             */
            resolvePaymentMethod: function () {
                let availablePaymentMethods = paymentService.getAvailablePaymentMethods(),
                    selectedPaymentMethod = checkoutData.getSelectedPaymentMethod();

                if (!_.isUndefined(checkoutConfig.bssAspConfig) &&
                    !_.isUndefined(checkoutConfig.bssAspConfig.payment) &&
                    !selectedPaymentMethod &&
                    availablePaymentMethods &&
                    availablePaymentMethods.length > 1
                ) {
                    let defaultPayment = checkoutConfig.bssAspConfig.payment.default,
                        positionPayment = checkoutConfig.bssAspConfig.payment.position;
                    this._autoSelect(defaultPayment, positionPayment, availablePaymentMethods, 'payment');
                }
                return checkoutDataResolverPayment.apply(this);
            },

            /**
             * Auto select with config
             * @param {String} defaultMethod
             * @param {String} positionMethod
             * @param {Array} availableMethods
             * @param {String} step
             */
            _autoSelect: function (defaultMethod, positionMethod, availableMethods, step) {
                let selectMethod;
                if (!_.isUndefined(defaultMethod) && defaultMethod) {
                    selectMethod = '';
                    availableMethods.some(function (method) {
                        if ((step === 'payment' && method.method === defaultMethod) ||
                            (step === 'shipping' && (method.carrier_code + '_' + method.method_code === defaultMethod))
                        ) {
                            selectMethod = method;
                        }
                    });
                }

                if (selectMethod) {
                    if (step === 'shipping') {
                        selectShippingMethodAction(selectMethod);
                    } else {
                        selectPaymentMethodAction(selectMethod);
                    }
                } else {
                    let position = '';
                    if (positionMethod === 'last') {
                        availableMethods.some(function (method, index) {
                            if (method['method_code'] !== null) {
                                position = index;
                            }
                        });
                    } else if (positionMethod === 'first') {
                        let count = 0;
                        availableMethods.some(function (method, index) {
                            if (method['method_code'] !== null) {
                                count++;
                                if (count === 1) {
                                    position = index;
                                }
                            }
                        });
                    }

                    if (position !== '') {
                        if (step === 'shipping') {
                            selectShippingMethodAction(availableMethods[position]);
                        } else {
                            selectPaymentMethodAction(availableMethods[position]);
                        }
                    }
                }
            }
        });
    };
});
