<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\PreSelectShippingPayment\Model\Config\Source;

use \Magento\Framework\Option\ArrayInterface;
use \Magento\Shipping\Model\Config\Source\Allmethods;

class ShippingMethods implements ArrayInterface
{
    /**
     * @var Allmethods
     */
    protected Allmethods $_allShippingMethods;

    /**
     * @param Allmethods $allShippingMethods
     */
    public function __construct(
        Allmethods $allShippingMethods
    )
    {
        $this->_allShippingMethods = $allShippingMethods;
    }

    /**
     * @return array|string[]
     */
    public function toOptionArray()
    {
        $results = $this->_allShippingMethods->toOptionArray(true);
        if (isset($results[0]) && isset($results[0]['label'])) {
            $results[0]['label'] = __('-- Please select --');
        }
        return $results;
    }
}
