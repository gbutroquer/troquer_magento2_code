<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\PreSelectShippingPayment\Model\Config\Source;

use \Magento\Framework\Option\ArrayInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Payment\Model\Config;

class PaymentMethods implements ArrayInterface
{
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_appConfigScopeConfigInterface;

    /**
     * @var Config
     */
    protected Config $_paymentModelConfig;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scope;

    /**
     * PaymentMethods constructor.
     * @param ScopeConfigInterface $scope
     * @param ScopeConfigInterface $appConfigScopeConfigInterface
     * @param Config $paymentModelConfig
     */
    public function __construct(
        ScopeConfigInterface $scope,
        ScopeConfigInterface $appConfigScopeConfigInterface,
        Config $paymentModelConfig
    )
    {
        $this->_scope = $scope;
        $this->_appConfigScopeConfigInterface = $appConfigScopeConfigInterface;
        $this->_paymentModelConfig = $paymentModelConfig;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $payments = $this->_paymentModelConfig->getActiveMethods();
        $methods = [];
        foreach ($payments as $paymentCode => $paymentModel) {
            $paymentTitle = $this->_appConfigScopeConfigInterface
                ->getValue('payment/' . $paymentCode . '/title');
            $methods[$paymentCode] = [
                'label' => $paymentTitle,
                'value' => $paymentCode
            ];
        }
        return $methods;
    }
}
