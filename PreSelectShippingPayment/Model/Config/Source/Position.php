<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\PreSelectShippingPayment\Model\Config\Source;

use \Magento\Framework\Option\ArrayInterface;

class Position implements ArrayInterface
{
    /**
     * @return array|array[]
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('None'),
                'value' => 'none'
            ],
            [
                'label' => __('Last Method'),
                'value' => 'last'
            ],
            [
                'label' => __('First Method'),
                'value' => 'first'
            ]
        ];
    }
}
