<?php

namespace Troquer\PreSelectShippingPayment\Model;

use \Magento\Store\Model\ScopeInterface;
use \Magento\Checkout\Model\ConfigProviderInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class CompositeConfigProvider
 *
 * @package Troquer\PreSelectShippingPayment\Model
 */
class CompositeConfigProvider implements ConfigProviderInterface
{
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $isEnabledShipping = $this->_scopeConfig->isSetFlag(
            'preselectshippingpayment/shipping/enable',
            ScopeInterface::SCOPE_STORE
        );
        $isEnabledPayment = $this->_scopeConfig->isSetFlag(
            'preselectshippingpayment/payment/enable',
            ScopeInterface::SCOPE_STORE
        );
        $output = [];
        if ($isEnabledShipping) {
            $output['bssAspConfig']['shipping'] = [
                'default' => $this->getShipingConfig('default'),
                'position' => $this->getShipingConfig('position')
            ];
        }
        if ($isEnabledPayment) {
            $output['bssAspConfig']['payment'] = [
                'default' => $this->getPaymentConfig('default'),
                'position' => $this->getPaymentConfig('position')
            ];
        }

        return $output;
    }

    /**
     * Get auto shipping config
     *
     * @param string $field
     * @return  mixed
     */
    public function getShipingConfig(string $field)
    {
        return $this->_scopeConfig->getValue(
            'preselectshippingpayment/shipping/' . $field,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get auto payment config
     *
     * @param string $field
     * @return  mixed
     */
    public function getPaymentConfig(string $field)
    {
        return $this->_scopeConfig->getValue(
            'preselectshippingpayment/payment/' . $field,
            ScopeInterface::SCOPE_STORE
        );
    }
}
