<?php
/** @noinspection PhpUnused */

namespace Troquer\PreSelectShippingPayment\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Store\Model\ScopeInterface;
use \Troquer\PreSelectShippingPayment\Model\CompositeConfigProvider;

class ConfigMethod extends Template
{
    /**
     * @var CompositeConfigProvider
     */
    protected CompositeConfigProvider $_shippingPayment;

    /**
     * ConfigMethod constructor.
     * @param Context $context
     * @param CompositeConfigProvider $shippingPayment
     * @param array $data
     */
    public function __construct(
        Context $context,
        CompositeConfigProvider $shippingPayment,
        array $data = []
    )
    {
        $this->_shippingPayment = $shippingPayment;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed|string
     */
    public function getShippingDefault()
    {
        $isEnabledShipping = $this->_scopeConfig->isSetFlag(
            'preselectshippingpayment/shipping/enable',
            ScopeInterface::SCOPE_STORE
        );
        if (!$isEnabledShipping) {
            return '';
        }
        return $this->_shippingPayment->getShipingConfig('default');
    }

    /**
     * @return mixed|string
     */
    public function getPaymentDefault()
    {
        $isEnabledPayment = $this->_scopeConfig->isSetFlag(
            'preselectshippingpayment/payment/enable',
            ScopeInterface::SCOPE_STORE
        );
        if (!$isEnabledPayment) {
            return '';
        }
        return $this->_shippingPayment->getPaymentConfig('default');
    }

    /**
     * @return mixed|string
     */
    public function getPaymentPosition()
    {
        $isEnabledPayment = $this->_scopeConfig->isSetFlag(
            'preselectshippingpayment/payment/enable',
            ScopeInterface::SCOPE_STORE
        );
        if (!$isEnabledPayment) {
            return '';
        }
        return $this->_shippingPayment->getPaymentConfig('position');
    }

    /**
     * @return mixed|string
     */
    public function getShippingPosition()
    {
        $isEnabledPayment = $this->_scopeConfig->isSetFlag(
            'preselectshippingpayment/payment/enable',
            ScopeInterface::SCOPE_STORE
        );
        if (!$isEnabledPayment) {
            return '';
        }
        return $this->_shippingPayment->getShipingConfig('position');
    }
}
