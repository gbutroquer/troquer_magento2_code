<?php

namespace Troquer\Checkout\Plugin\Block\Checkout;

use \Magento\Framework\Stdlib\ArrayManager;

class DirectoryDataProcessor
{
    /**
     * @var ArrayManager
     */
    protected ArrayManager $arrayManager;

    /**
     * LayoutProcessor constructor.
     * @param ArrayManager $arrayManager
     */
    public function __construct(
        ArrayManager $arrayManager
    )
    {
        $this->arrayManager = $arrayManager;
    }

    /**
     * @param $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(/** @noinspection PhpUnusedParameterInspection */ $subject, array $jsLayout)
    {
        $path = $this->arrayManager->findPath('street', $jsLayout);

        $streetNode = $this->arrayManager->get($path, $jsLayout);
        foreach ($streetNode['children'] as $idx => &$child) {
            if($idx < 2) {
                if (!isset($child['validation']['required-entry'])) {
                    $child['validation']['required-entry'] = true;
                }
            }
        }

        $jsLayout = $this->arrayManager->set($path, $jsLayout, $streetNode);

        return $jsLayout;
    }
}
