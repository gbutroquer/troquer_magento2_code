<?php

namespace Troquer\Checkout\Block\Onepage;

use \Magento\Checkout\Helper\Data;
use \Magento\Checkout\Model\Session;
use \Magento\Framework\App\Http\Context as HttpContext;
use \Magento\Framework\View\Element\Template\Context;

class Link extends \Magento\Checkout\Block\Onepage\Link
{
    /**
     * @var HttpContext
     */
    private HttpContext $_httpContext;

    /**
     * @param Context $context
     * @param Session $checkoutSession
     * @param Data $checkoutHelper
     * @param HttpContext $httpContext
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        Data $checkoutHelper,
        HttpContext $httpContext,
        array $data = []
    )
    {
        $this->_httpContext = $httpContext;
        parent::__construct($context, $checkoutSession, $checkoutHelper, $data);
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return (bool)$this->_httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }
}
