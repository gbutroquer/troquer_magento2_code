<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\Checkout\Block\Cart\Item;

use \Magento\Catalog\Block\Product\ImageBuilder;
use \Magento\Catalog\Helper\Product\Configuration;
use \Magento\Checkout\Model\Session;
use \Magento\Framework\Exception\InputException;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Message\ManagerInterface;
use \Magento\Framework\Module\Manager;
use \Magento\Framework\Pricing\PriceCurrencyInterface;
use \Magento\Framework\Url\Helper\Data;
use \Magento\Framework\View\Element\Message\InterpretationStrategyInterface;
use \Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\InventorySalesApi\Api\Data\SalesChannelInterface;
use \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use \Magento\InventorySalesApi\Api\StockResolverInterface;

class Renderer extends \Magento\Checkout\Block\Cart\Item\Renderer
{
    /**
     * @var GetProductSalableQtyInterface
     */
    private GetProductSalableQtyInterface $_productSalableQty;

    /**
     * @var StockResolverInterface
     */
    private StockResolverInterface $_stockResolver;


    /**
     * @param Context $context
     * @param Configuration $productConfig
     * @param Session $checkoutSession
     * @param ImageBuilder $imageBuilder
     * @param Data $urlHelper
     * @param ManagerInterface $messageManager
     * @param PriceCurrencyInterface $priceCurrency
     * @param Manager $moduleManager
     * @param InterpretationStrategyInterface $messageInterpretationStrategy
     * @param GetProductSalableQtyInterface $productSalableQty
     * @param StockResolverInterface $stockResolver
     * @param array $data
     * @param ItemResolverInterface|null $itemResolver
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @codeCoverageIgnore
     */
    public function __construct(
        Context $context,
        Configuration $productConfig,
        Session $checkoutSession,
        ImageBuilder $imageBuilder,
        Data $urlHelper,
        ManagerInterface $messageManager,
        PriceCurrencyInterface $priceCurrency,
        Manager $moduleManager,
        InterpretationStrategyInterface $messageInterpretationStrategy,
        GetProductSalableQtyInterface $productSalableQty,
        StockResolverInterface $stockResolver,
        array $data = [],
        ItemResolverInterface $itemResolver = null
    )
    {
        $this->_productSalableQty = $productSalableQty;
        $this->_stockResolver = $stockResolver;
        parent::__construct(
            $context,
            $productConfig,
            $checkoutSession,
            $imageBuilder,
            $urlHelper,
            $messageManager,
            $priceCurrency,
            $moduleManager,
            $messageInterpretationStrategy,
            $data,
            $itemResolver
        );
    }

    /**
     * @param $sku
     * @return mixed
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getProductStock($sku)
    {
        $stockId = $this->_stockResolver->execute(SalesChannelInterface::TYPE_WEBSITE, $this->getWebsiteCode())->getStockId();
        return $this->_productSalableQty->execute($sku, $stockId);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getWebsiteCode()
    {
        return $this->_storeManager->getWebsite()->getCode();
    }
}
