<?php
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUndefinedMethodInspection */

namespace Troquer\Checkout\Block\Cart;

use \Exception;
use \Magento\Catalog\Helper\Image;
use \Magento\Checkout\Model\Session;
use \Magento\Customer\CustomerData\JsLayoutDataProviderPoolInterface;
use \Magento\Customer\Model\Customer;
use \Magento\Framework\Data\Collection;
use \Magento\Framework\Data\CollectionFactory as InboundProductCollectionFactory;
use \Magento\Framework\DataObject;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Checkout\Block\Cart\Sidebar as MagentoSidebar;
use \Magento\Customer\Model\Session as CustomerSession;
use \Magento\Customer\Model\SessionFactory as SessionFactory;
use \Magento\Framework\App\Http\Context as HttpContext;
use \Troquer\Inbound\Helper\Data;
use \Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class Sidebar extends MagentoSidebar
{
    /**
     * @var HttpContext
     */
    protected HttpContext $_httpContext;

    /**
     * @var InboundProductCollectionFactory
     */
    protected InboundProductCollectionFactory $_inboundCollectionFactory;

    /**
     * @var Collection|null
     */
    protected ?Collection $_notifications = null;

    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var SessionFactory
     */
    protected SessionFactory $_sessionFactory;

    /**
     * @var TimezoneInterface
     */
    protected TimezoneInterface $_timezone;

    /**
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param Session $checkoutSession
     * @param Image $imageHelper
     * @param JsLayoutDataProviderPoolInterface $jsLayoutDataProvider
     * @param HttpContext $httpContext
     * @param InboundProductCollectionFactory $inboundCollectionFactory
     * @param Data $helper
     * @param SessionFactory $sessionFactory
     * @param array $data
     * @param TimezoneInterface $timezone
     * @param Json|null $serializer
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        Session $checkoutSession,
        Image $imageHelper,
        JsLayoutDataProviderPoolInterface $jsLayoutDataProvider,
        HttpContext $httpContext,
        InboundProductCollectionFactory $inboundCollectionFactory,
        Data $helper,
        SessionFactory $sessionFactory,
        TimezoneInterface $timezone,
        array $data = [],
        Json $serializer = null
    )
    {
        $this->_httpContext = $httpContext;
        $this->_inboundCollectionFactory = $inboundCollectionFactory;
        $this->_helper = $helper;
        $this->_sessionFactory = $sessionFactory;
        $this->_timezone = $timezone;
        parent::__construct($context, $customerSession, $checkoutSession, $imageHelper, $jsLayoutDataProvider, $data, $serializer);
    }

    /**
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        $customer = $this->_sessionFactory->create()->getCustomer();
        return $customer->getId() != null;
    }

    /**
     * @return bool|int|null
     */
    public function getCacheLifetime()
    {
        return null;
    }

    /**
     * Returns the Magento Customer Model for this block
     *
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->_sessionFactory->create()->getCustomer();
    }

    /**
     * @param $notification
     * @return false|string
     */
    public function getNotificationAge($notification)
    {
        $start = strtotime($notification->getData('created_at'));
        $end = strtotime($this->_timezone->date()->format('Y-m-d H:i:s'));
        $datediff = $end - $start;

        $minutes = round($datediff / (60));

        if ($minutes > 60) {
            $hours = round($datediff / (60 * 60));
            if ($hours > 24) {
                $days = round($datediff / (60 * 60 * 24));
                if ($days > 30) {
                    return date("j M", $start);
                } else {
                    return "Hace " . $days . " d";
                }
            } else {
                return "Hace " . $hours . " h";
            }
        } else {
            return "Hace " . $minutes . " min";
        }
    }

    /**
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getUnreadNotifications(): ?Collection
    {
        $this->_notifications = $this->_inboundCollectionFactory->create();
        $customer = $this->getCustomer();
        $requestData = [
            "email" => $customer->getEmail()
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/notifications/unread");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);

        $notifications = json_decode($result);
        foreach (array_reverse($notifications[1]) as $notification) {
            $varienObject = new DataObject();
            $varienObject->addData((array)$notification);
            try {
                $this->_notifications->addItem($varienObject);
            } catch (Exception $e) {
            }
        }
        return $this->_notifications;
    }
}
