<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\Checkout\Block\Cart;

use \Magento\Checkout\Model\Session;
use \Magento\Framework\App\Http\Context as HttpContext;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Sales\Model\ConfigInterface;
use \Magento\Checkout\Model\Cart;

class Totals extends \Magento\Checkout\Block\Cart\Totals
{
    /**
     * @var Cart
     */
    protected Cart $_cart;

    /**
     * @var HttpContext
     */
    protected HttpContext $_httpContext;

    /**
     * @param Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param Session $checkoutSession
     * @param ConfigInterface $salesConfig
     * @param Cart $cart
     * @param HttpContext $httpContext
     * @param array $layoutProcessors
     * @param array $data
     * @codeCoverageIgnore
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        Session $checkoutSession,
        ConfigInterface $salesConfig,
        Cart $cart,
        HttpContext $httpContext,
        array $layoutProcessors = [],
        array $data = []
    )
    {
        $this->_httpContext = $httpContext;
        $this->_cart = $cart;
        parent::__construct($context, $customerSession, $checkoutSession, $salesConfig, $layoutProcessors, $data);
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return (bool)$this->_httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

    /**
     * @return string
     */
    public function getCheckoutUrl()
    {
        return $this->getUrl('checkout');
    }

    /**
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function isDisabled()
    {
        return !$this->_checkoutSession->getQuote()->validateMinimumAmount();
    }
}
