<?php
/** @noinspection PhpUnused */

namespace Troquer\Checkout\Model;

use \Magento\Checkout\Model\ConfigProviderInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;

class AutocompleteConfigProvider implements ConfigProviderInterface
{
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfigInterface;

    /**
     * @param ScopeConfigInterface $scopeConfigInterface
     */
    public function __construct(
        ScopeConfigInterface $scopeConfigInterface
    )
    {
        $this->_scopeConfigInterface = $scopeConfigInterface;
    }

    /**
     * @return array|mixed
     */
    public function getConfig()
    {
        $config['troquer_autocomplete'] = [
            'api_key' => $this->_scopeConfigInterface->getValue('cms/pagebuilder/google_maps_api_key')
        ];
        return $config;
    }
}
