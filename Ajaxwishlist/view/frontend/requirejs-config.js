let config = {
    map: {
        '*': {
            'troquer/swatchRenderer': 'Troquer_Ajaxwishlist/js/swatch-renderer',
            'troquer/ajaxwishlist': 'Troquer_Ajaxwishlist/js/ajax-wishlist',
            'troquer/ajaxremove': 'Troquer_Ajaxwishlist/js/ajax-remove'
        }
    }
};
