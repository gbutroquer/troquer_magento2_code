<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\Ajaxwishlist\Helper;

use \Magento\Customer\Model\Session as CustomerSession;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\ObjectManager;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Json\DecoderInterface;
use \Magento\Framework\Json\EncoderInterface;
use \Magento\Framework\Registry;
use \Magento\Framework\View\LayoutFactory;
use \Magento\Store\Model\ScopeInterface;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Wishlist\Block\Customer\Wishlist;
use \Troquer\Ajaxsuite\Helper\Data as AjaxsuiteHelper;

class Data extends AbstractHelper
{
    /**
     * @var int
     */
    protected int $_storeId;

    /**
     * @var Registry
     */
    protected Registry $_coreRegistry;

    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $_storeManager;

    /**
     * @var CustomerSession
     */
    protected CustomerSession $_customerSession;

    /**
     * @var LayoutFactory
     */
    protected LayoutFactory $_layoutFactory;

    /**
     * @var EncoderInterface
     */
    protected EncoderInterface $_jsonEncoder;

    /**
     * @var DecoderInterface
     */
    protected DecoderInterface $_jsonDecoder;

    /**
     * @var AjaxsuiteHelper
     */
    protected AjaxsuiteHelper $_ajaxSuite;

    /**
     * @var Wishlist
     */
    protected Wishlist $_wishList;

    /**
     * @var \Magento\Wishlist\Helper\Data
     */
    protected \Magento\Wishlist\Helper\Data $_wishListHelper;

    /**
     * @var array
     */
    protected array $_productIds = [];

    /**
     * Data constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param Registry $coreRegistry
     * @param CustomerSession $customerSession
     * @param LayoutFactory $layoutFactory
     * @param EncoderInterface $jsonEncoder
     * @param DecoderInterface $jsonDecoder
     * @param AjaxsuiteHelper $ajaxSuite
     * @param Wishlist $wishList
     * @param \Magento\Wishlist\Helper\Data $wishListHelper
     * @throws NoSuchEntityException
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Registry $coreRegistry,
        CustomerSession $customerSession,
        LayoutFactory $layoutFactory,
        EncoderInterface $jsonEncoder,
        DecoderInterface $jsonDecoder,
        AjaxsuiteHelper $ajaxSuite,
        Wishlist $wishList,
        \Magento\Wishlist\Helper\Data $wishListHelper
    )
    {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->_coreRegistry = $coreRegistry;
        $this->_customerSession = $customerSession;
        $this->_layoutFactory = $layoutFactory;
        $this->_jsonEncoder = $jsonEncoder;
        $this->_jsonDecoder = $jsonDecoder;
        $this->_ajaxSuite = $ajaxSuite;
        $this->_wishList = $wishList;
        $this->_wishListHelper = $wishListHelper;
        $this->setStoreId($this->getCurrentStoreId());
    }

    /**
     * @param $store
     * @return Data
     */
    public function setStoreId($store)
    {
        $this->_storeId = $store;
        return $this;
    }

    /**
     * @return int
     * @throws NoSuchEntityException
     */
    public function getCurrentStoreId()
    {
        return $this->_storeManager->getStore(true)->getId();
    }

    /**
     * @return string
     */
    public function getAjaxWishlistInitOptions()
    {
        $optionsAjaxsuite = $this->_jsonDecoder->decode($this->_ajaxSuite->getAjaxSuiteInitOptions());
        $customerId = $this->_customerSession->getCustomer()->getId();
        $isEnable = $this->isEnabledAjaxWishlist();
        if ($this->isWishlistPlusEnabled()) {
            $objectManager = ObjectManager::getInstance();
            $wishlistPlusHelper = $objectManager->get('Troquer\WishlistPlus\Helper\Data');
            if ($wishlistPlusHelper->isWishlistPlusEnable()) {
                $isEnable = 0;
            }
        }
        $options = [
            'ajaxWishlist' => [
                'enabled' => $isEnable,
                'ajaxWishlistUrl' => $this->_getUrl('ajaxwishlist/wishlist/showPopup'),
                'loginUrl' => $this->_getUrl('customer/account/login'),
                'customerId' => $customerId,
            ],
        ];
        return $this->_jsonEncoder->encode(array_merge($optionsAjaxsuite, $options));
    }


    /**
     * @return bool
     */
    public function isEnabledAjaxWishlist()
    {
        return (bool)$this->scopeConfig->getValue(
            'ajaxwishlist/general/enabled',
            ScopeInterface::SCOPE_STORE,
            $this->_storeId
        );
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getOptionsPopupHtml()
    {
        $layout = $this->_layoutFactory->create();
        $update = $layout->getUpdate();
        $update->load('ajaxwishlist_options_popup');
        $layout->generateXml();
        $layout->generateElements();
        return $layout->getOutput();
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getSuccessHtml()
    {
        $layout = $this->_layoutFactory->create(['cacheable' => false]);
        $layout->getUpdate()->addHandle('ajaxwishlist_success_message')->load();
        $layout->generateXml();
        $layout->generateElements();
        $result = $layout->getOutput();
        $layout->__destruct();
        return $result;
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getErrorHtml()
    {
        $layout = $this->_layoutFactory->create(['cacheable' => false]);
        $layout->getUpdate()->addHandle('ajaxwishlist_error_message')->load();
        $layout->generateXml();
        $layout->generateElements();
        $result = $layout->getOutput();
        $layout->__destruct();
        return $result;
    }

    /**
     * @return mixed
     * @throws LocalizedException
     */
    public function getSuccessRemoveHtml()
    {
        $layout = $this->_layoutFactory->create(['cacheable' => false]);
        $layout->getUpdate()->addHandle('ajaxwishlist_remove_success_message')->load();
        $layout->generateXml();
        $layout->generateElements();
        $result = $layout->getOutput();
        $layout->__destruct();
        return $result;
    }

    public function isWishlistPlusEnabled()
    {
        return $this->_moduleManager->isEnabled('Troquer_WishlistPlus');
    }
}
