<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Ajaxwishlist\Block;

use \Magento\Framework\Json\EncoderInterface;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Wishlist\Block\Customer\Wishlist;
use \Troquer\Ajaxwishlist\Helper\Data;

class Js extends Template
{
    /**
     * @var Data
     */
    protected Data $_ajaxwishlistHelper;

    /**
     * @var Wishlist
     */
    protected Wishlist $_wishList;

    /**
     * @var \Magento\Wishlist\Helper\Data
     */
    protected \Magento\Wishlist\Helper\Data $_wishListHelper;

    /**
     * @var int
     */
    protected int $_products = 0;

    /**
     * @var EncoderInterface
     */
    protected EncoderInterface $_jsonEncoder;

    /**
     * Js constructor.
     * @param Context $context
     * @param Data $ajaxwishlistHelper
     * @param Wishlist $wishList
     * @param \Magento\Wishlist\Helper\Data $wishListHelper
     * @param EncoderInterface $jsonEncoder
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $ajaxwishlistHelper,
        Wishlist $wishList,
        \Magento\Wishlist\Helper\Data $wishListHelper,
        EncoderInterface $jsonEncoder,
        array $data = []
    )
    {
        parent::__construct($context, $data);

        $this->_wishList = $wishList;
        $this->_wishListHelper = $wishListHelper;
        $this->_ajaxwishlistHelper = $ajaxwishlistHelper;
        $this->_jsonEncoder = $jsonEncoder;
        $this->_template = 'js/main.phtml';
    }

    /**
     * @return string
     */
    public function getAjaxWishlistInitOptions()
    {
        return $this->_ajaxwishlistHelper->getAjaxWishlistInitOptions();
    }

    /**
     * @return int|void
     */
    public function getProductsNumber()
    {
        $products = $this->_products;
        if ($products === null) {
            $connection = $this->_wishList->getWishlistItems()->getConnection();
            $select = $this->_wishList->getWishlistItems()->getSelect();
            $items = $connection->fetchAll($select);
            $products = count($items);
            return $products;
        }
        return $this->_products;
    }
}
