<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\Ajaxwishlist\Block\Product;

use \Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use \Magento\Framework\ObjectManagerInterface;
use \Magento\Framework\Registry;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Catalog\Helper\Image as ImageHelper;

class Image extends Template
{
    /**
     * @var Registry|null
     */
    protected ?Registry $_coreRegistry = null;

    /**
     * @var ObjectManagerInterface
     */
    protected ObjectManagerInterface $_objectManager;

    /**
     * @var ImageHelper
     */
    protected ImageHelper $_prdImageHelper;

    /**
     * Image constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ObjectManagerInterface $objectManager
     * @param ImageHelper $imageHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ObjectManagerInterface $objectManager,
        ImageHelper $imageHelper,
        array $data
    )
    {
        parent::__construct($context, $data);
        $this->_coreRegistry = $registry;
        $this->_objectManager = $objectManager;
        $this->_prdImageHelper = $imageHelper;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        $color = $this->_request->getParam('color');
        $configurablePrdModel = $this->_objectManager->get('Magento\ConfigurableProduct\Model\Product\Type\Configurable');
        $attributeOptions = [93 => $color];
        $prdId = $this->_coreRegistry->registry('current_product')->getId();
        $product = $this->_objectManager->get('Magento\Catalog\Model\Product')->load($prdId);
        if ($product->getTypeId() == Configurable::TYPE_CODE) {
            $assPro = $configurablePrdModel->getProductByAttributes($attributeOptions, $product);
            if ($assPro->getId()) {
                $imageUrl = $this->getProductImageUrl($assPro, 'category');
            } else {
                $imageUrl = $this->getProductImageUrl($product, 'category');
            }
        } else {
            $imageUrl = $this->getProductImageUrl($product, 'category');
        }
        return $imageUrl;
    }

    /**
     * @param $product
     * @param $size
     * @return string
     */
    public function getProductImageUrl($product, $size)
    {
        $imageSize = 'product_page_image_' . $size;
        if ($size == 'category') {
            $imageSize = 'category_page_list';
        }
        return $this->_prdImageHelper->init($product, $imageSize)
            ->keepAspectRatio(true)
            ->keepFrame(false)
            ->getUrl();
    }
}
