<?php
/** @noinspection PhpUnused */

namespace Troquer\Ajaxwishlist\Block\Product;

use \Magento\Framework\View\Element\Template;

class ConfigurableOption extends Template
{

    /**
     * @return mixed
     */
    public function getColorLabel()
    {
        return $this->_request->getParam('colorLabel');
    }

    /**
     * @return mixed
     */
    public function getSizeLabel()
    {
        return $this->_request->getParam('sizeLabel');
    }
}
