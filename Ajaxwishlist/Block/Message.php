<?php

namespace Troquer\Ajaxwishlist\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Troquer\Ajaxsuite\Helper\Data;

class Message extends Template
{
    /**
     * @var Data
     */
    protected Data $_ajaxsuiteHelper;

    /**
     * Message constructor.
     * @param Context $context
     * @param Data $ajaxsuiteHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $ajaxsuiteHelper,
        array $data
    )
    {
        parent::__construct($context, $data);
        $this->_ajaxsuiteHelper = $ajaxsuiteHelper;
    }

    /**
     * @return mixed|string
     */
    public function getMessage()
    {
        $message = $this->_ajaxsuiteHelper->getScopeConfig('ajaxwishlist/general/message');
        if (!$message) {
            $message = __('You have added this product to your wishlist');
        }
        return $message;
    }
}
