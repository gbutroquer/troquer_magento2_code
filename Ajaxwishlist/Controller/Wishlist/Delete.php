<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Ajaxwishlist\Controller\Wishlist;

use \Exception;
use \Magento\Catalog\Api\Data\ProductInterface;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Exception\NotFoundException;
use \Magento\Framework\Registry;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Wishlist\Controller\AbstractIndex;
use \Magento\Wishlist\Helper\Data;
use \Magento\Wishlist\Model\Wishlist;
use \Troquer\Ajaxwishlist\Helper\Data as AjaxwishlistData;

class Delete extends AbstractIndex
{
    /**
     * @var Wishlist
     */
    protected Wishlist $_wishlist;

    /**
     * @var ProductRepositoryInterface
     */
    protected ProductRepositoryInterface $_productRepository;

    /**
     * @var AjaxwishlistData
     */
    protected AjaxwishlistData $_ajaxWishlistHelper;

    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $_storeManager;

    /**
     * @var Registry
     */
    protected Registry $_coreRegistry;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected \Magento\Framework\Json\Helper\Data $_jsonEncode;

    /**
     * Delete constructor.
     * @param Context $context
     * @param ProductRepositoryInterface $productRepository
     * @param AjaxwishlistData $ajaxWishlistHelper
     * @param Registry $registry
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Framework\Json\Helper\Data $jsonEncode
     * @param Wishlist $wishlist
     */
    public function __construct(
        Context $context,
        ProductRepositoryInterface $productRepository,
        AjaxwishlistData $ajaxWishlistHelper,
        Registry $registry,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Json\Helper\Data $jsonEncode,
        Wishlist $wishlist
    )
    {
        $this->_wishlist = $wishlist;
        $this->_ajaxWishlistHelper = $ajaxWishlistHelper;
        $this->_jsonEncode = $jsonEncode;
        $this->_storeManager = $storeManager;
        $this->_productRepository = $productRepository;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws NotFoundException
     */
    public function execute()
    {
        $request = $this->getRequest();
        $product_id = $request->getParam('product', null);
        if (!$product_id) {
            throw new NotFoundException(__('Page not found'));
        }

        $customer_id = $request->getParam('customerId', null);
        $wishlist = $this->_wishlist->loadByCustomerId($customer_id);
        if (!$wishlist) {
            throw new NotFoundException(__('Page not found'));
        }

        $items = $wishlist->getItemCollection();
        foreach ($items as $item) {
            if ($item->getProductId() == $product_id) {
                try {
                    $item->delete();
                    $wishlist->save();
                } catch (LocalizedException $e) {
                    $this->messageManager->addError(
                        __('We can\'t delete the item from Wish List right now because of an error: %1.',
                            $e->getMessage())
                    );
                } catch (Exception $e) {
                    $this->messageManager->addError(__('We can\'t delete the item from the Wish List right now.'));
                }
            }
        }
        if (!empty($this->getRequest()->getParam('isRemoveSubmit', null))) {
            $product = $this->_initProduct();
            $this->_coreRegistry->register('product', $product);
            $this->_coreRegistry->register('current_product', $product);
            $htmlPopup = $this->_ajaxWishlistHelper->getSuccessRemoveHtml();
            $result['success'] = true;
            $result['html_popup'] = $htmlPopup;
            $this->getResponse()->representJson($this->_jsonEncode->jsonEncode($result));
        }
        $this->_objectManager->get(Data::class)->calculate();
    }

    /**
     * @return bool|ProductInterface
     * @throws NoSuchEntityException
     */
    protected function _initProduct()
    {
        $productId = (int)$this->getRequest()->getParam('product', null);
        if ($productId) {
            $storeId = $this->_storeManager->getStore()->getId();
            try {
                return $this->_productRepository->getById($productId, false, $storeId);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }
        return false;
    }
}
