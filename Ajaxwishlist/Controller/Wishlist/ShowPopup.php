<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Ajaxwishlist\Controller\Wishlist;

use \Exception;
use \Magento\Catalog\Api\Data\ProductInterface;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Registry;
use \Troquer\Ajaxsuite\Helper\Data;

class ShowPopup extends Action
{
    /**
     * @var \Troquer\Ajaxwishlist\Helper\Data
     */
    protected \Troquer\Ajaxwishlist\Helper\Data $_ajaxWishlistHelper;

    /**
     * @var Data
     */
    protected Data $_ajaxSuiteHelper;

    /**
     * @var ProductRepositoryInterface
     */
    protected ProductRepositoryInterface $_productRepository;

    /**
     * @var Registry|null
     */
    protected ?Registry $_coreRegistry = null;

    /**
     * ShowPopup constructor.
     * @param Context $context
     * @param \Troquer\Ajaxwishlist\Helper\Data $ajaxWishlistHelper
     * @param Data $ajaxSuiteHelper
     * @param ProductRepositoryInterface $productRepository
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        \Troquer\Ajaxwishlist\Helper\Data $ajaxWishlistHelper,
        Data $ajaxSuiteHelper,
        ProductRepositoryInterface $productRepository,
        Registry $registry
    )
    {
        parent::__construct($context);
        $this->_ajaxWishlistHelper = $ajaxWishlistHelper;
        $this->_productRepository = $productRepository;
        $this->_coreRegistry = $registry;
        $this->_ajaxSuiteHelper = $ajaxSuiteHelper;
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     * @throws LocalizedException
     */
    public function execute()
    {
        $result = [];
        $isLoggedIn = $this->_ajaxSuiteHelper->getLoggedCustomer();

        if ($isLoggedIn == true) {
            try {
                $product = $this->_initProduct();
                $this->_coreRegistry->register('product', $product);
                $this->_coreRegistry->register('current_product', $product);

                $htmlPopup = $this->_ajaxWishlistHelper->getOptionsPopupHtml();
                $result['success'] = true;
                $result['html_popup'] = $htmlPopup;
            } catch (Exception $e) {
                $this->messageManager->addException($e, __('You can\'t login right now.'));
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $result['success'] = false;
            }
        } else {
            $product = $this->_initProduct();
            $this->_coreRegistry->register('product', $product);
            $this->_coreRegistry->register('current_product', $product);

            $htmlPopup = $this->_ajaxWishlistHelper->getErrorHtml();
            $result['success'] = false;
            $result['html_popup'] = $htmlPopup;
        }
        $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
        );
    }

    /**
     * @return bool|ProductInterface
     */
    protected function _initProduct()
    {
        $productId = (int)$this->getRequest()->getParam('product');
        if ($productId) {
            $storeId = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore()->getId();
            try {
                return $this->_productRepository->getById($productId, false, $storeId);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }
        return false;
    }
}
