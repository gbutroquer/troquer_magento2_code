<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Ajaxwishlist\Plugin\Controller\Index;

use \Magento\Catalog\Api\Data\ProductInterface;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Framework\App\ObjectManager;
use \Magento\Framework\Controller\Result\Redirect;
use \Magento\Framework\Controller\Result\RedirectFactory;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Json\Helper\Data;
use \Magento\Framework\Registry;
use \Magento\Store\Model\StoreManagerInterface;
use \Troquer\Ajaxwishlist\Helper\Data as AjaxwishlistData;

class Add
{
    /**
     * @var Registry|null
     */
    protected ?Registry $_coreRegistry = null;

    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $_storeManager;

    /**
     * @var ProductRepositoryInterface
     */
    protected ProductRepositoryInterface $_productRepository;

    /**
     * @var Data
     */
    protected Data $_jsonEncode;

    /**
     * @var RedirectFactory
     */
    protected RedirectFactory $_resultRedirectFactory;

    /**
     * @var AjaxwishlistData
     */
    protected AjaxwishlistData $_ajaxWishlistHelper;


    /**
     * Add constructor.
     * @param Registry $registry
     * @param StoreManagerInterface $storeManager
     * @param ProductRepositoryInterface $productRepository
     * @param Data $jsonEncode
     * @param RedirectFactory $redirectFactory
     * @param AjaxwishlistData $ajaxWishlistHelper
     */
    public function __construct
    (
        Registry $registry,
        StoreManagerInterface $storeManager,
        ProductRepositoryInterface $productRepository,
        Data $jsonEncode,
        RedirectFactory $redirectFactory,
        AjaxwishlistData $ajaxWishlistHelper
    )
    {
        $this->_resultRedirectFactory = $redirectFactory;
        $this->_jsonEncode = $jsonEncode;
        $this->_storeManager = $storeManager;
        $this->_productRepository = $productRepository;
        $this->_ajaxWishlistHelper = $ajaxWishlistHelper;
        $this->_coreRegistry = $registry;
    }

    /**
     * @param $subject
     * @param $proceed
     * @return Redirect
     * @throws LocalizedException
     */
    public function aroundExecute($subject, $proceed)
    {
        if ($this->_ajaxWishlistHelper->isWishlistPlusEnabled()) {
            $objectManager = ObjectManager::getInstance();
            $wishlistPlusHelper = $objectManager->get('Troquer\WishlistPlus\Helper\Data');
            if ($wishlistPlusHelper->isWishlistPlusEnable()) {
                $proceed();
            } else {
                $result = [];
                $params = $subject->getRequest()->getParams();

                $product = $this->_initProduct($subject);
                if (!empty($params['isWishlistSubmit'])) {
                    $proceed();
                    $this->_coreRegistry->register('product', $product);
                    $this->_coreRegistry->register('current_product', $product);

                    $htmlPopup = $this->_ajaxWishlistHelper->getSuccessHtml();
                    $result['success'] = true;
                    $result['html_popup'] = $htmlPopup;

                    $subject->getResponse()->representJson($this->_jsonEncode->jsonEncode($result));
                } else {
                    $proceed();
                    return $this->_resultRedirectFactory->create()->setPath('*');
                }
            }
        } else {
            $result = [];
            $params = $subject->getRequest()->getParams();

            $product = $this->_initProduct($subject);

            if (!empty($params['isWishlistSubmit'])) {
                $proceed();
                $this->_coreRegistry->register('product', $product);
                $this->_coreRegistry->register('current_product', $product);

                $htmlPopup = $this->_ajaxWishlistHelper->getSuccessHtml();
                $result['success'] = true;
                $result['html_popup'] = $htmlPopup;

                $subject->getResponse()->representJson($this->_jsonEncode->jsonEncode($result));
            } else {
                $proceed();
                return $this->_resultRedirectFactory->create()->setPath('*');
            }
        }
        return null;
    }

    /**
     * @param $subject
     * @return bool|ProductInterface
     * @throws NoSuchEntityException
     */
    protected function _initProduct($subject)
    {
        $productId = (int)$subject->getRequest()->getParam('product');
        if ($productId) {
            $storeId = $this->_storeManager->getStore()->getId();
            try {
                return $this->_productRepository->getById($productId, false, $storeId);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }
        return false;
    }
}
