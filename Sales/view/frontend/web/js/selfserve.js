define([
    'jquery',
    'listnavjs'
], function ($, _) {
    'use strict';

    return function (options) {
        localStorage.removeItem("tracking_number");
        localStorage.removeItem("inbound_order_id");
        localStorage.setItem("inbound_order_id", options["order_id"]);

        $('.genderselected').on("click", function () {
            $(this).parent().parent().find('.genderselected').removeClass('activegenderselected');
            $(this).addClass('activegenderselected');
            let val = $(this).attr('data-value');
            $(this).parent().find('input').val(val);
        });

        $('.info-statu').on("click", function () {
            $('.info-statu').removeClass('activegenderselected');
            $(this).addClass('activegenderselected');
            let val = $(this).attr('data-value');
            $(this).parent().find('input').val(val);
            let txtselectestado = $(this).children('h4').text();
            let modalestado = $('#modalestado');
            $('.btneditestado').remove();
            $('#modalestado > span.titleself').text('*Estado de uso: ');
            $('#modalestado > span.infoself').text(txtselectestado);
            $('#btnacepstatus').removeClass('desactivedbotton');
            modalestado.addClass('btn-active-model')
        });

        $("#search_brand").on("keyup", function () {
            let value = $(this).val().toLowerCase();
            $("#listBrand li").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $("#search_model").on("keyup", function () {
            let vmodel = $(this).val().toLowerCase();
            $("#listModel li").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(vmodel) > -1)
            });
        });

        $("#search_material").on("keyup", function () {
            let vmaterial = $(this).val().toLowerCase();
            $("#listMaterial li").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(vmaterial) > -1)
            });
        });

        const general = $('section#general');
        const characteristics = $('section#characteristics');
        const picture = $('section#picture');

        function updateView() {
            let pathname = window.location.href;
            pathname = pathname.split("#")[1];
            let pb_general = $('li#pb-general');
            let pb_characteristics = $('li#pb-characteristics');
            let pb_picture = $('li#pb-picture');
            let head_text = $('#head-text');
            switch (pathname) {
                case 'informacion-general':
                    head_text.html('Ingresa la <span class="emphasys">información general</span> de tu pieza. Esto nos ayudará a calcular un valor más acertado.');
                    general.show();
                    characteristics.hide();
                    picture.hide();
                    pb_general.addClass('active');
                    pb_characteristics.removeClass('active');
                    pb_picture.removeClass('active');
                    break;
                case 'caracteristicas':
                    head_text.html('Ahora necesitaremos conocer <span class="emphasys">características más específicas</span> de tu pieza.');
                    general.hide();
                    characteristics.show();
                    picture.hide();
                    pb_general.addClass('active');
                    pb_characteristics.addClass('active');
                    pb_picture.removeClass('active');
                    break;
                case 'fotografia':
                    head_text.html('Y por <span class="emphasys">último</span> antes de darte la ganancia estimada, necesitamos una fotografía de tu pieza.');
                    general.hide();
                    characteristics.hide();
                    picture.show();
                    pb_general.addClass('active');
                    pb_characteristics.addClass('active');
                    pb_picture.addClass('active');
                    break;
                default:
                    general.show();
                    characteristics.hide();
                    picture.hide();
                    pb_general.addClass('active');
                    pb_characteristics.removeClass('active');
                    pb_picture.removeClass('active');
                    break;
            }
        }

        updateView();
        general.find('button.primary').on("click", function () {
            if ($('.gender-hombre').is(":not(:checked)") || $('.gender-mujer').is(":not(:checked)") || $('.gender-singenero').is(":not(:checked)")) {
                if ($("#garment-select option:selected").index() === 0 || $("#category-select option:selected").index() === 0) {
                    alert("Alguno de sus campos esta vacio");
                } else {
                    parent.location.hash = "caracteristicas";
                    $(document).scrollTop(0);
                }
            } else {
                parent.location.hash = "caracteristicas";
                $(document).scrollTop(0);
            }
        });

        characteristics.find('button.secondary').on("click", function () {
            parent.location.hash = "informacion-general";
            $(document).scrollTop(0);
        });

        characteristics.find('button.primary').on("click", function () {
            if ($("#modalbrand span.infoself").text() === '' || $("#modalmaterial span.infoself").text() === '' || $("#modalcolor span.infoself").text() === '' || $("#modalestado span.infoself").text() === '') {
                alert("Alguno de sus campos esta vacio");
            } else {
                parent.location.hash = "fotografia";
                $(document).scrollTop(0);
            }
        });

        picture.find('button.secondary').on("click", function () {
            parent.location.hash = "caracteristicas";
            $(document).scrollTop(0);
        });
        picture.find('button.primary').on("click", function () {
            if ($('#fileimage').get(0).files.length === 0 && localStorage.getItem("selfserve_image") === null) {
                alert("Debe seleccionar una imagen");
            } else {
                $('#form-save-product > #color_id').val(localStorage.getItem("selfserve_color"));
                $('#form-save-product > #material_id').val(localStorage.getItem("selfserve_material"));
                $('#form-save-product > #condition_id').val(localStorage.getItem("selfserve_status"));
                $('#form-save-product > #garment_id').val(localStorage.getItem("selfserve_garment"));
                $('#form-save-product > #garment_category_id').val(localStorage.getItem("selfserve_category"));
                $('#form-save-product > #photo').val(localStorage.getItem("selfserve_image"));
                $('#form-save-product > #brand_id').val(localStorage.getItem("selfserve_brand"));
                $('#form-save-product > #gender_id').val(localStorage.getItem("selfserve_gender"));
                $('#form-save-product > #model_id').val(localStorage.getItem("selfserve_model") ? localStorage.getItem("selfserve_model") : '');
                $('#form-save-product > #original_price').val(localStorage.getItem("selfserve_price"));
                $('#form-save-product > #selfserve_status_magento').val(localStorage.getItem("selfserve_status_magento"));
                $('#form-save-product > #selfserve_material_magento').val(localStorage.getItem("selfserve_material_magento"));
                $('#form-save-product > #selfserve_garment_magento').val(localStorage.getItem("selfserve_garment_magento"));
                $('#form-save-product > #selfserve_brand_magento').val(localStorage.getItem("selfserve_brand_magento"));

                Object.entries(localStorage).map(
                    x => x[0]
                ).filter(
                    x => x.substring(0, 10) === "selfserve_"
                ).map(
                    x => localStorage.removeItem(x)
                );

                $("#form-save-product").submit();
            }
        });
        window.addEventListener('popstate', function () {
            updateView();
        });

        const arrayGarments = options["arrayGarments"];
        const arrayBrands = options["arrayBrands"];
        const arrayModels = options["arrayModels"];
        const garmentSelect = $('#garment-select');
        const brandSelect = $('#modalbrand > .infoself');
        const btnaceptmodel = $('#btnacepmodel');

        brandSelect.on("change", function () {
            localStorage.setItem("selfserve_brand", this.value);
        });

        $(".genderselected").each(function () {
            $(this).on("click", function () {
                localStorage.setItem("selfserve_gender", $(this).attr('data-value'));
            });
        });

        $('#category-select').on("change", function () {
            let spinner_div = $('#category-spinner');
            spinner_div.addClass('spinner');
            localStorage.setItem("selfserve_category", this.value);
            let category_name = $(this.options[this.selectedIndex]).text();

            localStorage.setItem("selfserve_category_name", category_name);
            localStorage.removeItem("selfserve_brand");
            localStorage.removeItem("selfserve_brand_has_models");
            $('#modalbrand > span.titleself').text('*Marca');
            let infoself = $('#modalbrand > span.infoself');
            let modalbrand = $('#modalbrand');
            infoself.text('');
            infoself.attr('value', 'txtvalue');
            $('#btnacepbrand').addClass('desactivedbotton');
            modalbrand.removeClass('btn-active-model');
            emptyListModel();
            $('#listBrand-nav').remove();
            $('#listModel-nav').remove();

            if (this.value !== 0) {
                garmentSelectPopulate(this.value);
                brandSelectPopulate(category_name);
            }
            spinner_div.removeClass('spinner');
        });

        garmentSelect.on("change", function () {
            localStorage.setItem("selfserve_garment", this.value);
            localStorage.setItem("selfserve_garment_magento", $('option:selected', this).attr('data-value'));
        });

        let lblmaterial = $(".lblmaterial");
        lblmaterial.each(function () {
            $(this).on("click", function () {
                localStorage.setItem("selfserve_material", $(this).prev().val());
                localStorage.setItem("selfserve_material_magento", $(this).prev().attr("data-value"));
            });
        });

        let lblcolor = $(".lblcolor");
        lblcolor.each(function () {
            $(this).on("click", function () {
                localStorage.setItem("selfserve_color", $(this).prev().val());
            });
        });

        $(".info-statu").each(function () {
            $(this).on("click", function () {
                localStorage.setItem("selfserve_status", $(this).attr('data-value'));
                localStorage.setItem("selfserve_status_magento", $(this).attr('data-id'));
            });
        });

        const btnprice = $('#btn-price');
        btnprice.on("click", function () {
            let elvalueprice = $('#valueprice');
            let value_price;
            if (elvalueprice.val() === '') {
                value_price = 0;
            } else {
                value_price = elvalueprice.val();
            }
            localStorage.setItem("selfserve_price", value_price);
        });

        $("#fileimage").on("change", function () {
            uploadPhoto(this);
        });
        $(".photo_upload").on("click", function () {
            $('#fileimage').trigger('click');
        });

        function uploadPhoto(input) {
            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function (readerEvent) {
                    const image = new Image();
                    image.onload = function (_) {
                        let canvas = document.createElement('canvas'),
                            max_size = 1500,
                            width = image.width,
                            height = image.height;
                        if (width > height) {
                            if (width > max_size) {
                                height *= max_size / width;
                                width = max_size;
                            }
                        } else {
                            if (height > max_size) {
                                width *= max_size / height;
                                height = max_size;
                            }
                        }
                        canvas.width = width;
                        canvas.height = height;
                        canvas.getContext('2d').drawImage(image, 0, 0, width, height);
                        let dataUrl = canvas.toDataURL('image/jpeg');
                        let resizedImage = dataURLToBlob(dataUrl);
                        $.event.trigger({
                            type: "imageResized",
                            blob: resizedImage,
                            url: dataUrl
                        });
                        let output = $('#output');
                        output.attr('src', dataUrl);
                        localStorage.setItem("selfserve_image", dataUrl);
                    }
                    image.src = readerEvent.target.result.toString();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function dataURLToBlob(dataURL) {
            let BASE64_MARKER = ';base64,';
            if (dataURL.indexOf(BASE64_MARKER) === -1) {
                let parts = dataURL.split(',');
                let contentType = parts[0].split(':')[1];
                let raw = parts[1];

                return new Blob([raw], {type: contentType});
            }

            let parts = dataURL.split(BASE64_MARKER);
            let contentType = parts[0].split(':')[1];
            let raw = window.atob(parts[1]);
            let rawLength = raw.length;

            let uInt8Array = new Uint8Array(rawLength);

            for (let i = 0; i < rawLength; ++i) {
                uInt8Array[i] = raw.charCodeAt(i);
            }

            return new Blob([uInt8Array], {type: contentType});
        }

        $(document).ready(function () {
            if (localStorage.getItem('selfserve_gender')) {
                $('div[data-value=' + localStorage.getItem('selfserve_gender') + ']').addClass('activegenderselected');
            }

            if (localStorage.getItem("selfserve_category")) {
                if (parseInt(localStorage.getItem("selfserve_category")) !== 0) {
                    garmentSelectPopulate(localStorage.getItem("selfserve_category"));
                }
                $('#category-select').val(localStorage.getItem("selfserve_category"));
            }

            if (localStorage.getItem('selfserve_garment')) {
                $('#garment-select').val(localStorage.getItem('selfserve_garment'));
            }

            let category_name = localStorage.getItem("selfserve_category_name");
            brandSelectPopulate(category_name);

            if (localStorage.getItem('selfserve_brand')) {
                $('#modalbrand').addClass('btn-active-model');
                $('#modalbrand > span.titleself').text('*Marca: ');
                let spaninfoself = $('#modalbrand > span.infoself');
                let selfserfebrand = $('#brand-' + localStorage.getItem('selfserve_brand'));
                spaninfoself.text(selfserfebrand.next().text());
                spaninfoself.attr('value', selfserfebrand.next().text());
            }

            if (parseInt(localStorage.getItem("selfserve_brand_has_models")) !== 0 && parseInt(localStorage.getItem("selfserve_category")) === 1) {
                modelSelectPopulate();
            } else {
                emptyListModel();
            }
            $('#brandModal').hide();

            if (localStorage.getItem('selfserve_model')) {
                let modalmodel = $('#modalmodel');
                $('#modalmodel > span.titleself').text('Modelo: ');
                let spaninfoself = $('#modalmodel > span.infoself');
                let selfserfebrand = $('#model-' + localStorage.getItem('selfserve_model'));
                spaninfoself.text(selfserfebrand.next().text());
                spaninfoself.attr('value', selfserfebrand.next().text());
                btnaceptmodel.removeClass('desactivedbotton');
                modalmodel.addClass('btn-active-model');
            }

            if (localStorage.getItem('selfserve_material')) {
                $('#modalmaterial').addClass('btn-active-model');
                $('#modalmaterial > span.titleself').text('*Material: ');
                let spaninfoself = $('#modalmaterial > span.infoself');
                spaninfoself.text($('#material-' + localStorage.getItem('selfserve_material')).next().text());
                spaninfoself.attr('value', localStorage.getItem('selfserve_material'));
            }

            if (localStorage.getItem('selfserve_color')) {
                let colorid = localStorage.getItem('selfserve_color');
                $('#modalcolor').addClass('btn-active-model');
                $('#modalcolor > span.titleself').text('*Color: ');
                let spaninfoself = $('#modalcolor > span.infoself');
                spaninfoself.text($('#color-' + localStorage.getItem('selfserve_color')).next().next().text());
                spaninfoself.attr('value', colorid.replace("color-", ""));
            }

            if (localStorage.getItem('selfserve_status')) {
                let statusid = localStorage.getItem('selfserve_status');
                $('#modalestado').addClass('btn-active-model');
                $('#modalestado > span.titleself').text('*Estado de uso: ');
                let spaninfoself = $('#modalestado > span.infoself');
                spaninfoself.text($('#status-' + localStorage.getItem('selfserve_status')).children("h4").text());
                spaninfoself.attr('data-value', statusid.replace("status-", ""));
            }

            if (localStorage.getItem('selfserve_price')) {
                $('#modalprecio').addClass('btn-active-model');
                $('#modalprecio > span.titleself').text('*Price original: ');
                if (parseInt(localStorage.getItem('selfserve_price')) === 0) {
                    $('#modalprecio > span.infoself').text('Desconocido');
                    $('#priceselect').attr('checked', 'checked');
                    $(".input-pr").hide();
                } else {
                    $('#modalprecio > span.infoself').text('$' + localStorage.getItem('selfserve_price'));
                    $('#valueprice').val(localStorage.getItem('selfserve_price'));
                    $('.btn-pric').hide();
                }
            }

            if (localStorage.getItem('selfserve_image')) {
                document.getElementById("output").src = localStorage.getItem('selfserve_image');
            }

            let mediaqueryWidth = window.matchMedia("(min-width: 768px)");
            if (mediaqueryWidth.matches) {
                $('#listMaterial').listnav({
                    includeNums: true,
                    includeAll: true,
                    removeDisabled: true
                });
            }
        });

        function garmentSelectPopulate(category_id) {
            garmentSelect.find('option').not(':first').remove();
            let i;
            for (i = 0; i < arrayGarments.length; i++) {
                if (parseInt(arrayGarments[i]["catalog_garment_category_id"]) === parseInt(category_id)) {
                    let option = new Option(arrayGarments[i]["garment"], arrayGarments[i]["id"]);
                    garmentSelect.append(option);
                }
            }
        }

        function brandSelectPopulate(category_name) {
            let listbrand = $('#listBrand');
            listbrand.empty();
            let i;
            for (i = 0; i < Object.keys(arrayBrands).length; i++) {
                let indValue = Object.values(arrayBrands)[i];
                if ($.inArray(category_name, indValue["salable"]) !== -1) {
                    listbrand.append(
                        '<li>\n' +
                        '<div class="select-model selectinpt">\n' +
                        '<input type="radio" id="brand-' + indValue["id"] + '" name="brand" value="' + indValue["id"] + '">\n' +
                        '<label class="lblbrands" data-type="' + indValue["has_models"] + '" data-index="' + indValue["id"] + '" for="brand-' + indValue["id"] + '">' + indValue["brand"] + '</label><br>\n' +
                        '</div>\n' +
                        '</li>'
                    );
                }
            }
            let lblbrands = $(".lblbrands");
            lblbrands.each(function () {
                $(this).on("click", function () {
                    let brand_id = $(this).data("index");
                    let brand_has_models = $(this).data("type");
                    let brand_id_magento = $(this).data("value");
                    let infoself = $('#modalbrand > span.infoself');
                    localStorage.setItem("selfserve_brand", brand_id);
                    localStorage.setItem("selfserve_brand_magento", brand_id_magento);
                    localStorage.setItem("selfserve_brand_has_models", brand_has_models);
                    let textselect = $(this).text();
                    let txtvalue = $(this).prev().val();
                    let modalbrand = $('#modalbrand');
                    $('#modalbrand > span.titleself').text('*Marca: ');
                    infoself.text(textselect);
                    infoself.attr('value', txtvalue);
                    $('#btnacepbrand').removeClass('desactivedbotton');
                    modalbrand.addClass('btn-active-model');
                    $('#listModel-nav').remove();
                });
            });
            let mediaqueryWidth = window.matchMedia("(min-width: 768px)");
            if (mediaqueryWidth.matches) {
                listbrand.listnav({
                    includeNums: true,
                    includeAll: false,
                    removeDisabled: true
                });
            }
        }

        function modelSelectPopulate() {
            let listmodel = $('#listModel');
            let brand_id = localStorage.getItem("selfserve_brand");
            $('#divmodel').removeClass('divmodel');
            listmodel.empty();
            let i;
            for (i = 0; i < arrayModels.length; i++) {
                if (parseInt(arrayModels[i]["brand_id"]) === parseInt(brand_id)) {
                    listmodel.append(
                        '<li>\n' +
                        '<div class="select-model selectinpt">\n' +
                        '<input type="radio" id="model-' + arrayModels[i]["id"] + '" name="model" value="' + arrayModels[i]["name"] + '">\n' +
                        '<label class="lblmodel" data-index="' + arrayModels[i]["id"] + '" for="model-' + arrayModels[i]["id"] + '">' + arrayModels[i]["name"] + '</label><br>\n' +
                        '</div>\n' +
                        '</li>'
                    );
                }
            }

            $(".lblmodel").each(function () {
                $(this).on("click", function () {
                    let model_id = $(this).data("index");
                    let textmodelselect = $(this).text();
                    let modalmodel = $('#modalmodel');
                    localStorage.setItem("selfserve_model", model_id);
                    $('#modalmodel > span.titleself').text('Modelo: ');
                    $('#modalmodel > span.infoself').text(textmodelselect);
                    btnaceptmodel.removeClass('desactivedbotton');
                    modalmodel.addClass('btn-active-model');
                });
            });
            let mediaqueryWidth = window.matchMedia("(min-width: 768px)");
            if (mediaqueryWidth.matches) {
                listmodel.listnav({
                    includeNums: true,
                    includeAll: true,
                    removeDisabled: true
                });
            }
        }

        function emptyListModel() {
            let listmodel = $('#listModel');
            localStorage.removeItem("selfserve_model");
            $('#divmodel').addClass('divmodel');
            $('#modalmodel > span.titleself').text('Modelo');
            $('#modalmodel > span.infoself').text('');
            btnaceptmodel.addClass('desactivedbotton');
            $('#modalmodel').removeClass('btn-active-model');
            listmodel.empty();
        }

        const photomodal = $("#photoModal").get(0);
        $("#photoButton").on("click", function () {
            photomodal.style.display = "block";
        });
        $(".photo-modal-close").first().on("click", function () {
            photomodal.style.display = "none";
        });
        window.onclick = function (event) {
            if (event.target === photomodal) {
                photomodal.style.display = "none";
            }
        }

        const cpmodal = $("#cpModal").get(0);
        $("#modalmodel").on("click", function () {
            cpmodal.style.display = "block";
            $('.selected-model').removeClass("listselect-active");
        });
        $(".closemodel").first().on("click", function () {
            cpmodal.style.display = "none";
        });
        window.onclick = function (event) {
            if (event.target === cpmodal) {
                cpmodal.style.display = "none";
            }
        }

        const modalmat = $("#materialModal").get(0);
        $("#modalmaterial").on("click", function () {
            modalmat.style.display = "block";
            $('.selected-material').removeClass("listselect-active");
        });
        $(".closemat").first().on("click", function () {
            modalmat.style.display = "none";
        });
        window.onclick = function (event) {
            if (event.target === modalmat) {
                modalmat.style.display = "none";
            }
        }

        const modalbrand = $("#brandModal").get(0);
        $("#modalbrand").on("click", function () {
            modalbrand.style.display = "block";
            $('.selected-brand').removeClass("listselect-active");
        });
        $(".closemodal").first().on("click", function () {
            modalbrand.style.display = "none";
        });
        window.onclick = function (event) {
            if (event.target === modalbrand) {
                modalbrand.style.display = "none";
            }
        }

        const modalcolor = $("#colorModal").get(0);
        $("#modalcolor").on("click", function () {
            modalcolor.style.display = "block";
            $('.selected-color').removeClass("listselect-active");
        });
        $(".closecolor").first().on("click", function () {
            modalcolor.style.display = "none";
        });
        window.onclick = function (event) {
            if (event.target === modalcolor) {
                modalcolor.style.display = "none";
            }
        }

        const modalprecio = $("#precioModal").get(0);
        $("#modalprecio").on("click", function () {
            modalprecio.style.display = "block";
            $('.selected-color').removeClass("listselect-active");
            $('.tooltip-error-price').removeClass('tooltip-error-price-show');
            $('#precioModal').removeClass('_hide_modal');
        });
        $(".closeprecio").first().on("click", function () {
            modalprecio.style.display = "none";
        });
        window.onclick = function (event) {
            if (event.target === modalprecio) {
                modalprecio.style.display = "none";
            }
        }

        const modalestado = $("#estadoModal").get(0);
        $("#modalestado").on("click", function () {
            modalestado.style.display = "block";
            $('.selected-estado').removeClass("select-estado-active");
        });
        $(".closeestado").first().on("click", function () {
            modalestado.style.display = "none";
        });
        window.onclick = function (event) {
            if (event.target === modalestado) {
                modalestado.style.display = "none";
            }
        }

        $('#btnacepbrand').on("click", function () {
            if (parseInt(localStorage.getItem("selfserve_brand_has_models")) !== 0 && parseInt(localStorage.getItem("selfserve_category")) === 1) {
                modelSelectPopulate();
            } else {
                emptyListModel();
            }
            $('#brandModal').hide();
        });

        btnaceptmodel.on("click", function () {
            $('#cpModal').hide();
        });

        lblmaterial.each(function () {
            $(this).on("click", function () {
                let txtselectmaterial = $(this).text();
                let modalmaterial = $('#modalmaterial');
                $('#modalmaterial > span.titleself').text('Material: ');
                $('#modalmaterial > span.infoself').text(txtselectmaterial);
                $('#btnacepmaterial').removeClass('desactivedbotton');
                modalmaterial.addClass('btn-active-model');
            });
        });
        $('#btnacepmaterial').on("click", function () {
            $('#materialModal').hide();
        });

        lblcolor.each(function () {
            $(this).on("click", function () {
                $(this).addClass("listselect-active");
                let txtselectcolor = $(this).parent().find('span').text();
                let modalcolor = $('#modalcolor');
                $('.btneditcolor').remove();
                $('#modalcolor > span.titleself').text('*Color: ');
                $('#modalcolor > span.infoself').text(txtselectcolor);
                $('#btnacepcolor').removeClass('desactivedbotton');
                modalcolor.addClass('btn-active-model');
            });
        });
        $('#btnacepcolor').on("click", function () {
            $('#colorModal').hide();
        });

        $('#btnacepstatus').on("click", function () {
            $('#estadoModal').hide();
        });

        btnprice.on("click", function () {
            let condiciones = $("#priceselect").is(":checked");
            let valueprice = $('#valueprice');
            let valinput = valueprice.val().length;
            let valinputval = valueprice.val();
            let txtvalueprice = "$" + valinputval;
            if (condiciones) {
                txtvalueprice = "Desconocido";
            }
            if (valinput <= 0 && !condiciones) {
                $('.tooltip-error-price').addClass('tooltip-error-price-show');
            } else {
                $('.btneditprice').remove();
                let modalprecio = $('#modalprecio');
                $('#modalprecio > span.titleself').text('Precio original: ');
                $('#modalprecio > span.infoself').text(txtvalueprice);
                $('#price_insert').attr("value", txtvalueprice);

                modalprecio.addClass('btn-active-model');
                $('#precioModal').addClass('_hide_modal');
            }
        });

        $("#valueprice").on("keyup", function () {
            let priceorigin = $(this).val().length;
            if (priceorigin > 0) {
                btnprice.removeClass('desactivedbotton');
                $('.btn-pric').hide();
            } else {
                btnprice.addClass('desactivedbotton');
                $('.btn-pric').show();
            }
        });

        $('#priceselect').change(function () {
            if ($(this).is(":checked")) {
                btnprice.removeClass('desactivedbotton');
                $('.input-pr').hide();
            } else {
                btnprice.addClass('desactivedbotton');
                $('.input-pr').show();
            }
        });
    }
});
