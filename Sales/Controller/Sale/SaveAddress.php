<?php
/** @noinspection PhpUnused */

namespace Troquer\Sales\Controller\Sale;

use \Magento\Customer\Api\Data\CustomerInterface;
use \Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Customer\Model\Session;
use \Magento\Framework\App\Action\HttpPostActionInterface;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\Result\Redirect;
use \Magento\Framework\Controller\Result\RedirectFactory;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Webapi\Rest\Request;
use \Troquer\Inbound\Helper\Data;

class SaveAddress implements HttpPostActionInterface
{
    /**
     * @var Session
     */
    protected Session $_customerSession;

    /**
     * @var PageFactory
     */
    protected PageFactory $_resultPageFactory;

    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var CurrentCustomer
     */
    protected CurrentCustomer $_currentCustomer;

    /**
     * @var RedirectFactory
     */
    protected RedirectFactory $_redirectFactory;

    /**
     * @param Session $customerSession
     * @param Data $helper
     * @param CurrentCustomer $currentCustomer
     * @param PageFactory $resultPageFactory
     * @param Request $request
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Session $customerSession,
        Data $helper,
        CurrentCustomer $currentCustomer,
        PageFactory $resultPageFactory,
        Request $request,
        RedirectFactory $redirectFactory
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_customerSession = $customerSession;
        $this->_helper = $helper;
        $this->_currentCustomer = $currentCustomer;
        $this->_request = $request;
        $this->_redirectFactory = $redirectFactory;
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        $this->saveCustomerTroquerAddress($this->_request->getParams());
        $resultPage = $this->_redirectFactory->create();
        $resultPage->setPath('sales/sale/addresses');
        return $resultPage;
    }

    /**
     * Retrieve the Customer Data using the customer Id from the customer session.
     *
     * @return CustomerInterface
     */
    protected function getCustomer()
    {
        return $this->_currentCustomer->getCustomer();
    }

    /**
     * @param $address
     * @return mixed
     * @throws NoSuchEntityException
     */
    protected function saveCustomerTroquerAddress($address)
    {
        $street = $address["street"];
        $requestData = [
            "email" => $this->getCustomer()->getEmail(),
            "street" => $street[1],
            "external" => $street[2],
            "internal" => $street[3],
            "neighborhood" => $address["suburb"],
            "city" => $address["city"],
            "state" => $address["region"],
            "zip_code" => $address["postcode"],
            "reference" => $address["references"],
            "phone" => $address["phone"]
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/seller/update-address");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }
}
