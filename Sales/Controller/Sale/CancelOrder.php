<?php

namespace Troquer\Sales\Controller\Sale;

use \Magento\Customer\Model\Context;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use \Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\Result\Redirect;
use \Magento\Framework\Controller\Result\RedirectFactory;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Webapi\Rest\Request;
use \Troquer\Inbound\Helper\Data;
use \Zend_Http_Client;

class CancelOrder implements CsrfAwareActionInterface
{
    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var PageFactory
     */
    protected PageFactory $_resultPageFactory;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * @var Zend_Http_Client
     */
    protected Zend_Http_Client $_zendClient;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var HttpContext
     */
    protected HttpContext $_httpContext;

    /**
     * @var RedirectFactory
     */
    private RedirectFactory $_redirectFactory;

    /**
     * @param Request $request
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encrytor
     * @param Json $json
     * @param PageFactory $resultPageFactory
     * @param Data $helper
     * @param Zend_Http_Client $zendClient
     * @param HttpContext $httpContext
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Request $request,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encrytor,
        Json $json,
        PageFactory $resultPageFactory,
        Data $helper,
        Zend_Http_Client $zendClient,
        HttpContext $httpContext,
        RedirectFactory $redirectFactory
    )
    {
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $encrytor;
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_helper = $helper;
        $this->_httpContext = $httpContext;
        $this->_redirectFactory = $redirectFactory;
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        if (!(bool)$this->_httpContext->getValue(Context::CONTEXT_AUTH)) {
            $resultPage = $this->_redirectFactory->create();
            $resultPage->setPath('customer/account');
        } else {
            $this->cancelOrder($this->_request->getParams());
            $resultPage = $this->_redirectFactory->create();
            $resultPage->setPath('sales/sale/shipments');
        }

        return $resultPage;
    }

    /**
     * @param $order
     * @return mixed
     * @throws NoSuchEntityException
     */
    protected function cancelOrder($order)
    {
        $requestData = [
            "inbound_order_id" => $order["inbound_order_id"]
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/order/cancel-guide");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}
