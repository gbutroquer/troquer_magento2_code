<?php

namespace Troquer\Sales\Controller\Sale;

use \Magento\Customer\Model\Context;
use \Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use \Magento\Framework\App\Http\Context as HttpContext;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\Result\Redirect;
use \Magento\Framework\Controller\Result\RedirectFactory;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\View\Result\Page;
use \Magento\Framework\View\Result\PageFactory;

class Discounted implements HttpGetActionInterface
{
    /**
     * @var HttpContext
     */
    protected HttpContext $_httpContext;

    /**
     * @var PageFactory
     */
    protected PageFactory $_resultPageFactory;

    /**
     * @var RedirectFactory
     */
    private RedirectFactory $_redirectFactory;

    /**
     * @param HttpContext $httpContext
     * @param PageFactory $resultPageFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        HttpContext $httpContext,
        PageFactory $resultPageFactory,
        RedirectFactory $redirectFactory
    )
    {
        $this->_httpContext = $httpContext;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_redirectFactory = $redirectFactory;
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface|Page
     */
    public function execute()
    {
        if (!(bool)$this->_httpContext->getValue(Context::CONTEXT_AUTH)) {
            $resultPage = $this->_redirectFactory->create();
            $resultPage->setPath('customer/account');
        } else {
            $resultPage = $this->_resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->set(__('My Sales'));
        }

        return $resultPage;
    }
}
