<?php

namespace Troquer\Sales\Controller\Sale;

use \Magento\Customer\Api\AddressRepositoryInterface;
use \Magento\Customer\Api\Data\AddressInterface;
use \Magento\Customer\Api\Data\CustomerInterface;
use \Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Customer\Api\Data\AddressInterfaceFactory;
use \Magento\Customer\Model\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use \Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\Result\Redirect;
use \Magento\Framework\Controller\Result\RedirectFactory;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Webapi\Rest\Request;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Framework\Serialize\Serializer\Json;
use \Troquer\Inbound\Helper\Data;
use \Magento\Customer\Api\Data\RegionInterface;
use \Zend_Http_Client;
use \Troquer\Inbound\Model\Order;
use Zend_Http_Client_Exception;

class SaveOrder implements CsrfAwareActionInterface
{
    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var PageFactory
     */
    protected PageFactory $_resultPageFactory;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * @var Zend_Http_Client
     */
    protected Zend_Http_Client $_zendClient;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var HttpContext
     */
    protected HttpContext $_httpContext;

    /**
     * @var CurrentCustomer
     */
    protected CurrentCustomer $_currentCustomer;

    /**
     * @var RedirectFactory
     */
    protected RedirectFactory $_redirectFactory;

    /**
     * @var AddressRepositoryInterface
     */
    protected AddressRepositoryInterface $_addressRepository;

    /**
     * @var AddressInterfaceFactory
     */
    protected AddressInterfaceFactory $_addressFactory;

    /**
     * @var RegionInterface
     */
    protected RegionInterface $_regionInterface;

    /**
     * @var Order
     */
    protected Order $_order;

    /**
     * @param Request $request
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encrytor
     * @param Json $json
     * @param PageFactory $resultPageFactory
     * @param Data $helper
     * @param CurrentCustomer $currentCustomer
     * @param Zend_Http_Client $zendClient
     * @param HttpContext $httpContext
     * @param AddressRepositoryInterface $addressRepository
     * @param AddressInterfaceFactory $addressFactory
     * @param RegionInterface $regionInterface
     * @param Order $order
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Request $request,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encrytor,
        Json $json,
        PageFactory $resultPageFactory,
        Data $helper,
        CurrentCustomer $currentCustomer,
        Zend_Http_Client $zendClient,
        HttpContext $httpContext,
        AddressRepositoryInterface $addressRepository,
        AddressInterfaceFactory $addressFactory,
        RegionInterface $regionInterface,
        Order $order,
        RedirectFactory $redirectFactory
    )
    {
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $encrytor;
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_currentCustomer = $currentCustomer;
        $this->_helper = $helper;
        $this->_httpContext = $httpContext;
        $this->_addressRepository = $addressRepository;
        $this->_redirectFactory = $redirectFactory;
        $this->_regionInterface = $regionInterface;
        $this->_order = $order;
        $this->_addressFactory = $addressFactory;
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     * @throws LocalizedException
     * @throws Zend_Http_Client_Exception
     */
    public function execute()
    {
        if (!(bool)$this->_httpContext->getValue(Context::CONTEXT_AUTH)) {
            $resultPage = $this->_redirectFactory->create();
            $resultPage->setPath('customer/account');
        } else {
            $params = $this->_request->getParams();
            $this->saveCustomerTroquerAddress($params);
            $this->saveOrder($params);
            $resultPage = $this->_redirectFactory->create();
            $resultPage->setPath('vende/hazlo-en-linea/confirmacion-guia');
        }
        return $resultPage;
    }

    /**
     * @return CustomerInterface
     */
    protected function getCustomer(): CustomerInterface
    {
        return $this->_currentCustomer->getCustomer();
    }

    /**
     * @param $order
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    protected function saveOrder($order)
    {
        $requestData = [
            "inbound_order_id" => $order["inbound_order_id"]
        ];
        return $this->_order->completeOrder($requestData);
    }

    /**
     * @param $address
     * @throws LocalizedException
     */
    protected function saveCustomerTroquerAddress($address)
    {
        if($this->getSalesAddressId()) {
            $sales_address = $this->getSalesAddress();
            $street = $sales_address->getStreet();
            $street[0] = $address["street"];
            $street[1] = $address["outside"];
            $street[2] = $address["inside"];
            $sales_address->setStreet($street);
            $sales_address->setCustomAttribute("suburb", $address["suburb"]);
            $sales_address->setCity($address["city"]);
            $sales_address->setRegion($this->_regionInterface->setRegionId($address["region_id"])->setRegion($address["region"]));
            $sales_address->setRegionId($address["region_id"]);
            $sales_address->setPostcode($address["postcode"]);
            $sales_address->setCustomAttribute("references", $address["references"]);
            $sales_address->setTelephone($address["phone"]);
            $this->_addressRepository->save($sales_address);
        } else {
            $customer = $this->getCustomer();
            $sales_address = $this->_addressFactory->create();
            $street = array();
            $street[0] = $address["street"];
            $street[1] = $address["outside"];
            $street[2] = $address["inside"];
            $sales_address->setFirstname($customer->getFirstname());
            $sales_address->setLastname($customer->getLastname());
            $sales_address->setCustomerId($customer->getId());
            $sales_address->setStreet($street);
            $sales_address->setCustomAttribute("suburb", $address["suburb"]);
            $sales_address->setCity($address["city"]);
            $sales_address->setRegion($this->_regionInterface->setRegionId($address["region_id"])->setRegion($address["region"]));
            $sales_address->setRegionId($address["region_id"]);
            $sales_address->setCountryId($address["country_id"]);
            $sales_address->setPostcode($address["postcode"]);
            $sales_address->setCustomAttribute("references", $address["references"]);
            $sales_address->setTelephone($address["phone"]);
            $sales_address->setCustomAttribute("is_for_sales", true);
            $sales_address->setIsDefaultBilling(false);
            $sales_address->setIsDefaultShipping(false);

            $this->_addressRepository->save($sales_address);
        }
    }

    /**
     * @return int|null
     */
    protected function getSalesAddressId(): ?int
    {
        $customer = $this->getCustomer();
        if ($customer === null) {
            return null;
        } else {
            foreach ($customer->getAddresses() as $addr) {
                if ($addr->getCustomAttribute('is_for_sales') && $addr->getCustomAttribute('is_for_sales')->getValue()) {
                    return $addr->getId();
                }
            }
            return null;
        }
    }

    /**
     * @return AddressInterface|null
     * @throws LocalizedException
     */
    public function getSalesAddress(): ?AddressInterface
    {
        $addressId = $this->getSalesAddressId();
        if($addressId) {
            return $this->_addressRepository->getById($addressId);
        }
        return null;
    }

    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}
