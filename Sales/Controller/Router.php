<?php

namespace Troquer\Sales\Controller;

use \Magento\Framework\App\Action\Forward;
use \Magento\Framework\App\ActionFactory;
use \Magento\Framework\App\ActionInterface;
use \Magento\Framework\App\RequestInterface;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\App\RouterInterface;
use \Magento\Framework\DataObject;
use \Magento\Framework\Url;

class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private ActionFactory $_actionFactory;

    /**
     * @var ResponseInterface
     */
    private ResponseInterface $_response;

    /**
     * Router constructor.
     * @param ActionFactory $actionFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ResponseInterface $response
    )
    {
        $this->_actionFactory = $actionFactory;
        $this->_response = $response;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');
        $condition = new DataObject(['identifier' => $identifier, 'continue' => true]);
        $identifier = $condition->getIdentifier();

        $position = strpos($identifier, "vende");
        if ($position !== false && $position === 0) {
            switch (true) {
                case $identifier === 'vende':
                    $request->setModuleName('sales')->setControllerName('sale')
                        ->setActionName('presell');
                    break;
                case $identifier === 'vende/lead':
                    $request->setModuleName('sales')->setControllerName('sale')
                        ->setActionName('vende');
                    break;
                case $identifier === 'vende/hazlo-en-linea/ganancia-estimada':
                    $request->setModuleName('sales')->setControllerName('sale')
                        ->setActionName('selfservevalue');
                    break;
                case $identifier === 'vende/hazlo-en-linea/direccion':
                    $request->setModuleName('sales')->setControllerName('sale')
                        ->setActionName('selfserveshipping');
                    break;
                case $identifier === 'vende/hazlo-en-linea/confirmacion-guia':
                    $request->setModuleName('sales')->setControllerName('sale')
                        ->setActionName('selfserveshippinguide');
                    break;
                case $identifier === 'vende/hazlo-en-linea':
                default:
                    $request->setModuleName('sales')->setControllerName('sale')
                        ->setActionName('selfserve');
                    break;
            }

            $request->setAlias(Url::REWRITE_REQUEST_PATH_ALIAS, $identifier);
            return $this->_actionFactory->create(Forward::class);
        }

        return null;
    }
}
