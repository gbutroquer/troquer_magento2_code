<?php
/** @noinspection PhpUnused */

namespace Troquer\Sales\Block\Sale;

use \Magento\Customer\Api\Data\CustomerInterface;
use \Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;

class Vende extends Template
{
    /**
     * @var CurrentCustomer
     */
    protected CurrentCustomer $_currentCustomer;

    /**
     * @param Context $context
     * @param CurrentCustomer $currentCustomer
     * @param array $data
     */
    public function __construct(
        Context $context,
        CurrentCustomer $currentCustomer,
        array $data = []
    )
    {
        $this->_currentCustomer = $currentCustomer;
        parent::__construct($context, $data);
    }

    /**
     * Returns the Magento Customer Model for this block
     *
     * @return CustomerInterface|null
     */
    public function getCustomer()
    {
        return $this->_currentCustomer->getCustomer();
    }

    /**
     * Returns the Magento Customer Model Postcode for this block
     *
     * @return String
     */
    public function getPostCode()
    {
        $customer = $this->getCustomer();
        $postcode = $customer->getCustomAttribute('postcode');
        return $postcode ? $postcode->getValue() : '';
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Appointments'));
    }

    /**
     * @return Vende
     */
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    /**
     * Get Pager child block output
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Get customer account URL
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }
}
