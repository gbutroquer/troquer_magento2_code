<?php
/** @noinspection PhpUndefinedMethodInspection */

namespace Troquer\Sales\Block\Sale;

use \DateInterval;
use \Exception;
use \Magento\Catalog\Api\Data\ProductInterface;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\Catalog\Model\ResourceModel\Product\Collection;
use \Magento\Customer\Model\Session;
use \Magento\Framework\DataObject;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use \Magento\Framework\Stdlib\DateTime;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactoryInterface;
use \Magento\Sales\Model\ResourceModel\Order\Collection as OrderCollection;
use \Magento\Theme\Block\Html\Pager;
use \Troquer\Inbound\Helper\Data;
use \Throwable;
use \Magento\Framework\Data\CollectionFactory as InboundProductCollectionFactory;

class History extends Template
{
    /**
     * @var Session
     */
    protected Session $_customerSession;

    /**
     * @var \Magento\Framework\Data\Collection|null
     */
    protected ?\Magento\Framework\Data\Collection $_processed = null;

    /**
     * @var \Magento\Framework\Data\Collection|null
     */
    protected ?\Magento\Framework\Data\Collection $_rejected = null;

    /**
     * @var \Magento\Framework\Data\Collection|null
     */
    protected ?\Magento\Framework\Data\Collection $_returned = null;

    /**
     * @var InboundProductCollectionFactory
     */
    protected InboundProductCollectionFactory $_inboundCollectionFactory;

    /**
     * @var OrderCollection|null
     */
    protected ?OrderCollection $_sold = null;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $_productCollectionFactory;

    /**
     * @var Collection|null
     */
    protected ?Collection $_published = null;

    /**
     * @var Collection|null
     */
    protected ?Collection $_discounted = null;

    /**
     * @var Collection|null
     */
    protected ?Collection $_expired = null;

    /**
     * @var ProductRepositoryInterface
     */
    protected ProductRepositoryInterface $_productRepository;

    /**
     * @var CollectionFactoryInterface
     */
    protected CollectionFactoryInterface $_orderCollectionFactory;

    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var TimezoneInterface
     */
    protected TimezoneInterface $_date;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param CollectionFactory $orderCollectionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param InboundProductCollectionFactory $inboundCollectionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param Data $helper
     * @param TimezoneInterface $date
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CollectionFactory $orderCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        InboundProductCollectionFactory $inboundCollectionFactory,
        ProductRepositoryInterface $productRepository,
        Data $helper,
        TimezoneInterface $date,
        array $data = []
    )
    {
        $this->_customerSession = $customerSession;
        $this->_inboundCollectionFactory = $inboundCollectionFactory;
        $this->_productRepository = $productRepository;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_helper = $helper;
        $this->_date = $date;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->_helper->getStoreUrl() . 'media/catalog/product';
    }

    /**
     * @param $productId
     * @return ProductInterface
     * @throws NoSuchEntityException
     */
    public function getProduct($productId)
    {
        return $product = $this->_productRepository->getById($productId);
    }

    /**
     * Get customer orders
     *
     * @return bool|OrderCollection
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getSold()
    {
        $troquerId = $this->getCustomerSessionTroquerId();
        if ($troquerId) {
            if (!($customerId = $this->_customerSession->getCustomerId())) {
                return false;
            }
            if (!$this->_sold) {
                $this->_sold = $this->_orderCollectionFactory->create()->addFieldToSelect(
                    'status'
                )->addFieldToFilter(
                    'status',
                    ['in' => ['complete', 'processing', 'in_transit']]
                )->setOrder(
                    'created_at',
                    'desc'
                );
                $this->_sold->getSelect()
                    ->joinLeft(
                        ["soi" => "sales_order_item"],
                        'main_table.entity_id = soi.order_id',
                        array('*')
                    )
                    ->where('soi.qty_canceled = 0 and soi.qty_refunded = 0 and soi.sku like "' . $troquerId . '%"');
            }
        } else {
            return false;
        }
        return $this->_sold;
    }

    /**
     * Get customer orders
     *
     * @return bool|Collection
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getExpired()
    {
        $troquerId = $this->getCustomerSessionTroquerId();
        if ($troquerId) {
            $now = new DateTime();
            $now->sub(new DateInterval('P10M'));
            if (!($customerId = $this->_customerSession->getCustomerId())) {
                return false;
            }
            if (!$this->_expired) {
                $this->_expired = $this->_productCollectionFactory->create()->addFieldToSelect(
                    '*'
                )->addAttributeToFilter(
                    'sku',
                    array('like' => $troquerId . '%')
                )->addAttributeToFilter(
                    'status',
                    1
                )->addAttributeToFilter(
                    'publication_date',
                    ['lteq' => $now->format('Y-m-d H:i:s')]
                )->setOrder(
                    'publication_date',
                    'desc'
                );
            }
        } else {
            return false;
        }
        return $this->_expired;
    }

    /**
     * Get customer orders
     *
     * @return bool|Collection
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getPublished()
    {
        $troquerId = $this->getCustomerSessionTroquerId();
        $sold = $this->getSold();
        $soldIds = [];
        if($sold) {
            foreach ($sold as $product) {
                array_push($soldIds, $product->getData('product_id'));
            }
        }
        if ($troquerId) {
            if (!($customerId = $this->_customerSession->getCustomerId())) {
                return false;
            }
            if (!$this->_published) {
                $this->_published = $this->_productCollectionFactory->create()->addFieldToSelect(
                    '*'
                )->addAttributeToFilter(
                    'sku',
                    array('like' => $troquerId . '%')
                );

                if (count($soldIds)) {
                    $this->_published->addAttributeToFilter(
                        'entity_id',
                        array('nin' => $soldIds));
                }

                $this->_published->addAttributeToFilter(
                    'status',
                    1
                )->setOrder(
                    'publication_date',
                    'desc'
                );
            }
        } else {
            return false;
        }
        return $this->_published;
    }

    /**
     * @return string
     */
    public function getActualDate()
    {
        return $this->_date->date()->format('Y-m-d H:i:s');
    }

    /**
     * Get customer orders
     *
     * @return bool|Collection
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getDiscounted()
    {
        $day = (new \DateTime())->format('d');
        if ($day < 15) {
            $date = (new \DateTime())->sub(new DateInterval('P1M'));
        } else {
            $date = (new \DateTime());
        }
        $month = $date->format(  'm');
        $year = $date->format(  'Y');
        $date = (new \DateTime())->setDate($year, $month, 15)->setTime(23, 59, 59);
        $now = $date->format(DateTime::DATETIME_PHP_FORMAT);
        $troquerId = $this->getCustomerSessionTroquerId();
        if ($troquerId) {
            if (!($customerId = $this->_customerSession->getCustomerId())) {
                return false;
            }
            if (!$this->_discounted) {
                $this->_discounted = $this->getPublished();

                $clothesandshoes = clone $this->_discounted;
                $clothesandshoesDate = date('Y-m-d H:i:s', strtotime('-5 month', strtotime($now)));
                $accesoriesandbags = clone $this->_discounted;
                $accesoriesandbagsDate = date('Y-m-d H:i:s', strtotime('-4 month', strtotime($now)));

                $clothesandshoes->addAttributeToFilter(
                    'attribute_set_id',
                    array('nin' => [10, 11, 12, 13, 14, 15])
                );
                $clothesandshoes->addAttributeToFilter(
                    'publication_date',
                    ['lteq' => $clothesandshoesDate]
                );

                $accesoriesandbags->addAttributeToFilter(
                    'attribute_set_id',
                    array('in' => [10, 11, 12, 13, 14, 15])
                );
                $accesoriesandbags->addAttributeToFilter(
                    'publication_date',
                    ['lteq' => $accesoriesandbagsDate]
                );

                $productIds = [];
                foreach ($clothesandshoes as $product) {
                    array_push($productIds, $product->getId());
                }
                foreach ($accesoriesandbags as $product) {
                    array_push($productIds, $product->getId());
                }

                $this->_discounted = $this->_productCollectionFactory->create()->addFieldToSelect(
                    '*'
                )->addAttributeToFilter(
                    'price',
                    ['lt' => 40000]
                )
                    ->addAttributeToFilter(
                        'row_id',
                        array('in' => $productIds)
                    );

                $this->_discounted->addAttributeToFilter(
                    'status',
                    1
                )->setOrder(
                    'publication_date',
                    'desc'
                );
            }
        } else {
            return false;
        }
        return $this->_discounted;
    }

    /**
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getDiscountedSkus()
    {
        $discounted = $this->getDiscounted();
        $skus = [];
        if($discounted) {
            foreach ($discounted as $product) {
                array_push($skus, $product->getSku());
            }
        }

        return $skus;
    }

    /**
     * @return mixed
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerSessionTroquerId()
    {
        $troquerId = $this->_customerSession->getTroquerId();
        if (!$troquerId && $this->_customerSession->isLoggedIn()) {
            $customerData = $this->getCustomerTroquerId($this->_customerSession->getCustomerData()->getEmail());
            $troquerId = $customerData[0]->seller_code;
            $this->_customerSession->setTroquerId($troquerId);
        }

        return $troquerId;
    }

    /**
     * @return \Magento\Framework\Data\Collection|null
     * @throws NoSuchEntityException
     */
    public function getProcessed()
    {
        $this->_processed = $this->_inboundCollectionFactory->create();
        $processed = $this->getApiProducts([5, 6, 9, 11, 12, 13]);
        foreach ($processed[0] as $item) {
            $varienObject = new DataObject();
            $varienObject->addData((array)$item);
            try {
                $this->_processed->addItem($varienObject);
            } catch (Exception $e) {
            }
        }
        return $this->_processed;
    }

    /**
     * @return \Magento\Framework\Data\Collection|null
     * @throws NoSuchEntityException
     */
    public function getRejected()
    {
        $this->_rejected = $this->_inboundCollectionFactory->create();
        $processed = $this->getApiProducts([4]);
        foreach ($processed[0] as $item) {
            $varienObject = new DataObject();
            $varienObject->addData((array)$item);
            try {
                $this->_rejected->addItem($varienObject);
            } catch (Exception $e) {
            }
        }
        return $this->_rejected;
    }

    /**
     * @return \Magento\Framework\Data\Collection|null
     * @throws NoSuchEntityException
     */
    public function getReturned()
    {
        $this->_returned = $this->_inboundCollectionFactory->create();
        $processed = $this->getApiProducts([10]);
        foreach ($processed[0] as $item) {
            $varienObject = new DataObject();
            $varienObject->addData((array)$item);
            try {
                $this->_returned->addItem($varienObject);
            } catch (Exception $e) {
            }
        }
        return $this->_returned;
    }

    /**
     * @param $status
     * @return mixed
     * @throws NoSuchEntityException
     */
    protected function getApiProducts($status)
    {
        $requestData = [
            "search" => $this->_customerSession->getCustomer()->getEmail(),
            "status" => $status
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/product/get-status");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @param $email
     * @return mixed
     * @throws NoSuchEntityException
     */
    protected function getCustomerTroquerId($email)
    {
        $requestData = [
            "email" => $email
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/seller/get-email");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @return mixed|string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerHasReturned()
    {
        if ($this->_customerSession->isLoggedIn()) {
            $folio = $this->getCustomerSessionTroquerId();
            $requestData = [
                "folio" => $folio
            ];
            $token = $this->_helper->getApiToken();
            $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/product/get-troquer-sku");
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
            $result = curl_exec($ch);
            return json_decode($result);
        }
        return '';
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     * @throws Exception
     */
    protected function _prepareLayout()
    {
        switch ($this->_template) {
            case 'Troquer_Sales::sale/deserted.phtml':
                $blockName = 'sales.sale.deserted.pager';
                $collection = $this->getReturned();
                break;
            case 'Troquer_Sales::sale/expired.phtml':
                $blockName = 'sales.sale.expired.pager';
                $collection = $this->getExpired();
                break;
            case 'Troquer_Sales::sale/processed.phtml':
                $blockName = 'sales.sale.processed.pager';
                $collection = $this->getProcessed();
                break;
            case 'Troquer_Sales::sale/published.phtml':
                $blockName = 'sales.sale.published.pager';
                $collection = $this->getPublished();
                break;
            case 'Troquer_Sales::sale/discounted.phtml':
                $blockName = 'sales.sale.discounted.pager';
                $collection = $this->getDiscounted();
                break;
            case 'Troquer_Sales::sale/rejected.phtml':
                $blockName = 'sales.sale.rejected.pager';
                $collection = $this->getRejected();
                break;
            default:
                $blockName = 'sales.sale.sold.pager';
                $collection = $this->getSold();
                break;
        }

        if($collection && $collection->getSize()) {
            try {
                $pager = $this->getLayout()->getBlock(
                    $blockName
                )->setCollection(
                    $collection
                );
                $this->setChild('pager', $pager);
                $collection->load();
            } catch (Throwable $t) {
                $pager = $this->getLayout()->createBlock(
                    Pager::class,
                    $blockName
                )->setCollection(
                    $collection
                );
                $this->setChild('pager', $pager);
                $collection->load();
            }
        }

        return $this;
    }

    /**
     * Get Pager child block output
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Get customer account URL
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }

    /**
     * Return the Url for saving.
     *
     * @return string
     */
    public function getRejectSkuUrl()
    {
        return $this->_urlBuilder->getUrl(
            'rest/V1'
        );
    }

    /**
     * @param $product
     * @return int
     * @throws Exception
     */
    public function getProductDiscount($product)
    {
        $discount = 0;
        $publicationDate = $product->getData('publication_date');
        $attributeSetId = $product->getAttributeSetId();
        $publicationDate = new \DateTime($publicationDate);
        $day = (new \DateTime())->format('d');
        if ($day < 15) {
            $date = (new \DateTime())->sub(new DateInterval('P1M'));
        } else {
            $date = (new \DateTime());
        }
        $month = $date->format(  'm');
        $year = $date->format(  'Y');
        $date = (new \DateTime())->setDate($year, $month, 15)->setTime(23, 59, 59);
        $diff = $publicationDate->diff($date);
        $price = $product->getPrice();
        $months = (int)$diff->format('%y') * 12 + (int)$diff->format('%m');
        if ($price >= 40000) {
            return $discount;
        }

        if ($attributeSetId == 10 || $attributeSetId == 11 || $attributeSetId == 12 || $attributeSetId == 13 || $attributeSetId == 14 || $attributeSetId == 15) { //Bolsas y Accesorios
            if ($price >= 20000 && $months > 5) {
                $months = 5;
            }
            if ($months == 4) {
                $discount = 20;
            } else if ($months == 5) {
                $discount = 30;
            } else if ($months == 6) {
                $discount = 40;
            } else if ($months >= 7) {
                $discount = 50;
            }
        } else {//Ropa y Zapatos
            if ($price >= 20000 && $months > 7) {
                $months = 7;
            }
            if ($months >= 5 && $months < 7) {
                $discount = 20;
            } else if ($months == 7) {
                $discount = 30;
            } else if ($months == 8) {
                $discount = 40;
            } else if ($months >= 9) {
                $discount = 50;
            }
        }

        if ($discount == 50 && $price * (100 - $discount) / 100 < 450) {
            $discount = 40;
        }
        if ($discount == 40 && $price * (100 - $discount) / 100 < 450) {
            $discount = 30;
        }
        if ($discount == 30 && $price * (100 - $discount) / 100 < 450) {
            $discount = 20;
        }
        if ($discount == 20 && $price * (100 - $discount) / 100 < 450) {
            $discount = 0;
        }

        return $discount;
    }
}
