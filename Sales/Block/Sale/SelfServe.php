<?php
/** @noinspection PhpUnused */

namespace Troquer\Sales\Block\Sale;

use \Magento\Customer\Model\Customer;
use \Magento\Framework\App\Http\Context as HttpContext;
use \Exception;
use \Throwable;
use \Magento\Framework\Data\Collection;
use \Magento\Framework\Data\CollectionFactory as InboundProductCollectionFactory;
use \Magento\Framework\DataObject;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\UrlInterface;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactoryInterface;
use \Magento\Theme\Block\Html\Pager;
use \Troquer\Inbound\Helper\Data;
use \Magento\Sales\Model\ResourceModel\Order\Collection as OrderCollection;
use \Magento\Customer\Model\Session;

class SelfServe extends Template
{
    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var UrlInterface
     */
    protected UrlInterface $_url;

    /**
     * @var HttpContext
     */
    protected HttpContext $_httpContext;

    /**
     * @var CollectionFactoryInterface
     */
    protected CollectionFactoryInterface $_orderCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $_productCollectionFactory;

    /**
     * @var OrderCollection|null
     */
    protected ?OrderCollection $_sold = null;

    /**
     * @var Session
     */
    protected Session $_customerSession;

    /**
     * @var Collection|null
     */
    protected ?Collection $_received = null;

    /**
     * @var Collection|null
     */
    protected ?Collection $_transit = null;

    /**
     * @var Collection|null
     */
    protected ?Collection $_expiredShipment = null;

    /**
     * @var InboundProductCollectionFactory
     */
    protected InboundProductCollectionFactory $_inboundCollectionFactory;

    /**
     * @param Context $context
     * @param Data $helper
     * @param UrlInterface $url
     * @param HttpContext $httpContext
     * @param CollectionFactory $orderCollectionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param InboundProductCollectionFactory $inboundCollectionFactory
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $helper,
        UrlInterface $url,
        HttpContext $httpContext,
        CollectionFactory $orderCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        InboundProductCollectionFactory $inboundCollectionFactory,
        Session $customerSession,
        array $data = []
    )
    {
        $this->_httpContext = $httpContext;
        $this->_helper = $helper;
        $this->_url = $url;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_inboundCollectionFactory = $inboundCollectionFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getAttributesByCode()
    {
        $requestData = [
            "catalog" => ["gender", "garment-categories", "garments", "brands", "materials", "colors", "conditions", "models"]
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/catalog/catalogs");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return (bool)$this->_httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

    /**
     * Get sold products
     *
     * @return OrderCollection
     */
    public function getSoldProducts()
    {
        if (!$this->_sold) {
            $this->_sold = $this->_orderCollectionFactory->create()->addFieldToSelect(
                'status'
            )->addFieldToFilter(
                'status',
                ['in' => ['complete', 'processing']]
            )->setOrder(
                'created_at',
                'desc'
            );
            $this->_sold->getSelect()
                ->joinLeft(
                    ["soi" => "sales_order_item"],
                    'main_table.entity_id = soi.order_id',
                    array('*')
                )
                ->where('soi.qty_canceled = 0 and soi.qty_refunded = 0');
        }

        return $this->_sold;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->_customerSession->getCustomer();
    }

    /**
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function newOrder()
    {
        $customer = $this->getCustomer();
        $gender = $customer->getData("gender");
        if ((int)$gender == 2) {
            $gender = "W";
        } else {
            $gender = "M";
        }
        $requestData = [
            "email" => $customer->getEmail(),
            "first_name" => $customer->getData("firstname"),
            "last_name" => $customer->getData("lastname"),
            "gender" => $gender
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/order/new");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @param $products
     * @return float
     */
    protected function productFilters($products)
    {
        $products->clear();
        $fromAndJoin = $products->getSelect()->getPart('FROM');
        $updatedfromAndJoin = [];
        foreach ($fromAndJoin as $key => $index) {
            if ($key == 'stock_status_index') {
                $index['joinType'] = 'left join';
            }
            $updatedfromAndJoin[$key] = $index;
        }
        $products->getSelect()->setPart('FROM', $updatedfromAndJoin);

        $where = $products->getSelect()->getPart('where');
        $updatedWhere = [];
        foreach ($where as $key => $condition) {
            $updatedWhere[] = str_replace("AND (stock_status_index.stock_status = 1)", "", $condition);
        }
        $products->getSelect()->setPart('where', $updatedWhere);
        $products->load();

        $prices = [];
        foreach ($products as $product) {
            array_push($prices, $product->getData('price'));
        }
        $averagePrice = 0.0;
        if (count($prices)) {
            $averagePrice = array_sum($prices) / count($prices);
        }

        return $averagePrice;
    }

    /**
     * @param $orderId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getOrder($orderId)
    {
        $requestData = [
            "inbound_order_id" => $orderId
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/order/get");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @param $status
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getOrderByStatus($status)
    {
        $customer = $this->getCustomer();
        $requestData = [
            "email" => $customer->getEmail(),
            "status" => $status
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/order/get-by-status");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @param $orderId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function createSlaveGuide($orderId)
    {
        $requestData = [
            "inbound_order_id" => $orderId
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/order/slave-guide");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @param $orderId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function cancelGuide($orderId)
    {
        $requestData = [
            "inbound_order_id" => $orderId
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/order/cancel-guide");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @return Collection|null
     * @throws NoSuchEntityException
     */
    public function getReceived()
    {
        $this->_received = $this->_inboundCollectionFactory->create();
        $received = $this->getOrderByStatus([3, 5, 7]);
        foreach ($received[0] as $item) {
            $varienObject = new DataObject();
            $varienObject->addData((array)$item);
            try {
                $this->_received->addItem($varienObject);
            } catch (Exception $e) {
            }
        }
        return $this->_received;
    }

    /**
     * @return Collection|null
     * @throws NoSuchEntityException
     */
    public function getTransit()
    {
        $this->_transit = $this->_inboundCollectionFactory->create();
        $transit = $this->getOrderByStatus([2]);
        foreach ($transit[0] as $item) {
            $varienObject = new DataObject();
            $varienObject->addData((array)$item);
            try {
                $this->_transit->addItem($varienObject);
            } catch (Exception $e) {
            }
        }
        return $this->_transit;
    }

    /**
     * @return Collection|null
     * @throws NoSuchEntityException
     */
    public function getExpiredShipment()
    {
        $this->_expiredShipment = $this->_inboundCollectionFactory->create();
        $expiredShipment = $this->getOrderByStatus([4, 6]);
        foreach ($expiredShipment[0] as $item) {
            $varienObject = new DataObject();
            $varienObject->addData((array)$item);
            try {
                $this->_expiredShipment->addItem($varienObject);
            } catch (Exception $e) {
            }
        }
        return $this->_expiredShipment;
    }

    /**
     * @inheritDoc
     * @throws LocalizedException
     * @throws Exception
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        switch ($this->_template) {
            case 'Troquer_Sales::sale/received.phtml':
                $blockName = 'sales.sale.received.pager';
                $collection = $this->getReceived();
                break;
            case 'Troquer_Sales::sale/transit.phtml':
                $blockName = 'sales.sale.transit.pager';
                $collection = $this->getTransit();
                break;
            case 'Troquer_Sales::sale/expiredshipment.phtml':
                $blockName = 'sales.sale.expiredshipment.pager';
                $collection = $this->getExpiredShipment();
                break;
            default:
                $blockName = '';
                $collection = null;
                break;
        }

        if ($collection && $collection->getSize()) {
            try {
                $pager = $this->getLayout()->getBlock(
                    $blockName
                )->setCollection(
                    $collection
                );
                $this->setChild('pager', $pager);
                $collection->load();
            } catch (Throwable $t) {
                $pager = $this->getLayout()->createBlock(
                    Pager::class,
                    $blockName
                )->setCollection(
                    $collection
                );
                $this->setChild('pager', $pager);
                $collection->load();
            }
        }

        return parent::_prepareLayout();
    }

    /**
     * Get Pager child block output
     *
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
