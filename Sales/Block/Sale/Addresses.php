<?php
/** @noinspection PhpDeprecationInspection */
/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedClassInspection */

namespace Troquer\Sales\Block\Sale;

use \Exception;
use \Magento\Customer\Api\AddressMetadataInterface;
use \Magento\Customer\Api\AddressRepositoryInterface;
use \Magento\Customer\Api\Data\AddressInterface;
use \Magento\Customer\Api\Data\AddressInterfaceFactory;
use \Magento\Customer\Api\Data\CustomerInterface;
use \Magento\Customer\Block\Address\Edit;
use \Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Customer\Model\Address;
use \Magento\Customer\Model\Session;
use \Magento\Directory\Model\CountryFactory;
use \Magento\Directory\Model\ResourceModel\Region\CollectionFactory;
use \Magento\Directory\Model\ResourceModel\Country\CollectionFactory as CountryCollectionFactory;
use \Magento\Framework\Api\DataObjectHelper;
use \Magento\Framework\App\Cache\Type\Config;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Json\EncoderInterface;
use \Magento\Framework\View\Element\Template\Context;
use \Troquer\Inbound\Helper\Data;
use \Magento\Customer\Model\AddressFactory;

class Addresses extends Edit
{
    /**
     * @var CountryFactory
     */
    protected CountryFactory $_countryFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfigInterface;

    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var AddressFactory
     */
    protected AddressFactory $_addressFactory;

    /**
     * @param Context $context
     * @param \Magento\Directory\Helper\Data $directoryHelper
     * @param EncoderInterface $jsonEncoder
     * @param Config $configCacheType
     * @param CollectionFactory $regionCollectionFactory
     * @param CountryCollectionFactory $countryCollectionFactory
     * @param Session $customerSession
     * @param AddressRepositoryInterface $addressRepository
     * @param AddressInterfaceFactory $addressDataFactory
     * @param CurrentCustomer $currentCustomer
     * @param DataObjectHelper $dataObjectHelper
     * @param CountryFactory $countryFactory
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param Data $helper
     * @param AddressFactory $addressFactory
     * @param AddressMetadataInterface|null $addressMetadata
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Directory\Helper\Data $directoryHelper,
        EncoderInterface $jsonEncoder,
        Config $configCacheType,
        CollectionFactory $regionCollectionFactory,
        CountryCollectionFactory $countryCollectionFactory,
        Session $customerSession,
        AddressRepositoryInterface $addressRepository,
        AddressInterfaceFactory $addressDataFactory,
        CurrentCustomer $currentCustomer,
        DataObjectHelper $dataObjectHelper,
        CountryFactory $countryFactory,
        ScopeConfigInterface $scopeConfigInterface,
        Data $helper,
        AddressFactory $addressFactory,
        AddressMetadataInterface $addressMetadata = null,
        array $data = []
    )
    {
        $this->_countryFactory = $countryFactory;
        $this->_scopeConfigInterface = $scopeConfigInterface;
        $this->_helper = $helper;
        $this->_customerSession = $customerSession;
        $this->_addressFactory = $addressFactory;
        parent::__construct(
            $context,
            $directoryHelper,
            $jsonEncoder,
            $configCacheType,
            $regionCollectionFactory,
            $countryCollectionFactory,
            $customerSession,
            $addressRepository,
            $addressDataFactory,
            $currentCustomer,
            $dataObjectHelper,
            $data,
            $addressMetadata
        );
    }

    /**
     * Return the specified region id
     *
     * @param string $region
     * @return string
     */
    public function getRegionByCode(string $region): string
    {
        $regionCode = $this->_regionCollectionFactory->create()
            ->addRegionNameFilter($region)
            ->getFirstItem()
            ->toArray();
        return isset($regionCode['region_id']) ? $regionCode['region_id'] : '0';
    }

    /**
     * Get customer's default sales address
     *
     * @return int|null
     */
    private function getSalesAddressId()
    {
        $customer = $this->getCustomer();
        if ($customer === null) {
            return null;
        } else {
            foreach ($customer->getAddresses() as $addr) {
                if ($addr->getCustomAttribute('is_for_sales') && $addr->getCustomAttribute('is_for_sales')->getValue()) {
                    return $addr->getId();
                }
            }
            return $this->getDefaultShipping();
        }
    }

    /**
     * @return AddressInterface|Address
     * @throws LocalizedException
     */
    public function getSalesAddress() {
        $addressId = $this->getSalesAddressId();
        if($addressId) {
            return $this->_addressRepository->getById($addressId);
        } else {
            return $this->_addressFactory->create();
        }
    }

    /**
     * Get customer's default shipping address
     *
     * @return int|null
     */
    private function getDefaultShipping()
    {
        $customer = $this->getCustomer();
        if ($customer === null) {
            return null;
        } else {
            return $customer->getDefaultShipping();
        }
    }

    /**
     * Return the allowed countries.
     *
     * @return string
     */
    public function getAllowedCountries(): string
    {
        $allowedCountries = "'MX'";
        try {
            $allowedCountries = explode(',', $this->_scopeConfigInterface->getValue('general/country/allow'));
            $allowedCountries = "'" . implode("','", $allowedCountries) . "'";
        } catch (Exception $exception) {
            $this->_logger->error($exception);
        }
        return $allowedCountries;
    }

    /**
     * @param $countryCode
     * @return string
     */
    public function getCountryByCode($countryCode)
    {
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }

    /**
     * Retrieve the Customer Data using the customer Id from the customer session.
     *
     * @return CustomerInterface
     */
    public function getCustomer()
    {
        return $this->currentCustomer->getCustomer();
    }

    /**
     * Return the Url for saving.
     *
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->_urlBuilder->getUrl(
            'sales/sale/saveAddress'
        );
    }

    /**
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function newOrder()
    {
        $customer = $this->getCustomer();
        $gender = $customer->getGender();
        if ((int)$gender == 2) {
            $gender = "W";
        } else {
            $gender = "M";
        }
        $requestData = [
            "email" => $customer->getEmail(),
            "first_name" => $customer->getFirstname(),
            "last_name" => $customer->getLastname(),
            "gender" => $gender
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/order/new");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @param $orderId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getOrder($orderId)
    {
        $requestData = [
            "inbound_order_id" => $orderId
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/order/get");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }
}
