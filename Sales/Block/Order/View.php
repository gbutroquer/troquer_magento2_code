<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\Sales\Block\Order;

use \Magento\Customer\Model\Context;
use \Magento\Framework\Registry;
use \Magento\Payment\Helper\Data;

class View extends \Magento\Sales\Block\Order\View
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param Registry $registry
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param Data $paymentHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        Registry $registry,
        \Magento\Framework\App\Http\Context $httpContext,
        Data $paymentHelper,
        array $data = []
    ) {
        parent::__construct($context, $registry, $httpContext, $paymentHelper, $data);
    }

    /**
     * Return back url for logged in and guest users
     *
     * @return string
     */
    public function getBackUrl()
    {
        $order = $this->getOrder();

        switch($order->getStatus()) {
            case 'complete':
                $urlkey = 'delivered';
                break;
            case 'pending':
            case 'processing':
                $urlkey = 'transit';
                break;
            default:
                $urlkey = 'history';
                break;
        }

        if ($this->httpContext->getValue(Context::CONTEXT_AUTH)) {
            return $this->getUrl('*/*/' . $urlkey);
        }
        return $this->getUrl('*/*/form');
    }
}
