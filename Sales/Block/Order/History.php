<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\Sales\Block\Order;

use \Magento\Customer\Model\Session;
use \Magento\Framework\App\ObjectManager;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\View\Element\AbstractBlock;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Sales\Model\Order\Config;
use \Magento\Sales\Model\ResourceModel\Order\Collection;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactoryInterface;
use \Magento\Theme\Block\Html\Pager;
use \Throwable;
use \Troquer\Inbound\Helper\Data;

class History extends \Magento\Sales\Block\Order\History
{
    /**
     * @var Collection|null
     */
    protected ?Collection $_deliveredOrders = null;

    /**
     * @var Collection|null
     */
    protected ?Collection $_returnedOrders = null;

    /**
     * @var Collection|null
     */
    protected ?Collection $_transitOrders = null;

    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @param Context $context
     * @param CollectionFactory $orderCollectionFactory
     * @param Data $helper
     * @param Session $customerSession
     * @param Config $orderConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $orderCollectionFactory,
        Data $helper,
        Session $customerSession,
        Config $orderConfig,
        array $data = []
    )
    {
        $this->_helper = $helper;
        parent::__construct($context, $orderCollectionFactory, $customerSession, $orderConfig, $data);
    }

    /**
     * Provide order collection factory
     *
     * @return CollectionFactoryInterface
     * @deprecated 100.1.1
     */
    protected function getOrderCollectionFactory()
    {
        if ($this->_orderCollectionFactory === null) {
            $this->_orderCollectionFactory = ObjectManager::getInstance()->get(CollectionFactoryInterface::class);
        }
        return $this->_orderCollectionFactory;
    }

    /**
     * Get customer orders
     *
     * @return bool|Collection
     */
    public function getDeliveredOrders()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        if (!$this->_deliveredOrders) {
            $this->_deliveredOrders = $this->getOrderCollectionFactory()->create($customerId)->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'status',
                ['in' => 'complete']
            )->setOrder(
                'created_at',
                'desc'
            );
        }
        return $this->_deliveredOrders;
    }

    /**
     * Get customer orders
     *
     * @return bool|Collection
     */
    public function getTransitOrders()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        if (!$this->_transitOrders) {
            $this->_transitOrders = $this->getOrderCollectionFactory()->create($customerId)->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'status',
                ['in' => ['in_transit', 'pending', 'processing']]
            )->setOrder(
                'created_at',
                'desc'
            );
        }
        return $this->_transitOrders;
    }

    /**
     * Get customer orders
     *
     * @return bool|Collection
     */
    public function getReturnedOrders()
    {
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            return false;
        }
        if (!$this->_returnedOrders) {
            $this->_returnedOrders = $this->getOrderCollectionFactory()->create($customerId)->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'status',
                ['in' => $this->_orderConfig->getVisibleOnFrontStatuses()]
            )->setOrder(
                'created_at',
                'desc'
            );
            $this->_returnedOrders->getSelect()
                ->joinLeft(
                    ["soi" => "sales_order_item"],
                    'main_table.entity_id = soi.order_id',
                    array('')
                )
                ->where('soi.qty_canceled > 0 or soi.qty_refunded > 0')
                ->group('entity_id');
        }
        return $this->_returnedOrders;
    }

    /**
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->_helper->getStoreUrl() . 'media/catalog/product';
    }

    /**
     * @return $this|AbstractBlock|\Magento\Sales\Block\Order\History|History
     * @throws LocalizedException
     */
    protected function _prepareLayout()
    {
        switch ($this->_template) {
            case 'Troquer_Sales::order/returned.phtml':
                $blockName = 'sales.order.returned.pager';
                $collection = $this->getReturnedOrders();
                break;
            case 'Troquer_Sales::order/delivered.phtml':
                $blockName = 'sales.order.delivered.pager';
                $collection = $this->getDeliveredOrders();
                break;
            case 'Troquer_Sales::order/transit.phtml':
                $blockName = 'sales.order.transit.pager';
                $collection = $this->getTransitOrders();
                break;
            default:
                $blockName = 'sales.order.all.pager';
                $collection = $this->getOrders();
                break;
        }

        if($collection && $collection->getSize()) {
            try {
                $pager = $this->getLayout()->getBlock(
                    $blockName
                )->setCollection(
                    $collection
                );
                $this->setChild('pager', $pager);
                $collection->load();
            } catch (Throwable $t) {
                $pager = $this->getLayout()->createBlock(
                    Pager::class,
                    $blockName
                )->setCollection(
                    $collection
                );
                $this->setChild('pager', $pager);
                $collection->load();
            }
        }

        return $this;
    }
}
