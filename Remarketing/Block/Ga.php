<?php
/** @noinspection PhpUndefinedMethodInspection */

/** @noinspection PhpDeprecationInspection */

namespace Troquer\Remarketing\Block;

use \Magento\Cookie\Helper\Cookie;
use \Magento\Framework\Json\Helper\Data;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Sales\Model\Order;
use \Magento\Sales\Model\Order\Item;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

class Ga extends \Magento\GoogleTagManager\Block\Ga
{
    /**
     * @param Context $context
     * @param CollectionFactory $salesOrderCollection
     * @param \Magento\GoogleTagManager\Helper\Data $googleAnalyticsData
     * @param Cookie $cookieHelper
     * @param Data $jsonHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $salesOrderCollection,
        \Magento\GoogleTagManager\Helper\Data $googleAnalyticsData,
        Cookie $cookieHelper,
        Data $jsonHelper,
        array $data = []
    )
    {
        parent::__construct($context, $salesOrderCollection, $googleAnalyticsData, $cookieHelper, $jsonHelper, $data);
    }

    /**
     * Return information about order and items
     *
     * @return array
     * @since 100.2.0
     */
    public function getOrdersDataArray()
    {
        $result = [];
        $orderIds = $this->getOrderIds();
        if (empty($orderIds) || !is_array($orderIds)) {
            return $result;
        }
        $collection = $this->_salesOrderCollection->create();
        $collection->addFieldToFilter('entity_id', ['in' => $orderIds]);

        /** @var Order $order */
        foreach ($collection as $order) {
            $products = [];
            $productIds = [];
            /** @var Item $item */
            foreach ($order->getAllVisibleItems() as $item) {
                $productIds[] = $item->getProductId();
                $products[] = [
                    'id' => $item->getProductId(),
                    'name' => $item->getName(),
                    'price' => $item->getBasePrice(),
                    'quantity' => $item->getQtyOrdered(),
                ];
            }

            $orderData = [
                'id' => $order->getIncrementId(),
                'revenue' => $order->getBaseGrandTotal(),
                'tax' => $order->getBaseTaxAmount(),
                'shipping' => $order->getBaseShippingAmount(),
                'coupon' => (string)$order->getCouponCode(),
                'ids' => $productIds,
                'numItems' => count($order->getAllVisibleItems())
            ];

            $result[] = [
                'ecommerce' => [
                    'purchase' => [
                        'actionField' => $orderData,
                        'products' => $products
                    ],
                    'currencyCode' => $this->getStoreCurrencyCode()
                ],
                'event' => 'purchase'
            ];
        }
        return $result;
    }
}
