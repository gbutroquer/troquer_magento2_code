<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Remarketing\Block;

use \Magento\Banner\Model\ResourceModel\Banner\CollectionFactory;
use \Magento\Catalog\Model\Layer\Resolver;
use \Magento\Catalog\Model\Product;
use \Magento\Checkout\Helper\Cart;
use \Magento\Checkout\Model\Session;
use \Magento\Framework\App\Request\Http;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Exception\SessionException;
use \Magento\Framework\Module\Manager;
use \Magento\Framework\Registry;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\GoogleTagManager\Helper\Data;
use \Magento\GoogleTagManager\Model\Banner\Collector;
use \Magento\Catalog\Model\ProductFactory;
use \Magento\Quote\Model\Quote;

class ListJson extends \Magento\GoogleTagManager\Block\ListJson
{
    /**
     * @var \Magento\CatalogSearch\Helper\Data
     */
    protected \Magento\CatalogSearch\Helper\Data $_catalogSearchData;

    /**
     * @var ProductFactory
     */
    protected ProductFactory $_productFactory;

    /**
     * @param Context $context
     * @param Data $helper
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param Registry $registry
     * @param Session $checkoutSession
     * @param \Magento\Customer\Model\Session $customerSession
     * @param Cart $checkoutCart
     * @param Resolver $layerResolver
     * @param Manager $moduleManager
     * @param Http $request
     * @param CollectionFactory $bannerColFactory
     * @param Collector $bannerCollector
     * @param \Magento\CatalogSearch\Helper\Data $catalogSearchData
     * @param ProductFactory $productFactory
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Data $helper,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        Registry $registry,
        Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        Cart $checkoutCart,
        Resolver $layerResolver,
        Manager $moduleManager,
        Http $request,
        CollectionFactory $bannerColFactory,
        Collector $bannerCollector,
        \Magento\CatalogSearch\Helper\Data $catalogSearchData,
        ProductFactory $productFactory,
        array $data = []
    )
    {
        $this->_catalogSearchData = $catalogSearchData;
        $this->_productFactory = $productFactory;
        parent::__construct($context, $helper, $jsonHelper, $registry, $checkoutSession, $customerSession, $checkoutCart, $layerResolver, $moduleManager, $request, $bannerColFactory, $bannerCollector, $data);
    }

    /**
     * @return string
     */
    public function getSearchQueryText()
    {
        return $this->_catalogSearchData ? $this->_catalogSearchData->getEscapedQueryText() : '';
    }

    /**
     * @param $productId
     * @return Product
     */
    public function loadProduct($productId)
    {
        return $this->_productFactory->create()->load($productId);
    }

    /**
     * Generates json array of all products in the cart for javascript on each checkout step
     *
     * @return string
     * @throws SessionException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCart()
    {
        /** @var Quote $quote */
        return $this->getCheckoutSession()->getQuote();
    }

    /**
     * Returns checkout session object.
     *
     * @return Session
     * @throws SessionException
     */
    private function getCheckoutSession()
    {
        if (!$this->checkoutSession->isSessionExists()) {
            $this->checkoutSession->start();
        }
        return $this->checkoutSession;
    }

    /**
     * Generates json array of all products in the cart for javascript on each checkout step
     *
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws SessionException
     */
    public function getCartContent()
    {
        $cart = [];
        /** @var Quote $quote */
        $quote = $this->getCheckoutSession()->getQuote();
        foreach ($quote->getAllVisibleItems() as $item) {
            $cart[]= $this->_formatProduct($item);
        }
        return $this->jsonHelper->jsonEncode($cart);
    }

    /**
     * Generates json array of all products in the cart for javascript on each checkout step
     *
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws SessionException
     */
    public function getCartContentForUpdate()
    {
        $cart = [];
        /** @var Quote $quote */
        $quote = $this->getCheckoutSession()->getQuote();
        foreach ($quote->getAllVisibleItems() as $item) {
            $cart[$item->getProduct()->getId()]= $this->_formatProduct($item);
        }
        return $this->jsonHelper->jsonEncode($cart);
    }
}
