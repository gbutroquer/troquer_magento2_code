<?php

namespace Troquer\Remarketing\Model;

use \Magento\Catalog\Model\Product\Attribute\Source\Status;
use \Magento\Catalog\Model\ResourceModel\Product\Collection;
use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use \Magento\Framework\Exception\FileSystemException;
use \Magento\Framework\Exception\InputException;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Filesystem;
use \Magento\Framework\App\Filesystem\DirectoryList;
use \Magento\Framework\UrlInterface;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Catalog\Model\CategoryFactory;
use \Magento\InventorySalesApi\Api\Data\SalesChannelInterface;
use \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use \Magento\InventorySalesApi\Api\StockResolverInterface;
use \Magento\Catalog\Model\Product\Gallery\ReadHandler as GalleryReadHandler;

class Feed
{
    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $_productCollectionFactory;

    /**
     * @var DirectoryList
     */
    protected DirectoryList $_directoryList;

    /**
     * @var Filesystem
     */
    protected Filesystem $_fileSystem;

    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $_storeManager;

    /**
     * @var GetProductSalableQtyInterface
     */
    protected GetProductSalableQtyInterface $_productSalableQty;

    /**
     * @var StockResolverInterface
     */
    protected StockResolverInterface $_stockResolver;

    /**
     * @var CategoryFactory
     */
    protected CategoryFactory $_categoryFactory;

    /**
     * @var GalleryReadHandler
     */
    protected GalleryReadHandler $_galleryReadHandler;

    /**
     * @param CollectionFactory $productCollectionFactory
     * @param DirectoryList $directoryList
     * @param StoreManagerInterface $storeManager
     * @param GetProductSalableQtyInterface $productSalableQty
     * @param StockResolverInterface $stockResolver
     * @param CategoryFactory $categoryFactory
     * @param GalleryReadHandler $galleryReadHandler
     * @param Filesystem $filesystem
     */
    public function __construct(
        CollectionFactory $productCollectionFactory,
        DirectoryList $directoryList,
        StoreManagerInterface $storeManager,
        GetProductSalableQtyInterface $productSalableQty,
        StockResolverInterface $stockResolver,
        CategoryFactory $categoryFactory,
        GalleryReadHandler $galleryReadHandler,
        Filesystem $filesystem
    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_directoryList = $directoryList;
        $this->_storeManager = $storeManager;
        $this->_productSalableQty = $productSalableQty;
        $this->_stockResolver = $stockResolver;
        $this->_fileSystem = $filesystem;
        $this->_galleryReadHandler = $galleryReadHandler;
        $this->_categoryFactory = $categoryFactory;
    }

    /**
     * @return Collection
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function initializeProductCollection()
    {
        $collection = $this->_productCollectionFactory->create();

        $collection->addAttributeToSelect('*')
            ->setFlag('has_stock_status_filter', false)
            ->joinField('stock_item', 'cataloginventory_stock_item', 'is_in_stock', 'product_id=entity_id', 'is_in_stock=1')
            ->addAttributeToFilter('status', Status::STATUS_ENABLED)
            ->setStore($this->_storeManager->getStore())
            ->addAttributeToSort('created_at', 'desc');

        return $collection;
    }

    /**
     * @param $sku
     * @return mixed
     * @throws NoSuchEntityException
     * @throws InputException
     * @throws LocalizedException
     */
    public function getProductStock($sku)
    {
        $stockId = $this->_stockResolver->execute(
            SalesChannelInterface::TYPE_WEBSITE, $this->getWebsiteCode()
        )->getStockId();
        return $this->_productSalableQty->execute($sku, $stockId);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getWebsiteCode()
    {
        return $this->_storeManager->getWebsite()->getCode();
    }

    /**
     * @return string
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws FileSystemException
     */
    public function execute()
    {
        $collection = $this->initializeProductCollection();
        $document_feed = '<?xml version="1.0" encoding="utf-8" ?>
        <rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
            <channel>
                <title>Feed product Troquer</title>
                <link>https://troquer.com.mx</link>
                <description>Information of product</description>';

        foreach ($collection as $product) {
            if ($this->getProductStock($product->getData('sku')) <= 0) {
                continue;
            }
            $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . "catalog/product" . $product->getData('image');
            $identifierExists = "true";
            $manufacturer = $product->getAttributeText('manufacturer');
            $brand = "";
            if (!$manufacturer) {
                continue;
            } else if ($manufacturer == "Sin Marca") {
                $identifierExists = "false";
            } else {
                $brand = "<g:brand><![CDATA[" . $manufacturer . "]]></g:brand>";
            }

            $productUrl = $this->_storeManager->getStore()->getBaseUrl() . 'catalog/product/view/id/' . $product->getId();
            $finalPrice = $product->getPriceInfo()->getPrice('final_price')->getValue();
            $regularPrice = $product->getPriceInfo()->getPrice('regular_price')->getValue();

            if ($finalPrice < $regularPrice) {
                $priceTag = '<g:price><![CDATA[' . number_format($regularPrice, 2, '.', '') . ' MXN]]></g:price>
                            <g:sale_price><![CDATA[' . number_format($finalPrice, 2, '.', '') . ' MXN]]></g:sale_price>
                            <g:custom_label_1>$' . number_format($finalPrice, 2, '.', ',') . ' MXN</g:custom_label_1>';
            } else {
                $priceTag = '<g:price><![CDATA[' . number_format($regularPrice, 2, '.', '') . ' MXN]]></g:price>
                            <g:custom_label_1>$' . number_format($regularPrice, 2, '.', ',') . ' MXN</g:custom_label_1>';
            }

            $condition = $product->getAttributeText('condition');
            if (!$condition) {
                $condition = "used";
            }
            if ($condition == "Nuevo con Etiqueta" || $condition == "Nuevo sin Etiqueta") {
                $condition = "new";
            } else {
                $condition = "used";
            }

            $title = $product->getAttributeText('garment_type') . " " . $manufacturer;
            $title = mb_substr($title, 0, 150);

            $size = "";
            if ($product->getAttributeText('size_clothes_alpha')) {
                $size = '<g:size><![CDATA[' . $product->getAttributeText('size_clothes_alpha') . ']]></g:size>';
            } else if ($product->getAttributeText('size_lenses')) {
                $size = '<g:size><![CDATA[' . $product->getAttributeText('size_lenses') . ']]></g:size>';
            } else if ($product->getAttributeText('size_rings')) {
                $size = '<g:size><![CDATA[' . $product->getAttributeText('size_rings') . ']]></g:size>';
            } else if ($product->getAttributeText('size_shoes_alpha')) {
                $size = '<g:size><![CDATA[' . $product->getAttributeText('size_shoes_alpha') . ']]></g:size>';
            }

            $this->_galleryReadHandler->execute($product);
            $images = $product->getMediaGalleryImages();
            $additionalImage = "";
            $index = 0;
            if ($images) {
                foreach ($images as $child) {
                    if ($index == 0) {
                        $index++;
                        continue;
                    } else if ($index > 5) {
                        break;
                    }
                    $additionalImage .= '<g:additional_image_link>' . $child->getUrl() . '</g:additional_image_link>';
                    $index++;
                }
            }

            $document_feed .= sprintf('<item>
                <g:id><![CDATA[%s]]></g:id>
                <g:title><![CDATA[%s]]></g:title>
                <g:description><![CDATA[%s]]></g:description>
                <g:link><![CDATA[%s]]></g:link>
                <g:image_link>%s</g:image_link>
                <g:condition>%s</g:condition>
                <g:availability>in_stock</g:availability>
                <g:identifier_exists>%s</g:identifier_exists>
                <g:product_type><![CDATA[%s]]></g:product_type>
                %s
                <g:color><![CDATA[%s]]></g:color>
                %s
                %s%s
                <g:custom_label_0><![CDATA[1]]></g:custom_label_0>
            </item>',
                $product->getData('entity_id'),
                $title, $product->getData('description'),
                $productUrl,
                $mediaUrl,
                $condition,
                $identifierExists,
                $product->getAttributeText('garment_type'),
                $priceTag,
                $product->getAttributeText('color'),
                $brand,
                $size,
                $additionalImage
            );
        }

        $document_feed .= '</channel></rss>';
        $media = $this->_fileSystem->getDirectoryWrite($this->_directoryList::MEDIA);
        $media->writeFile("remarketing/feed.xml", $document_feed);

        return $document_feed;
    }
}
