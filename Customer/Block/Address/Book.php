<?php

namespace Troquer\Customer\Block\Address;

use \Magento\Customer\Api\AddressRepositoryInterface;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Customer\Model\Address\Config;
use \Magento\Customer\Model\Address\Mapper;
use \Magento\Customer\Block\Address\Grid as AddressesGrid;
use \Magento\Framework\View\Element\Template\Context;

class Book extends \Magento\Customer\Block\Address\Book
{
    /**
     * @param Context $context
     * @param CustomerRepositoryInterface|null $customerRepository
     * @param AddressRepositoryInterface $addressRepository
     * @param CurrentCustomer $currentCustomer
     * @param Config $addressConfig
     * @param Mapper $addressMapper
     * @param array $data
     * @param AddressesGrid|null $addressesGrid
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __construct(
        Context $context,
        AddressRepositoryInterface $addressRepository,
        CurrentCustomer $currentCustomer,
        Config $addressConfig,
        Mapper $addressMapper,
        array $data = [],
        CustomerRepositoryInterface $customerRepository = null,
        AddressesGrid $addressesGrid = null
    )
    {
        parent::__construct($context, $customerRepository, $addressRepository, $currentCustomer, $addressConfig, $addressMapper, $data, $addressesGrid);
    }

    /**
     * Get customer's default sales address
     *
     * @return int|null
     */
    public function getDefaultSales()
    {
        $customer = $this->getCustomer();
        if ($customer === null) {
            return null;
        } else {
            foreach ($customer->getAddresses() as $addr) {
                if ($addr->getCustomAttribute('is_for_sales')) {
                    return $addr->getId();
                }
            }
            return null;
        }
    }
}
