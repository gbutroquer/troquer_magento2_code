<?php

declare(strict_types=1);

namespace Troquer\Customer\Block\Address;

use \Magento\Customer\Api\Data\AddressInterface;
use \Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Customer\Model\ResourceModel\Address\Collection;
use \Magento\Customer\Model\ResourceModel\Address\CollectionFactory as AddressCollectionFactory;
use \Magento\Directory\Model\CountryFactory;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\View\Element\Template\Context;

class Grid extends \Magento\Customer\Block\Address\Grid
{
    /**
     * @var AddressCollectionFactory
     */
    private AddressCollectionFactory $addressCollectionFactory;

    /**
     * @var Collection|null
     */
    private ?Collection $addressCollection = null;

    /**
     * @param Context $context
     * @param CurrentCustomer $currentCustomer
     * @param AddressCollectionFactory $addressCollectionFactory
     * @param CountryFactory $countryFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        CurrentCustomer $currentCustomer,
        AddressCollectionFactory $addressCollectionFactory,
        CountryFactory $countryFactory,
        array $data = []
    )
    {
        $this->addressCollectionFactory = $addressCollectionFactory;
        parent::__construct($context, $currentCustomer, $addressCollectionFactory, $countryFactory, $data);
    }

    /**
     * Get current additional customer addresses
     *
     * Return array of address interfaces if customer has additional addresses and false in other cases
     *
     * @return AddressInterface[]
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @since 102.0.1
     */
    public function getAdditionalAddresses(): array
    {
        $additional = [];
        $addresses = $this->getAddressCollection();
        $primaryAddressIds = [$this->getDefaultSales(), $this->getDefaultShipping()];
        foreach ($addresses as $address) {
            if (!in_array((int)$address->getId(), $primaryAddressIds, true)) {
                $additional[] = $address->getDataModel();
            }
        }
        return $additional;
    }

    /**
     * Get default billing address
     *
     * Return address string if address found and null if not
     *
     * @return int
     */
    private function getDefaultSales(): int
    {
        $customer = $this->getCustomer();

        foreach ($customer->getAddresses() as $addr) {
            if ($addr->getCustomAttribute('is_for_sales')) {
                return (int)$addr->getId();
            }
        }
        return 0;
    }

    /**
     * Get default shipping address
     *
     * Return address string if address found and null if not
     *
     * @return int
     */
    private function getDefaultShipping(): int
    {
        $customer = $this->getCustomer();

        return (int)$customer->getDefaultShipping();
    }

    /**
     * Get customer addresses collection.
     *
     * Filters collection by customer id
     *
     * @return Collection
     * @throws NoSuchEntityException|LocalizedException
     */
    private function getAddressCollection(): Collection
    {
        if (null === $this->addressCollection) {
            if (null === $this->getCustomer()) {
                throw new NoSuchEntityException(__('Customer not logged in'));
            }
            $collection = $this->addressCollectionFactory->create();
            $collection->setOrder('entity_id', 'desc');
            $collection->addFieldToFilter(
                'entity_id',
                ['nin' => [$this->getDefaultSales(), $this->getDefaultShipping()]]
            );
            $collection->setCustomerFilter([$this->getCustomer()->getId()]);
            $this->addressCollection = $collection;
        }
        return $this->addressCollection;
    }
}
