<?php
/** @noinspection PhpDeprecationInspection */
/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedClassInspection */

namespace Troquer\Customer\Block\Address;

use \Exception;
use \Magento\Customer\Api\AddressMetadataInterface;
use \Magento\Customer\Api\AddressRepositoryInterface;
use \Magento\Customer\Api\Data\AddressInterfaceFactory;
use \Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Customer\Model\Session;
use \Magento\Directory\Helper\Data;
use \Magento\Directory\Model\CountryFactory;
use \Magento\Directory\Model\ResourceModel\Region\CollectionFactory;
use \Magento\Directory\Model\ResourceModel\Country\CollectionFactory as CountryCollectionFactory;
use \Magento\Framework\Api\DataObjectHelper;
use \Magento\Framework\App\Cache\Type\Config;
use \Magento\Framework\Json\EncoderInterface;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\App\Config\ScopeConfigInterface;

class Edit extends \Magento\Customer\Block\Address\Edit
{
    /**
     * @var CountryFactory
     */
    protected CountryFactory $_countryFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfigInterface;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Data $directoryHelper
     * @param EncoderInterface $jsonEncoder
     * @param Config $configCacheType
     * @param CollectionFactory $regionCollectionFactory
     * @param CountryCollectionFactory $countryCollectionFactory
     * @param Session $customerSession
     * @param AddressRepositoryInterface $addressRepository
     * @param AddressInterfaceFactory $addressDataFactory
     * @param CurrentCustomer $currentCustomer
     * @param DataObjectHelper $dataObjectHelper
     * @param CountryFactory $countryFactory
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param array $data
     * @param AddressMetadataInterface|null $addressMetadata
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Data $directoryHelper,
        EncoderInterface $jsonEncoder,
        Config $configCacheType,
        CollectionFactory $regionCollectionFactory,
        CountryCollectionFactory $countryCollectionFactory,
        Session $customerSession,
        AddressRepositoryInterface $addressRepository,
        AddressInterfaceFactory $addressDataFactory,
        CurrentCustomer $currentCustomer,
        DataObjectHelper $dataObjectHelper,
        CountryFactory $countryFactory,
        ScopeConfigInterface $scopeConfigInterface,
        array $data = [],
        AddressMetadataInterface $addressMetadata = null
    )
    {
        $this->_countryFactory = $countryFactory;
        $this->_scopeConfigInterface = $scopeConfigInterface;
        parent::__construct(
            $context,
            $directoryHelper,
            $jsonEncoder,
            $configCacheType,
            $regionCollectionFactory,
            $countryCollectionFactory,
            $customerSession,
            $addressRepository,
            $addressDataFactory,
            $currentCustomer,
            $dataObjectHelper,
            $data,
            $addressMetadata
        );
    }

    /**
     * Return the specified region id
     *
     * @param string $region
     * @return string
     */
    public function getRegionByCode(string $region): string
    {
        $regionCode = $this->_regionCollectionFactory->create()
            ->addRegionNameFilter($region)
            ->getFirstItem()
            ->toArray();
        return isset($regionCode['region_id']) ? $regionCode['region_id'] : '0';
    }

    /**
     * Return the allowed countries.
     *
     * @return string
     */
    public function getAllowedCountries(): string
    {
        $allowedCountries = "'MX'";
        try {
            $allowedCountries = explode(',', $this->_scopeConfigInterface->getValue('general/country/allow'));
            $allowedCountries = "'" . implode("','", $allowedCountries) . "'";
        } catch (Exception $exception) {
            $this->_logger->error($exception);
        }
        return $allowedCountries;
    }

    /**
     * @param $countryCode
     * @return string
     */
    public function getCountryByCode($countryCode)
    {
        $country = $this->_countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }
}
