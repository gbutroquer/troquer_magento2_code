<?php

namespace Troquer\Customer\Block\Account\Dashboard;

use \Magento\Customer\Helper\Session\CurrentCustomer;
use \Magento\Customer\Helper\Session\CurrentCustomerAddress;
use \Magento\Customer\Model\Address\Config;
use \Magento\Customer\Model\Address\Mapper;
use \Magento\Framework\Phrase;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Customer\Api\CustomerRepositoryInterface;

class Address extends \Magento\Customer\Block\Account\Dashboard\Address
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected CustomerRepositoryInterface $_customerRepository;

    /**
     * @param Context $context
     * @param CurrentCustomer $currentCustomer
     * @param CurrentCustomerAddress $currentCustomerAddress
     * @param Config $addressConfig
     * @param Mapper $addressMapper
     * @param CustomerRepositoryInterface $customerRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        CurrentCustomer $currentCustomer,
        CurrentCustomerAddress $currentCustomerAddress,
        Config $addressConfig,
        Mapper $addressMapper,
        CustomerRepositoryInterface $customerRepository,
        array $data = []
    )
    {
        $this->_customerRepository = $customerRepository;
        parent::__construct($context, $currentCustomer, $currentCustomerAddress, $addressConfig, $addressMapper, $data);
    }

    /**
     * HTML for Shipping Address
     *
     * @return Phrase|string
     */
    public function getPrimarySalesAddressHtml()
    {
        $customer = $this->getCustomer();
        $address = null;
        foreach ($customer->getAddresses() as $addr) {
            if ($addr->getCustomAttribute('is_for_sales')) {
                $address = $addr;
                break;
            }
        }

        if ($address) {
            return $this->_getAddressHtml($address);
        } else {
            return __('You have not set a default shipping address.');
        }
    }

    /**
     * @return string
     */
    public function getPrimarySalesAddressEditUrl()
    {
        if (!$this->getCustomer()) {
            return '';
        } else {
            $customer = $this->getCustomer();
            $address = null;
            foreach ($customer->getAddresses() as $addr) {
                if ($addr->getCustomAttribute('is_for_sales')) {
                    $address = $addr;
                    break;
                }
            }
            $addressId = $address ? $address->getId() : null;
            return $this->_urlBuilder->getUrl(
                'customer/address/edit',
                ['id' => $addressId]
            );
        }
    }

    /**
     * @return bool
     */
    public function hasSalesAddress()
    {
        $customer = $this->getCustomer();
        foreach ($customer->getAddresses() as $addr) {
            if ($addr->getCustomAttribute('is_for_sales')) {
                return true;
            }
        }
        return false;
    }
}
