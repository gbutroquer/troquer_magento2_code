<?php

namespace Troquer\Customer\Block\Form;

class Register extends \Magento\Customer\Block\Form\Register
{
    /**
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->_customerUrl->getRegisterPostUrl();
    }
}
