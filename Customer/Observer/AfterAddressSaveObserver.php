<?php
/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedMethodInspection */
/** @noinspection PhpUndefinedClassInspection */

namespace Troquer\Customer\Observer;

use \Magento\Customer\Model\Address;
use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Troquer\Inbound\Helper\Data;
use \Psr\Log\LoggerInterface;
use \Exception;

class AfterAddressSaveObserver implements ObserverInterface
{
    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $_logger;

    public function __construct(
        Data $helper,
        LoggerInterface $logger
    )
    {
        $this->_helper = $helper;
        $this->_logger = $logger;
    }

    /**
     * @param Observer $observer
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        /** @var $customerAddress Address */
        $customerAddress = $observer->getCustomerAddress();
        $isForSales = $customerAddress->getData("is_for_sales") ? (int)$customerAddress->getData("is_for_sales") : 0;
        if($isForSales) {
            $customer = $customerAddress->getCustomer();
            $this->saveCustomerTroquerAddress($customerAddress, $customer);
        }
    }

    /**
     * @param $address
     * @param $customer
     * @return mixed
     * @throws NoSuchEntityException
     */
    protected function saveCustomerTroquerAddress($address, $customer)
    {
        $street = $address->getStreet();
        $requestData = [
            "email" => $customer->getEmail(),
            "street" => $street[0],
            "external" => $street[1],
            "internal" => count($street) > 2 ? $street[2] : '',
            "neighborhood" => $address->getData("suburb"),
            "city" => $address->getData("city"),
            "state" => $address->getData("region"),
            "zip_code" => $address->getData("postcode"),
            "reference" => $address->getData("references"),
            "phone" => $address->getData("telephone"),
            "magento_id" => (int)$address->getId()
        ];

        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/seller/update-address");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }
}
