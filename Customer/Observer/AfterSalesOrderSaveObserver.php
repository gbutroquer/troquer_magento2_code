<?php
/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedMethodInspection */
/** @noinspection PhpUndefinedClassInspection */

namespace Troquer\Customer\Observer;

use \Magento\Framework\Event\Observer as EventObserver;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Sales\Model\Order\Address\Interceptor;
use \Psr\Log\LoggerInterface;

class AfterSalesOrderSaveObserver implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $_logger;

    public function __construct(
        LoggerInterface $logger
    )
    {
        $this->_logger = $logger;
    }

    /**
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $shipping_address = $order->getShippingAddress();
        $shipping_address->setData("suburb", str_replace("suburb", "", $shipping_address->getData("suburb")));
        $shipping_address->setData("references", str_replace("references\n", "", $shipping_address->getData("references")));
        $billing_address = $order->getBillingAddress();
        $billing_address->setData("suburb", str_replace("suburb", "", $shipping_address->getData("suburb")));
        $billing_address->setData("references", str_replace("references\n", "", $shipping_address->getData("references")));
    }
}
