<?php

namespace Troquer\Customer\Helper;

use \Magento\Directory\Model\ResourceModel\Region\CollectionFactory;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $_regionCollectionFactory;

    /**
     * @param Context $context
     * @param CollectionFactory $regionCollectionFactory
     */
    public function __construct(
        Context $context,
        CollectionFactory $regionCollectionFactory
    ) {
        $this->_regionCollectionFactory = $regionCollectionFactory;
        parent::__construct($context);
    }

    /**
     * @param string $region
     * @return string
     */
    public function getRegionByCode(string $region): string
    {
        $regionCode = $this->_regionCollectionFactory->create()
            ->addRegionNameFilter($region)
            ->getFirstItem()
            ->toArray();
        return isset($regionCode['region_id']) ? $regionCode['region_id'] : '0';
    }
}
