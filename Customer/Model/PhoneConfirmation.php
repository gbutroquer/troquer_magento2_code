<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Customer\Model;

use \Exception;
use \Magento\Framework\Data\Collection\AbstractDb;
use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\HTTP\Adapter\CurlFactory;
use \Magento\Framework\Model\Context;
use \Magento\Framework\Model\ResourceModel\AbstractResource;
use \Magento\Framework\Registry;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Zend_Http_Client;
use \Zend_Http_Response;

class PhoneConfirmation extends AbstractModel
{
    const STATUS = 'OK';

    /**
     * @var CurlFactory
     */
    protected CurlFactory $_curlFactory;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param CurlFactory $curlFactory
     * @param Json $json
     * @param EncryptorInterface $encryptor
     * @param ScopeConfigInterface $scopeConfig
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        CurlFactory $curlFactory,
        Json $json,
        EncryptorInterface $encryptor,
        ScopeConfigInterface $scopeConfig,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->_curlFactory = $curlFactory;
        $this->_json = $json;
        $this->_encryptor = $encryptor;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @return bool|String
     */
    protected function status()
    {
        $url = $this->_scopeConfig->getValue('customer/create_account/infobip_url') . '/status';

        return $this->sendRequest($url, [], Zend_Http_Client::GET);
    }

    /**
     * @param String $phone
     *
     * @return String
     */
    public function sendCode(string $phone)
    {
        if ($this->status() != $this::STATUS) {
            return true;
        }

        $url = $this->_scopeConfig->getValue('customer/create_account/infobip_url') . '/2fa/2/pin';
        $requestBody = [
            'applicationId' => $this->_scopeConfig->getValue('customer/create_account/application_id'),
            'messageId' => $this->_scopeConfig->getValue('customer/create_account/message_id'),
            'from' => $this->_scopeConfig->getValue('customer/create_account/auth_sms_from'),
            'to' => $phone
        ];
        $response = $this->sendRequest($url, $requestBody, Zend_Http_Client::POST);

        if (isset($response['smsStatus']) && ($response['smsStatus'] == 'MESSAGE_SENT')) {
            return $response['pinId'];
        } else if (isset($response['smsStatus']) && $response['smsStatus'] == 'MESSAGE_NOT_SENT') {
            return false;
        }

        return $response;
    }

    /**
     * @param Int $code
     * @param String $pinId
     *
     * @return String
     */
    public function validateCode(int $code, string $pinId)
    {
        if ($this->status() != $this::STATUS) {
            return true;
        }

        $url = $this->_scopeConfig->getValue('customer/create_account/infobip_url') . '/2fa/2/pin/' . $pinId . '/verify';
        $requestBody = array();
        $requestBody['pin'] = $code;
        $response = $this->sendRequest($url, $requestBody, Zend_Http_Client::POST);

        if (isset($response['verified']) && $response['verified'] == true) {
            return $response['pinId'];
        } else if (isset($response['verified']) && $response['verified'] == false) {
            return false;
        }

        return $response;
    }

    /**
     * @param String $pinId
     *
     * @return String
     */
    public function resendCode(string $pinId)
    {
        if ($this->status() != $this::STATUS) {
            return true;
        }

        $url = $this->_scopeConfig->getValue('customer/create_account/infobip_url') . '/2fa/2/pin/' . $pinId . '/resend';
        $response = $this->sendRequest($url, [], Zend_Http_Client::POST);

        if (isset($response['smsStatus']) && ($response['smsStatus'] == 'MESSAGE_SENT')) {
            return $response['pinId'];
        } else if ($response['smsStatus'] == 'MESSAGE_NOT_SENT') {
            return false;
        }

        return $response;
    }

    /**
     * @param string $url
     * @param array $requestBody
     * @param string $method
     *
     * @return String
     */
    protected function sendRequest(string $url, array $requestBody, string $method)
    {
        $public_api_key = 'App ' . $this->_encryptor->decrypt(
                $this->_scopeConfig->getValue('customer/create_account/public_api_key')
            );

        $httpAdapter = $this->_curlFactory->create();
        $httpAdapter->write(
            $method,
            $url,
            '1.1',
            [
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: ' . $public_api_key
            ],
            $this->_json->serialize($requestBody)
        );

        try {
            $result = $httpAdapter->read();
        } catch (Exception $e) {
            return true;
        }

        $responseCode = Zend_Http_Response::extractCode($result);
        if (empty($result) || in_array($responseCode, ['500', '502', '503'])) {
            return true;
        }
        $body = Zend_Http_Response::extractBody($result);

        return $this->_json->unserialize($body);
    }
}
