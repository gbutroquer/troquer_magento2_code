<?php

namespace Troquer\Customer\Model;

class Url extends \Magento\Customer\Model\Url
{
    /**
     * Retrieve customer register form post url
     *
     * @return string
     */
    public function getRegisterPostUrl()
    {
        return $this->urlBuilder->getUrl('customer/account/createpost');
    }
}
