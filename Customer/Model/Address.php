<?php
/** @noinspection PhpDeprecationInspection */
/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedMethodInspection */

namespace Troquer\Customer\Model;

use \Magento\Customer\Api\AddressMetadataInterface;
use \Magento\Customer\Api\Data\AddressInterface;
use \Magento\Customer\Api\Data\AddressInterfaceFactory;
use \Magento\Customer\Api\Data\RegionInterfaceFactory;
use \Magento\Customer\Model\Address\Config;
use \Magento\Directory\Helper\Data;
use \Magento\Directory\Model\CountryFactory;
use \Magento\Directory\Model\RegionFactory;
use \Magento\Framework\Api\AttributeValueFactory;
use \Magento\Framework\Api\DataObjectHelper;
use \Magento\Framework\Api\ExtensionAttributesFactory;
use \Magento\Framework\Data\Collection\AbstractDb;
use \Magento\Framework\Indexer\IndexerRegistry;
use \Magento\Customer\Model\CustomerFactory;
use \Magento\Framework\Model\Context;
use \Magento\Framework\Model\ResourceModel\AbstractResource;
use \Magento\Framework\Reflection\DataObjectProcessor;
use \Magento\Framework\Registry;

class Address extends \Magento\Customer\Model\Address
{
    /**
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param Data $directoryData
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param Config $addressConfig
     * @param RegionFactory $regionFactory
     * @param CountryFactory $countryFactory
     * @param AddressMetadataInterface $metadataService
     * @param AddressInterfaceFactory $addressDataFactory
     * @param RegionInterfaceFactory $regionDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param CustomerFactory $customerFactory
     * @param DataObjectProcessor $dataProcessor
     * @param IndexerRegistry $indexerRegistry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $directoryData,
        \Magento\Eav\Model\Config $eavConfig,
        Config $addressConfig,
        RegionFactory $regionFactory,
        CountryFactory $countryFactory,
        AddressMetadataInterface $metadataService,
        AddressInterfaceFactory $addressDataFactory,
        RegionInterfaceFactory $regionDataFactory,
        DataObjectHelper $dataObjectHelper,
        CustomerFactory $customerFactory,
        DataObjectProcessor $dataProcessor,
        IndexerRegistry $indexerRegistry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $directoryData,
            $eavConfig,
            $addressConfig,
            $regionFactory,
            $countryFactory,
            $metadataService,
            $addressDataFactory,
            $regionDataFactory,
            $dataObjectHelper,
            $customerFactory,
            $dataProcessor,
            $indexerRegistry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Update Model with the data from Data Interface
     *
     * @param AddressInterface $address
     * @return $this
     * Use Api/RepositoryInterface for the operations in the Data Interfaces. Don't rely on Address Model
     */
    public function updateData(AddressInterface $address)
    {
        // Set all attributes
        $attributes = $this->dataProcessor
            ->buildOutputDataArray($address, AddressInterface::class);

        foreach ($attributes as $attributeCode => $attributeData) {
            if (AddressInterface::REGION === $attributeCode) {
                $this->setRegion($address->getRegion()->getRegion());
                $this->setRegionCode($address->getRegion()->getRegionCode());
                $this->setRegionId($address->getRegion()->getRegionId());
            } else {
                $this->setDataUsingMethod($attributeCode, $attributeData);
            }
        }
        // Need to explicitly set this due to discrepancy in the keys between model and data object
        $this->setIsDefaultBilling($address->isDefaultBilling());
        $this->setIsDefaultShipping($address->isDefaultShipping());
        $customAttributes = $address->getCustomAttributes();
        if ($customAttributes !== null) {
            foreach ($customAttributes as $attribute) {
                if ($attribute->getAttributeCode() === "references") {
                    $this->setCustomAttribute($attribute->getAttributeCode(), str_replace("references\n", "", $attribute->getValue()));
                } elseif ($attribute->getAttributeCode() === "suburb") {
                    $this->setCustomAttribute($attribute->getAttributeCode(), str_replace("suburb", "", $attribute->getValue()));
                } else {
                    $this->setCustomAttribute($attribute->getAttributeCode(), $attribute->getValue());
                }
            }
        }

        return $this;
    }
}
