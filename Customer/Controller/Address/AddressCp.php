<?php
/** @noinspection PhpUnused */

namespace Troquer\Customer\Controller\Address;

use Magento\Framework\App\CsrfAwareActionInterface;
use \Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\Result\Json;
use \Magento\Framework\Controller\Result\Redirect;
use \Magento\Framework\Controller\Result\RedirectFactory;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Webapi\Rest\Request;
use \Troquer\Inbound\Helper\Data;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Controller\Result\JsonFactory;

class AddressCp implements CsrfAwareActionInterface
{
    /**
     * @var PageFactory
     */
    protected PageFactory $_pageFactory;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var HttpContext
     */
    protected HttpContext $_httpContext;

    /**
     * @var JsonFactory
     */
    protected JsonFactory $_jsonFactory;

    /**
     * @var Data
     */
    protected Data $_helper;

    /**
     * @var RedirectFactory
     */
    protected RedirectFactory $_redirectFactory;

    /**
     * @param PageFactory $pageFactory
     * @param HttpContext $httpContext
     * @param JsonFactory $jsonFactory
     * @param Request $request
     * @param Data $helper
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        PageFactory $pageFactory,
        HttpContext $httpContext,
        JsonFactory $jsonFactory,
        Request $request,
        Data $helper,
        RedirectFactory $redirectFactory
    )
    {
        $this->_request = $request;
        $this->_pageFactory = $pageFactory;
        $this->_jsonFactory = $jsonFactory;
        $this->_httpContext = $httpContext;
        $this->_helper = $helper;
        $this->_redirectFactory = $redirectFactory;
    }

    /**
     * @return ResponseInterface|Json|Redirect|ResultInterface
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        $result = $this->_jsonFactory->create();
        $data = $this->getCustomerTroquerAddressByCode($this->_request->getParams());
        $result->setData($data);
        return $result;
    }

    /**
     * @param $params
     * @return bool|string
     * @throws NoSuchEntityException
     */
    protected function getCustomerTroquerAddressByCode($params)
    {
        $requestData = [
            "zip_code" => $params["zip_code"]
        ];
        $token = $this->_helper->getApiToken();
        $ch = curl_init($this->_helper->getStoreUrl() . "rest/V1/directory/cp");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}
