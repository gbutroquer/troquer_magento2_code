<?php
/** @noinspection PhpUndefinedMethodInspection */
declare(strict_types=1);

namespace Troquer\Customer\Controller\Address;

use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\Result\Json;
use \Magento\Framework\Controller\ResultInterface;
use \Troquer\Customer\Helper\Data;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Controller\Result\JsonFactory;

class RegionPost implements CsrfAwareActionInterface
{
    /**
     *
     * @var PageFactory
     */
    protected PageFactory $_pageFactory;

    /**
     * @var JsonFactory
     */
    protected JsonFactory $_jsonFactory;

    /**
     * @var Data
     */
    protected Data $_customerHelper;

    /**
     * @param Data $customerHelper
     * @param PageFactory $pageFactory
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Data $customerHelper,
        PageFactory $pageFactory,
        JsonFactory $jsonFactory
    )
    {
        $this->_customerHelper = $customerHelper;
        $this->_pageFactory = $pageFactory;
        $this->_jsonFactory = $jsonFactory;
    }

    /**
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $result = $this->_jsonFactory->create();
        $region = $this->getRequest()->getParam('region');
        $data = [
            'regionId' => $this->_customerHelper->getRegionByCode($region),
            'success' => true
        ];

        $result->setData($data);
        return $result;
    }

    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}
