<?php
/**
 * Copyright © 2018 Customer. All rights reserved.
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Troquer_Customer',
    __DIR__
);
