<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\CatalogWidget\Model\Config\Source;

use \Magento\Eav\Model\Config;
use \Magento\Framework\Data\OptionSourceInterface;

class CategoryAttributes implements OptionSourceInterface
{
    /**
     * @var Config
     */
    protected Config $_attributeConfig;

    /**
     * @param Config $attributeConfig
     */
    public function __construct(
        Config $attributeConfig
    )
    {
        $this->_attributeConfig = $attributeConfig;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $attributes = $this->_attributeConfig->getAttributes('catalog_category');
        $data = [];
        foreach ($attributes as $attribute) {
            array_push($data, ['value' => $attribute->getData('attribute_code'), 'label' => __($attribute->getData('frontend_label'))]);
        }
        return $data;
    }
}
