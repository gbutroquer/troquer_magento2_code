<?php

namespace Troquer\CatalogWidget\Block\Widget;

use \Magento\Catalog\Model\ResourceModel\Category\Collection;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Widget\Block\BlockInterface;
use \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\View\Element\Template;
use \Magento\Widget\Helper\Conditions;

class CategoryImages extends Template implements BlockInterface
{
    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $_categoryCollectionFactory;

    /**
     * @var Conditions
     */
    protected Conditions $_conditionsHelper;

    /**
     * @param Context $context
     * @param Conditions $conditionsHelper
     * @param CollectionFactory $categoryCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Conditions $conditionsHelper,
        CollectionFactory $categoryCollectionFactory,
        array $data = []
    )
    {
        $this->_conditionsHelper = $conditionsHelper;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_template = $data['template'];
        parent::__construct($context);
    }

    /**
     * Get value of widget' title parameter
     *
     * @return mixed|string
     */
    public function getTitle()
    {
        return $this->getData('title');
    }

    /**
     * Retrieve category ids from widget
     *
     * @return string
     */
    public function getCategoryIds()
    {
        $conditions = $this->getData('conditions')
            ? $this->getData('conditions')
            : $this->getData('conditions_encoded');

        if ($conditions) {
            $conditions = $this->_conditionsHelper->decode($conditions);
        }

        foreach ($conditions as $key => $condition) {
            if (!empty($condition['attribute']) && $condition['attribute'] == 'category_ids') {
                return $condition['value'];
            }
        }
        return '';
    }

    /**
     * Retrieve category attributes from widget
     *
     * @return array
     */
    public function getCategoryAttributes()
    {
        return explode(",", $this->getData('category_attributes'));
    }

    /**
     * Get the category collection based on the ids
     *
     * @return Collection
     * @throws LocalizedException
     */
    public function getCategoryCollection()
    {
        $category_ids = explode(",", $this->getCategoryIds());
        $condition = ['in' => array_values($category_ids)];
        return $this->_categoryCollectionFactory->create()->addAttributeToFilter('entity_id', $condition)
            ->addAttributeToSelect($this->getCategoryAttributes())
            ->setStoreId($this->_storeManager->getStore()->getId());
    }
}
