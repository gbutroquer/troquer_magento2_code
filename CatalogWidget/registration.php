<?php

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
ComponentRegistrar::MODULE,
'Troquer_CatalogWidget',
__DIR__
);
