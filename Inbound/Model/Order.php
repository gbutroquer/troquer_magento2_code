<?php

namespace Troquer\Inbound\Model;

use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\AddressInterface;
use Magento\Customer\Api\Data\AddressInterfaceFactory;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Api\Data\RegionInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\Webapi\Rest\Request;
use \Magento\Store\Model\ScopeInterface;
use \Troquer\Inbound\Api\OrderInterface;
use \Zend_Http_Client;
use \Zend_Http_Client_Exception;

/**
 * Class Order
 * @package Troquer\Inbound\Model
 * @author Daniel López - daniel.lopez@troquer.com.mx, ti.daniel.lb@gmail.com
 */
class Order implements OrderInterface
{
    /**
     * @var Zend_Http_Client
     */
    protected Zend_Http_Client $_zendClient;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * @var AddressRepositoryInterface
     */
    protected AddressRepositoryInterface $_addressRepository;

    /**
     * @var AddressInterfaceFactory
     */
    protected AddressInterfaceFactory $_addressFactory;

    /**
     * @var RegionInterface
     */
    protected RegionInterface $_regionInterface;

    /**
     * @var CustomerRepositoryInterface
     */
    protected CustomerRepositoryInterface $_customerRepository;

    /**
     * Construct
     * @param Zend_Http_Client $zendClient
     * @param Json $json
     * @param Request $request
     * @param ScopeConfigInterface $scopeConfig
     * @param AddressRepositoryInterface $addressRepository
     * @param AddressInterfaceFactory $addressFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param RegionInterface $regionInterface
     * @param EncryptorInterface $encrytor
     * @api
     */
    public function __construct(
        Zend_Http_Client $zendClient,
        Json $json,
        Request $request,
        ScopeConfigInterface $scopeConfig,
        AddressRepositoryInterface $addressRepository,
        AddressInterfaceFactory $addressFactory,
        CustomerRepositoryInterface $customerRepository,
        RegionInterface $regionInterface,
        EncryptorInterface $encrytor
    )
    {
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_customerRepository = $customerRepository;
        $this->_encryptor = $encrytor;
        $this->_addressRepository = $addressRepository;
        $this->_regionInterface = $regionInterface;
        $this->_addressFactory = $addressFactory;
    }

    /**
     * @return array
     * @throws Zend_Http_Client_Exception
     */
    public function get(): array
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "inbound-order/get");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "token" => $inboundToken,
            "inbound_order_id" => $post["inbound_order_id"]
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function new()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "inbound-order");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "token" => $inboundToken,
            "email" => $post["email"],
            "first_name" => $post["first_name"],
            "last_name" => $post["last_name"],
            "gender" => $post["gender"]
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function addProduct()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "inbound-order/add-product");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "inbound_order_id" => $post["inbound_order_id"],
            "color_id" => $post["color_id"],
            "material_id" => $post["material_id"],
            "condition_id" => $post["condition_id"],
            "garment_id" => $post["garment_id"],
            "gender_id" => $post["gender_id"],
            "brand_id" => $post["brand_id"],
            "garment_category_id" => $post["garment_category_id"],
            "photo" => $post["photo"],
            "model_id" => $post["model_id"],
            "original_price" => $post["original_price"],
            "token" => $inboundToken
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function updateProduct()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "inbound-order/update-product");
        $this->_zendClient->setMethod(Zend_Http_Client::PUT);
        $params = [
            "id" => $post["id"],
            "inbound_order_id" => $post["inbound_order_id"],
            "color_id" => $post["color_id"],
            "material_id" => $post["material_id"],
            "condition_id" => $post["condition_id"],
            "garment_id" => $post["garment_id"],
            "gender_id" => $post["gender_id"],
            "brand_id" => $post["brand_id"],
            "garment_category_id" => $post["garment_category_id"],
            "photo" => $post["photo"],
            "model_id" => $post["model_id"],
            "original_price" => $post["original_price"],
            "token" => $inboundToken
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function removeProduct()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "inbound-order/remove-product");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "inbound_order_id" => $post["inbound_order_id"],
            "product_id" => $post["product_id"],
            "token" => $inboundToken
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @param int $customerId
     * @return array|bool|float|int|mixed|string|null
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws Zend_Http_Client_Exception
     */
    public function complete(int $customerId)
    {
        $post = $this->_request->getBodyParams();
        if(array_key_exists('street', $post)) {
            $customerModel = $this->getCustomer($customerId);
            $this->saveCustomerTroquerAddress($post, $customerModel);
        }
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "inbound-order/complete");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "inbound_order_id" => $post["inbound_order_id"],
            "token" => $inboundToken
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @param $post
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function completeOrder($post)
    {
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "inbound-order/complete");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "inbound_order_id" => $post["inbound_order_id"],
            "token" => $inboundToken
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function createSlaveGuide()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "inbound-order/slave-guide");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "inbound_order_id" => $post["inbound_order_id"],
            "token" => $inboundToken
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function cancelGuide()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "inbound-order/cancel-guide");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "inbound_order_id" => $post["inbound_order_id"],
            "token" => $inboundToken
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function getByStatus()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "inbound-order/get-by-status");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "token" => $inboundToken,
            "email" => $post["email"],
            "status" => $post["status"]
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @param $address
     * @param $customer
     * @throws LocalizedException
     */
    protected function saveCustomerTroquerAddress($address, $customer)
    {
        if($this->getSalesAddressId($customer)) {
            $sales_address = $this->getSalesAddress($customer);
            $street = $sales_address->getStreet();
            $street[0] = $address["street"];
            $street[1] = $address["outside"];
            $street[2] = $address["inside"];
            $sales_address->setStreet($street);
            $sales_address->setCustomAttribute("suburb", $address["suburb"]);
            $sales_address->setCity($address["city"]);
            $sales_address->setRegion($this->_regionInterface->setRegionId($address["region_id"])->setRegion($address["region"]));
            $sales_address->setRegionId($address["region_id"]);
            $sales_address->setPostcode($address["postcode"]);
            $sales_address->setCustomAttribute("references", $address["references"]);
            $sales_address->setTelephone($address["phone"]);
            $this->_addressRepository->save($sales_address);
        } else {
            $sales_address = $this->_addressFactory->create();
            $street = array();
            $street[0] = $address["street"];
            $street[1] = $address["outside"];
            $street[2] = $address["inside"];
            $sales_address->setFirstname($customer->getFirstname());
            $sales_address->setLastname($customer->getLastname());
            $sales_address->setCustomerId($customer->getId());
            $sales_address->setStreet($street);
            $sales_address->setCustomAttribute("suburb", $address["suburb"]);
            $sales_address->setCity($address["city"]);
            $sales_address->setRegion($this->_regionInterface->setRegionId($address["region_id"])->setRegion($address["region"]));
            $sales_address->setRegionId($address["region_id"]);
            $sales_address->setCountryId($address["country_id"]);
            $sales_address->setPostcode($address["postcode"]);
            $sales_address->setCustomAttribute("references", $address["references"]);
            $sales_address->setTelephone($address["phone"]);
            $sales_address->setCustomAttribute("is_for_sales", true);
            $sales_address->setIsDefaultBilling(false);
            $sales_address->setIsDefaultShipping(false);

            $this->_addressRepository->save($sales_address);
        }
    }

    /**
     * Get customer's default sales address
     *
     * @param $customer
     * @return int|null
     */
    protected function getSalesAddressId($customer): ?int
    {
        if ($customer === null) {
            return null;
        } else {
            foreach ($customer->getAddresses() as $addr) {
                if ($addr->getCustomAttribute('is_for_sales') && $addr->getCustomAttribute('is_for_sales')->getValue()) {
                    return $addr->getId();
                }
            }
            return null;
        }
    }

    /**
     * @param $customer
     * @return AddressInterface|null
     * @throws LocalizedException
     */
    public function getSalesAddress($customer): ?AddressInterface
    {
        $addressId = $this->getSalesAddressId($customer);
        if($addressId) {
            return $this->_addressRepository->getById($addressId);
        }
        return null;
    }

    /**
     * @param $customerId
     * @return CustomerInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function getCustomer($customerId): CustomerInterface
    {
        return $this->_customerRepository->getById($customerId);
    }
}
