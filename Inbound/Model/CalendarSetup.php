<?php
/** @noinspection PhpUnused */

namespace Troquer\Inbound\Model;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\Webapi\Rest\Request;
use \Magento\Store\Model\ScopeInterface;
use \Troquer\Inbound\Api\CalendarSetupInterface;
use \Zend_Http_Client;
use \Zend_Http_Client_Exception;

class CalendarSetup implements CalendarSetupInterface
{
    /**
     * @var Zend_Http_Client
     */
    protected Zend_Http_Client $_zendClient;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * Construct
     * @param Zend_Http_Client $zendClient
     * @param Json $json
     * @param Request $request
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encrytor
     * @api
     */
    public function __construct(
        Zend_Http_Client $zendClient,
        Json $json,
        Request $request,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encrytor
    )
    {
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $encrytor;
    }

    /**
     * Revisa si existe calendario disponible para el código postal
     * @return boolean
     * @throws Zend_Http_Client_Exception
     * @api
     */
    public function getAvailability()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "calendar-availability");
        $this->_zendClient->setMethod(Zend_Http_Client::GET);
        $this->_zendClient->setParameterGet([
            "zip_code" => $post["zip_code"],
            "token" => $inboundToken]);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * Obtiene las horas disponibles para crear una cita
     * @return array
     * @throws Zend_Http_Client_Exception
     * @api
     */
    public function getDateTimes()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "appointments-available-hours");
        $this->_zendClient->setMethod(Zend_Http_Client::GET);
        $this->_zendClient->setParameterGet([
            "date" => $post["date"],
            "calendar_id" => $post["calendar_id"],
            "garments" => $post["garments"],
            "origin" => "troquer",
            "token" => $inboundToken]);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }
}
