<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUndefinedClassInspection */

namespace Troquer\Inbound\Model;

use \Magento\Catalog\Api\Data\ProductInterfaceFactory;
use \Magento\Catalog\Model\Product\Attribute\Source\Status;
use \Magento\Catalog\Model\Product\Type;
use \Magento\Catalog\Model\Product as MagentoProduct;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Framework\Exception\CouldNotSaveException;
use \Magento\Framework\Exception\InputException;
use \Magento\Framework\Exception\StateException;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\Webapi\Rest\Request;
use \Magento\Sales\Model\ResourceModel\Order\CollectionFactoryInterface;
use \Magento\Store\Model\ScopeInterface;
use \Psr\Log\LoggerInterface;
use \Troquer\Inbound\Api\ProductInterface;
use \Zend_Http_Client;
use \Zend_Http_Client_Exception;
use \Magento\Catalog\Api\ProductRepositoryInterface;
use \Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use \Magento\InventorySalesApi\Api\StockResolverInterface;
use \Magento\Store\Model\StoreManagerInterface;

/**
 * Class Product
 * @package Troquer\Inbound\Model
 * @author Daniel López - daniel.lopez@troquer.com.mx, ti.daniel.lb@gmail.com
 */
class Product implements ProductInterface
{
    /**
     * @var Zend_Http_Client
     */
    protected Zend_Http_Client $_zendClient;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * @var ProductInterfaceFactory
     */
    protected ProductInterfaceFactory $_productFactory;

    /**
     * @var ProductRepositoryInterface
     */
    protected ProductRepositoryInterface $_productRepository;

    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $_logger;

    /**
     * @var MagentoProduct
     */
    protected MagentoProduct $_product;

    /**
     * @var GetProductSalableQtyInterface
     */
    protected GetProductSalableQtyInterface $_productSalableQty;

    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $_storeManager;

    /**
     * @var StockResolverInterface
     */
    protected StockResolverInterface $_stockResolver;

    /**
     * @var CollectionFactoryInterface
     */
    protected CollectionFactoryInterface $_orderCollectionFactory;

    /**
     * Construct
     * @param Zend_Http_Client $zendClient
     * @param Json $json
     * @param Request $request
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encrytor
     * @param ProductInterfaceFactory $productFactory
     * @param ProductRepositoryInterface $productRepository
     * @param MagentoProduct $product
     * @param CollectionFactoryInterface $orderCollectionFactory
     * @param LoggerInterface $logger
     * @api
     */
    public function __construct(
        Zend_Http_Client $zendClient,
        Json $json,
        Request $request,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encrytor,
        ProductInterfaceFactory $productFactory,
        ProductRepositoryInterface $productRepository,
        MagentoProduct $product,
        CollectionFactoryInterface $orderCollectionFactory,
        LoggerInterface $logger
    )
    {
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $encrytor;
        $this->_productFactory = $productFactory;
        $this->_productRepository = $productRepository;
        $this->_product = $product;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_logger = $logger;
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function getBySku()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "product/sku/" . $post["sku"]);
        $this->_zendClient->setMethod(Zend_Http_Client::GET);
        $params = ["token" => $inboundToken];
        $this->_zendClient->setParameterGet($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|string
     */
    public function getNoStockUpdateProductsBySku()
    {
        $post = $this->_request->getBodyParams();
        $skus = $post["skus"];
        $nonExistent = [];
        $sold = [];
        $soldProducts = $this->_orderCollectionFactory->create()->addFieldToSelect(
            'status'
        )->addFieldToFilter(
            'status',
            ['in' => ['processing', 'in_transit']]
        )->setOrder(
            'created_at',
            'desc'
        );
        $soldProducts->getSelect()
            ->joinLeft(
                ["soi" => "sales_order_item"],
                'main_table.entity_id = soi.order_id',
                array('*')
            )
            ->where('soi.qty_canceled = 0 and soi.qty_refunded = 0 and soi.sku in ("' . implode("\", \"", $skus) . '")');

        foreach ($skus as $sku) {
            if (!$this->productExistBySku($sku)) {
                $nonExistent[] = $sku;
            }
        }
        foreach ($soldProducts as $product) {
            $sold[] = $product->getSku();
        }
        $result = [];
        $result["non_existent"] = $nonExistent;
        $result["sold"] = $sold;
        return $this->_json->serialize($result);
    }

    /**
     * @param String $sku
     * @return bool
     */
    private function productExistBySku(string $sku)
    {
        if ($this->_product->getIdBySku($sku)) {
            return true;
        }
        return false;
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function getByEmail()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "products/list-status");
        $this->_zendClient->setMethod(Zend_Http_Client::GET);
        $params = [
            "filter" => "email",
            "search" => $post["search"],
            "status" => $post["status"],
            "token" => $inboundToken
        ];
        $this->_zendClient->setParameterGet($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return mixed|void
     */
    public function add()
    {
        $post = $this->_request->getBodyParams();
        $product = $this->_productFactory->create();
        $product->setSku($post["sku"]);
        $product->setTypeId(Type::TYPE_SIMPLE);
        $product->setName($post["name"]);
        $product->setStatus(1);
        $product->setAttributeSetId($post["attribute_set_id"]);
        $product->setWebsiteIds([1]);
        $product->setVisibility(4);
        $product->setPrice(1);
        $product->setCustomAttribute("garment_type", 297);
        $product->setStatus(Status::STATUS_DISABLED);
        $product->setStockData(
            array(
                'use_config_manage_stock' => 0,
                'manage_stock' => 0,
                'is_in_stock' => 1,
                'qty' => 1
            ));
        try {
            $this->_productRepository->save($product);
        } catch (CouldNotSaveException $e) {
            $this->_logger->info($e->getLogMessage());
        } catch (InputException $e) {
            $this->_logger->info($e->getLogMessage());
        } catch (StateException $e) {
            $this->_logger->info($e->getLogMessage());
        }
    }

    /**
     * @return array|void
     */
    public function update()
    {
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function returnGuide()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "products/return-guide");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = array_merge($post, ["token" => $inboundToken]);
        $this->_zendClient->setParameterGet($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function registerTroquerSku()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "troquer/sku");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = array_merge($post, ["token" => $inboundToken]);
        $this->_zendClient->setParameterGet($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function getTroquerSku()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "troquer/get-sku");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = array_merge($post, ["token" => $inboundToken]);
        $this->_zendClient->setParameterGet($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function deleteTroquerSku()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "troquer/delete-sku");
        $this->_zendClient->setMethod(Zend_Http_Client::DELETE);
        $params = array_merge($post, ["token" => $inboundToken]);
        $this->_zendClient->setParameterGet($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }
}
