<?php

namespace Troquer\Inbound\Model;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\Webapi\Rest\Request;
use \Magento\Store\Model\ScopeInterface;
use \Troquer\Inbound\Api\DirectoryInterface;
use \Zend_Http_Client;
use \Zend_Http_Client_Exception;

/**
 * Class Directory
 * @package Troquer\Inbound\Model
 * @author Daniel López - daniel.lopez@troquer.com.mx, ti.daniel.lb@gmail.com
 */
class Directory implements DirectoryInterface
{
    /**
     * @var Zend_Http_Client
     */
    protected Zend_Http_Client $_zendClient;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * Construct
     * @param Zend_Http_Client $zendClient
     * @param Json $json
     * @param Request $request
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encryptor
     * @api
     */
    public function __construct(
        Zend_Http_Client $zendClient,
        Json $json,
        Request $request,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encryptor
    )
    {
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $encryptor;
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function getDirectoryByCP()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "directory/cp");
        $this->_zendClient->setMethod(Zend_Http_Client::GET);
        $post["token"] = $inboundToken;
        $this->_zendClient->setParameterGet($post);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }
}
