<?php

namespace Troquer\Inbound\Model;

use \Magento\Customer\Model\Customer;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\Webapi\Rest\Request;
use \Magento\Store\Model\ScopeInterface;
use \Troquer\Inbound\Api\NotificationInterface;
use \Zend_Http_Client;
use \Zend_Http_Client_Exception;

/**
 * Class Notification
 * @package Troquer\Inbound\Model
 * @author Daniel López - daniel.lopez@troquer.com.mx, ti.daniel.lb@gmail.com
 */
class Notification implements NotificationInterface
{
    /**
     * @var Zend_Http_Client
     */
    protected Zend_Http_Client $_zendClient;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * Construct
     * @param Zend_Http_Client $zendClient
     * @param Json $json
     * @param Request $request
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encrytor
     * @api
     */
    public function __construct(
        Zend_Http_Client $zendClient,
        Json $json,
        Request $request,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encrytor
    )
    {
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $encrytor;
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function get()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "notifications/get");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "token" => $inboundToken,
            "email" => $post["email"]
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function unread()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "notifications/unread");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "token" => $inboundToken,
            "email" => $post["email"]
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function read()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "notifications/read");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "token" => $inboundToken,
            "email" => $post["email"],
            "notification_id" => $post["notification_id"]
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @param Customer|null $customer
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function send(Customer $customer = null)
    {
        if ($customer == null) {
            $post = $this->_request->getBodyParams();
        }
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "notifications/send");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        if ($customer == null) {
            $params = [
                "token" => $inboundToken,
                "email" => $post["email"],
                "title" => $post["title"],
                "body" => $post["body"],
                "cta" => $post["cta"]
            ];
        } else {
            $params = [
                "token" => $inboundToken,
                "email" => $customer->getEmail(),
                "title" => "Bienvenidx a Troquer",
                "body" => "Gracias por sumarte al consumo sustentable y a la economía cíclica. Da click aquí y llena tu perfil.",
                "cta" => "customer/account/index/"
            ];
        }
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function delete()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "notifications/delete");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "token" => $inboundToken,
            "email" => $post["email"],
            "notification_id" => $post["notification_id"]
        ];
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }
}
