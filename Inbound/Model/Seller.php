<?php

namespace Troquer\Inbound\Model;

use \Exception;
use \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\Webapi\Rest\Request;
use \Magento\Store\Model\ScopeInterface;
use \Troquer\Inbound\Api\SellerInterface;
use \Zend_Http_Client;
use \Zend_Http_Client_Exception;

/**
 * Class Seller
 * @package Troquer\Inbound\Model
 * @author Daniel López - daniel.lopez@troquer.com.mx, ti.daniel.lb@gmail.com
 */
class Seller implements SellerInterface
{
    /**
     * @var Zend_Http_Client
     */
    protected Zend_Http_Client $_zendClient;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $_collectionFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    private CustomerRepositoryInterface $_customerRepository;

    /**
     * Construct
     * @param Zend_Http_Client $zendClient
     * @param Json $json
     * @param Request $request
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encryptor
     * @param CollectionFactory $collectionFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @api
     */
    public function __construct(
        Zend_Http_Client $zendClient,
        Json $json,
        Request $request,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encryptor,
        CollectionFactory $collectionFactory,
        CustomerRepositoryInterface $customerRepository
    )
    {
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $encryptor;
        $this->_collectionFactory = $collectionFactory;
        $this->_customerRepository = $customerRepository;
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function new()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "sellers/new");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $post["token"] = $inboundToken;
        $this->_zendClient->setParameterPost($post);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return bool|void
     * @throws Zend_Http_Client_Exception
     */
    public function getAll()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "sellers/get-all");
        $this->_zendClient->setMethod(Zend_Http_Client::GET);
        $post["token"] = $inboundToken;
        $this->_zendClient->setParameterGet($post);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return mixed|void
     * @throws Zend_Http_Client_Exception
     */
    public function syncAll()
    {
        //TODO: revisar la sicronizacion de direcciones
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        //Primero se sincroniza de inbound a Magento
        $this->_zendClient->setUri($inboundUrl . "sellers");
        $post["token"] = $inboundToken;
        $this->_zendClient->setMethod(Zend_Http_Client::GET);
        $this->_zendClient->setParameterGet($post);
        $response = $this->_zendClient->request()->getBody();
        foreach ($response as $troquerSeller) {
            try {
                $customer = $this->_customerRepository->get($troquerSeller["email"]);
                $customer->setFirstname($troquerSeller["first_name"]);
                $customer->setLastname($troquerSeller["last_name"]);
                $customer->setCustomAttribute('phone', $troquerSeller["celphone"]);
                $customer->setGender($troquerSeller["sex"]);
                $this->_customerRepository->save($customer);
            } catch (Exception $e) {
                $e->getMessage();
            }
        }
        //Por ultimo se sincroniza de Magento a inbound
        $customerCollection = $this->_collectionFactory->create();
        $customerArray = array();
        $i = 0;
        foreach ($customerCollection as $customer) {
            $customerArray[$i]["email"] = $customer->getEmail();
            $customerArray[$i]["first_name"] = $customer->getFirstname();
            $customerArray[$i]["middle_name"] = $customer->getMiddlename();
            $customerArray[$i]["last_name"] = $customer->getLastname();
            $customerArray[$i]["gender"] = $customer->getGender();
            $customerArray[$i++]["phone"] = $customer->getPhone();
        }
        $this->_zendClient->setUri($inboundUrl . "sellers/sync-all");
        $post["troqueras"] = $customerArray;
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $this->_zendClient->setParameterPost($post);
        $this->_zendClient->request()->getBody();
    }

    /**
     * @return array|mixed
     * @throws LocalizedException
     */
    public function searchByEmail()
    {
        $data = array();
        $post = $this->_request->getBodyParams();
        try {
            $websiteId = 1;
            $customer = $this->_customerRepository->get($post["email"], $websiteId);
        } catch (Exception $e) {
            throw new LocalizedException(__("The customer email isn't defined."));
        }
        $data["customer_id"] = $customer->getId();
        return $data;
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function getByEmail()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "seller-email");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $post["token"] = $inboundToken;
        $this->_zendClient->setParameterPost($post);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function getAddress()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "seller-address");
        $this->_zendClient->setMethod(Zend_Http_Client::GET);
        $post["token"] = $inboundToken;
        $this->_zendClient->setParameterGet($post);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }

    /**
     * @return array|bool|float|int|mixed|string|null
     * @throws Zend_Http_Client_Exception
     */
    public function updateAddress()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "seller-update-address");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $post["token"] = $inboundToken;
        $this->_zendClient->setParameterPost($post);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }
}
