<?php
/** @noinspection PhpUnused */

namespace Troquer\Inbound\Model;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\Webapi\Rest\Request;
use \Magento\Store\Model\ScopeInterface;
use \Troquer\Inbound\Api\AppointmentInterface;
use \Zend_Http_Client;
use \Zend_Http_Client_Exception;

class Appointment implements AppointmentInterface
{
    /**
     * @var Zend_Http_Client
     */
    protected Zend_Http_Client $_zendClient;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * Construct
     * @param Zend_Http_Client $zendClient
     * @param Json $json
     * @param Request $request
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encrytor
     * @api
     */
    public function __construct(
        Zend_Http_Client $zendClient,
        Json $json,
        Request $request,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encrytor
    )
    {
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $encrytor;
    }

    /**
     * Registra una nueva cita
     * @return boolean
     * @throws Zend_Http_Client_Exception
     * @api
     */
    public function add()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "save-appointment");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $params = [
            "troquera_id" => $post["troquera_id"],
            "phone" => $post["phone"],
            "type" => $post["type"],
            "duration" => $post["duration"],
            "date" => $post["date"],
            "time" => $post["time"],
            "calendar_id" => $post["calendar_id"],
            "has_address" => $post["has_address"],
            "token" => $inboundToken];
        if ($post["has_address"] == true) {
            $addressArray = [
                "street" => $post["street"],
                "neighborhood" => $post["neighborhood"],
                "city" => $post["city"],
                "state" => $post["state"],
                "zip_code" => $post["zip_code"]];
            $params = array_merge($params, $addressArray);
        }
        $this->_zendClient->setParameterPost($params);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }
}
