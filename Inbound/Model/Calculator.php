<?php

namespace Troquer\Inbound\Model;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Framework\Webapi\Rest\Request;
use \Magento\Store\Model\ScopeInterface;
use \Troquer\Inbound\Api\CalculatorInterface;
use \Zend_Http_Client;
use \Zend_Http_Client_Exception;

class Calculator implements CalculatorInterface
{
    /**
     * @var Zend_Http_Client
     */
    protected Zend_Http_Client $_zendClient;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @var Request
     */
    protected Request $_request;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * Construct
     * @param Zend_Http_Client $zendClient
     * @param Json $json
     * @param Request $request
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface $encrytor
     * @api
     */
    public function __construct(
        Zend_Http_Client $zendClient,
        Json $json,
        Request $request,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encrytor
    )
    {
        $this->_zendClient = $zendClient;
        $this->_json = $json;
        $this->_request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->_encryptor = $encrytor;
    }

    /**
     * Obtiene el precio generado por la calculadora
     * @return string Información de la calculadora
     * @throws Zend_Http_Client_Exception
     * @api
     */
    public function calculate()
    {
        $post = $this->_request->getBodyParams();
        $inboundUrl = $this->_scopeConfig->getValue("troquer_inbound/configuration/url", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_scopeConfig->getValue("troquer_inbound/configuration/token", ScopeInterface::SCOPE_STORE);
        $inboundToken = $this->_encryptor->decrypt($inboundToken);
        $this->_zendClient->setUri($inboundUrl . "calculate-price");
        $this->_zendClient->setMethod(Zend_Http_Client::POST);
        $this->_zendClient->setParameterPost([
            "garment_id" => $post["garment_id"],
            "brand_id" => $post["brand_id"],
            "material_id" => $post["material_id"],
            "condition_id" => $post["condition_id"],
            "model_id" => (isset($post["model_id"])) ? $post["model_id"] : "",
            "original_price" => (isset($post["original_price"])) ? $post["original_price"] : "",
            "from_vende" => false,
            "user" => "1",
            "token" => $inboundToken]);
        $response = $this->_zendClient->request()->getBody();
        return $this->_json->unserialize($response);
    }
}
