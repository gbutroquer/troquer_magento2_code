<?php

namespace Troquer\Inbound\Api;

/**
 * Interface SellerInterface
 * @package Troquer\Inbound\Api
 * @author Daniel López - daniel.lopez@troquer.com.mx, ti.daniel.lb@gmail.com
 */
interface SellerInterface
{
    /**
     * Get all the sellers
     * @return boolean
     * @api
     */
    public function getAll();

    /**
     * Register new seller
     * @return mixed
     */
    public function new();

    /**
     * @return mixed
     */
    public function syncAll();

    /**
     * @return mixed
     */
    public function searchByEmail();

    /**
     * @return mixed
     */
    public function getByEmail();

    /**
     * @return mixed
     */
    public function getAddress();

    /**
     * @return mixed
     */
    public function updateAddress();
}
