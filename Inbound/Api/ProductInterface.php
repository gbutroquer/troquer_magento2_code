<?php

namespace Troquer\Inbound\Api;

/**
 * Interface ProductInterface
 * @package Troquer\Inbound\Api
 * @author Daniel López - daniel.lopez@troquer.com, ti.daniel.lb@gmail.com
 */
interface ProductInterface
{
    /**
     * Get product attributes by SKU
     * @api
     * @return array
     */
    public function getBySku();

    /**
     * Get products with no stock update by SKU
     * @api
     * @return array
     */
    public function getNoStockUpdateProductsBySku();

    /**
     * Get product attributes by email
     * @api
     * @return array
     */
    public function getByEmail();

    /**
     * Create a new product in Magento
     * @api
     * @return mixed
     */
    public function add();

    /**
     * Update product attributes
     * @api
     * @return array
     */
    public function update();

    /**
     * Generate Fedex return guide
     * @api
     * @return array
     */
    public function returnGuide();

    /**
     * @return mixed
     */
    public function registerTroquerSku();

    /**
     * @return mixed
     */
    public function getTroquerSku();

    /**
     * @return mixed
     */
    public function deleteTroquerSku();
}
