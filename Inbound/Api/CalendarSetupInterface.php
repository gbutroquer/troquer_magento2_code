<?php

namespace Troquer\Inbound\Api;

interface CalendarSetupInterface
{
    /**
     * Revisa si existe disponibilidad de calendario de acuerdo al código postal
     * @api
     * @return boolean
    */
    public function getAvailability();

    /**
     * Obtiene las horas disponibles para crear una cita
     * @api
     * @return array
     */
    public function getDateTimes();
}
