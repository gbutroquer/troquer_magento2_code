<?php

namespace Troquer\Inbound\Api;

interface CalculatorInterface
{
    /**
     * Obtiene el precio generado por la calculadora
     * @api
     * @return string Información de la calculadora
    */
    public function calculate();
}
