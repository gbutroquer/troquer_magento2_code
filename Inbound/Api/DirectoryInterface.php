<?php

namespace Troquer\Inbound\Api;

/**
 * Interface DirectoryInterface
 * @package Troquer\Inbound\Api
 * @author Daniel López - daniel.lopez@troquer.com.mx, ti.daniel.lb@gmail.com
 */
interface DirectoryInterface
{
    /**
     * Get all the sellers
     * @return boolean
     * @api
     */
    public function getDirectoryByCP();
}
