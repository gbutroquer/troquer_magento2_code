<?php

namespace Troquer\Inbound\Api;

/**
 * Interface CatalogInterface
 * @package Troquer\Inbound\Api
 * @author Daniel López - daniel.lopez@troquer.com, ti.daniel.lb@gmail.com
 */
interface CatalogInterface
{
    /**
     * Get attributes code
     * @api
     * @return array
     */
    public function getAttributesByCode();
}
