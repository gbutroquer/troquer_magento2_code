<?php

namespace Troquer\Inbound\Api;

/**
 * Interface NotificationInterface
 * @package Troquer\Notification\Api
 * @author Daniel López - daniel.lopez@troquer.com.mx, ti.daniel.lb@gmail.com
 */
interface NotificationInterface
{
    /**
     * Get notifications
     * @api
     * @return array
     */
    public function get();

    /**
     * Mark notification as unread
     * @api
     * @return array
     */
    public function unread();

    /**
     * Send new notification
     * @api
     * @return array
     */
    public function read();

    /**
     * Get order detail
     * @api
     * @return array
     */
    public function send();

    /**
     * Get order detail
     * @api
     * @return array
     */
    public function delete();
}
