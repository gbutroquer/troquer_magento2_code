<?php

namespace Troquer\Inbound\Api;

/**
 * Interface OrderInterface
 * @package Troquer\Inbound\Api
 * @author Daniel López - daniel.lopez@troquer.com.mx, ti.daniel.lb@gmail.com
 */
interface OrderInterface
{
    /**
     * Get order detail
     * @api
     * @return array
     */
    public function get();

    /**
     * Crea una nueva orden
     * @return mixed
     */
    public function new();

    /**
     * @return mixed
     */
    public function getByStatus();

    /**
     * @return mixed
     */
    public function addProduct();

    /**
     * @return mixed
     */
    public function updateProduct();

    /**
     * @return mixed
     */
    public function removeProduct();

    /**
     * @param int $customerId
     * @return mixed
     */
    public function complete(int $customerId);

    /**
     * @return mixed
     */
    public function createSlaveGuide();

    /**
     * @return mixed
     */
    public function cancelGuide();
}
