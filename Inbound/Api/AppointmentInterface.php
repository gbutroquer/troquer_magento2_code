<?php

namespace Troquer\Inbound\Api;

interface AppointmentInterface
{
    /**
     * Almacena una cita al calendario
     * @api
     * @return string ID de la cita generada
     */
    public function add();
}
