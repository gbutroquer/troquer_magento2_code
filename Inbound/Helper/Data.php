<?php
/** @noinspection PhpUndefinedMethodInspection */

namespace Troquer\Inbound\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Session\SessionManagerInterface;
use \Magento\Framework\UrlInterface;
use \Magento\Framework\App\Helper\Context;
use \Magento\Store\Model\ScopeInterface;
use \Magento\User\Model\UserFactory;
use \Magento\User\Model\User;
use \OTPHP\TOTP;
use \Magento\TwoFactorAuth\Model\Provider\Engine\Google;

class Data extends AbstractHelper
{
    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * @var UrlInterface
     */
    protected UrlInterface $_url;

    /**
     * @var UserFactory
     */
    protected UserFactory $_userFactory;

    /**
     * @var User
     */
    protected User $_user;

    /**
     * @var Google
     */
    protected Google $_google;

    /**
     * @var UrlInterface
     */
    protected UrlInterface $_urlInterface;

    /**
     * @var SessionManagerInterface
     */
    protected SessionManagerInterface $_coreSession;

    /**
     * Data constructor.
     * @param Context $context
     * @param EncryptorInterface $encryptor
     * @param SessionManagerInterface $coreSession
     * @param UrlInterface $url
     * @param UserFactory $userFactory
     * @param User $user
     * @param Google $google
     * @param UrlInterface $urlInterface
     */
    public function __construct(
        Context $context,
        EncryptorInterface $encryptor,
        SessionManagerInterface $coreSession,
        UrlInterface $url,
        UserFactory $userFactory,
        User $user,
        Google $google,
        UrlInterface $urlInterface
    ) {
        $this->_encryptor = $encryptor;
        $this->_url = $url;
        $this->_coreSession = $coreSession;
        $this->_userFactory = $userFactory;
        $this->_user = $user;
        $this->_google = $google;
        $this->_urlInterface = $urlInterface;
        parent::__construct($context);
    }

    /**
     * @return bool|string
     * @throws NoSuchEntityException
     */
    public function getApiToken()
    {
        $token = $this->_coreSession->getToken();
        $validation = $this->_coreSession->getValidation();
        $hourdiff = -1;
        if($validation && is_string($token)) {
            $hourdiff = round((strtotime("now") - $validation) / 3600, 1);
        }
        if($hourdiff === -1 || $hourdiff >= 3) {
            $count = 0;
            $token = $this->getTokenTfa();
            while (!$this->isString($token) && $count < 10) {
                $token = $this->getTokenTfa();
                $count++;
            }
            $this->_coreSession->setToken(json_decode($token));
            $this->_coreSession->setValidation(strtotime("now"));
        }
        return $token;
    }

    /**
     * @return bool|string
     * @throws NoSuchEntityException
     */
    private function getTokenTfa() {
        $username = $this->scopeConfig->getValue("troquer_inbound/configuration2/mageuser", ScopeInterface::SCOPE_STORE);
        $password = $this->scopeConfig->getValue("troquer_inbound/configuration2/magepass", ScopeInterface::SCOPE_STORE);
        $password = $this->_encryptor->decrypt($password);
        $userData = array("username" => $username, "password" => $password, "otp" => (int)$this->getUserOtp($username));
        $ch = curl_init($this->getStoreUrl() . "rest/default/V1/tfa/provider/google/authenticate");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($userData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-Length: " . strlen(json_encode($userData))));
        return curl_exec($ch);
    }

    /**
     * @param $json
     * @return bool
     */
    private function isString($json) {
        if(is_string(json_decode($json))) {
            return true;
        }
        return false;
    }

    /**
     * @param string $username
     * @return string
     * @throws NoSuchEntityException
     */
    protected function getUserOtp(string $username): string
    {
        $user = $this->_userFactory->create();
        $user->loadByUsername($username);
        $totp = TOTP::create($this->_google->getSecretCode($user));

        return $totp->now();
    }

    /**
     * @return string
     */
    public function getStoreUrl()
    {
        $baseUrl = $this->_urlInterface->getBaseUrl();
        if (strpos($baseUrl, "local") !== false) {
            $baseUrl = "https://dev.troquer.com.mx/";
        }

        return $baseUrl;
    }
}
