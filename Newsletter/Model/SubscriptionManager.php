<?php
/** @noinspection PhpUndefinedClassInspection */

/** @noinspection PhpUndefinedMethodInspection */

namespace Troquer\Newsletter\Model;

use \Magento\Customer\Api\AccountManagementInterface;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Newsletter\Model\Subscriber;
use \Magento\Store\Model\ScopeInterface;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Newsletter\Model\SubscriberFactory;
use \Psr\Log\LoggerInterface;
use \Troquer\Newsletter\Helper\Data;

class SubscriptionManager extends \Magento\Newsletter\Model\SubscriptionManager
{
    /**
     * @var SubscriberFactory
     */
    protected SubscriberFactory $_subscriberFactory;

    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $_storeManager;

    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $_logger;

    /**
     * @var Data
     */
    protected Data $_data;

    /**
     * SubscriptionManager constructor.
     * @param SubscriberFactory $subscriberFactory
     * @param LoggerInterface $logger
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     * @param AccountManagementInterface $customerAccountManagement
     * @param CustomerRepositoryInterface $customerRepository
     * @param Data $data
     */
    public function __construct(
        SubscriberFactory $subscriberFactory,
        LoggerInterface $logger,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        AccountManagementInterface $customerAccountManagement,
        CustomerRepositoryInterface $customerRepository,
        Data $data
    )
    {
        $this->_storeManager = $storeManager;
        $this->_subscriberFactory = $subscriberFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_logger = $logger;
        $this->_data = $data;
        parent::__construct($subscriberFactory, $logger, $storeManager, $scopeConfig, $customerAccountManagement, $customerRepository);
    }

    /**
     * @param string $email
     * @param int $storeId
     * @return Subscriber
     * @throws NoSuchEntityException
     */
    public function subscribe(string $email, int $storeId): Subscriber
    {
        $websiteId = (int)$this->_storeManager->getStore($storeId)->getWebsiteId();
        $subscriber = $this->_subscriberFactory->create()->loadBySubscriberEmail($email, $websiteId);
        $currentStatus = (int)$subscriber->getStatus();

        $this->_data->subscribeActiveCampaign($email, "", "");

        if ($currentStatus === Subscriber::STATUS_SUBSCRIBED) {
            return $subscriber;
        }
        $status = $this->isConfirmNeed($storeId) ? Subscriber::STATUS_NOT_ACTIVE : Subscriber::STATUS_SUBSCRIBED;
        if (!$subscriber->getId()) {
            $subscriber->setSubscriberConfirmCode($subscriber->randomSequence());
            $subscriber->setSubscriberEmail($email);
        }

        $subscriber->setStatus($status)
            ->setStoreId($storeId)
            ->save();
        //$this->sendEmailAfterChangeStatus($subscriber);
        return $subscriber;
    }

    /**
     * @param string $email
     * @param int $storeId
     * @param string $confirmCode
     * @return Subscriber
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function unsubscribe(string $email, int $storeId, string $confirmCode): Subscriber
    {
        $websiteId = (int)$this->_storeManager->getStore($storeId)->getWebsiteId();
        /** @var Subscriber $subscriber */
        $subscriber = $this->_subscriberFactory->create()->loadBySubscriberEmail($email, $websiteId);

        $this->_data->unsubscribeActiveCampaign($email);

        if (!$subscriber->getId()) {
            return $subscriber;
        }

        $subscriber->setCheckCode($confirmCode);
        $subscriber->unsubscribe();
        return $subscriber;
    }

    /**
     * Is need to confirm subscription
     *
     * @param int $storeId
     * @return bool
     */
    protected function isConfirmNeed(int $storeId): bool
    {
        return (bool)$this->_scopeConfig->isSetFlag(
            Subscriber::XML_PATH_CONFIRMATION_FLAG,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Sends out email to customer after change subscription status
     *
     * @param Subscriber $subscriber
     * @return void
     */
    protected function sendEmailAfterChangeStatus(Subscriber $subscriber): void
    {
        $status = (int)$subscriber->getStatus();
        if ($status === Subscriber::STATUS_UNCONFIRMED) {
            return;
        }
        switch ($status) {
            case Subscriber::STATUS_UNSUBSCRIBED:
                $subscriber->sendUnsubscriptionEmail();
                break;
            case Subscriber::STATUS_SUBSCRIBED:
                $subscriber->sendConfirmationSuccessEmail();
                break;
            case Subscriber::STATUS_NOT_ACTIVE:
                $subscriber->sendConfirmationRequestEmail();
                break;
        }
    }
}
