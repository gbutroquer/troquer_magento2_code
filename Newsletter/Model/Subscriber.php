<?php
/** @noinspection PhpDeprecationInspection */
/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedMethodInspection */
/** @noinspection PhpUndefinedClassInspection */

namespace Troquer\Newsletter\Model;

use \Magento\Customer\Api\AccountManagementInterface;
use \Magento\Customer\Api\CustomerRepositoryInterface;
use \Magento\Customer\Api\Data\CustomerInterface;
use \Magento\Customer\Api\Data\CustomerInterfaceFactory;
use \Magento\Customer\Model\Session;
use \Magento\Framework\Api\DataObjectHelper;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Data\Collection\AbstractDb;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Mail\Template\TransportBuilder;
use \Magento\Framework\Model\Context;
use \Magento\Framework\Model\ResourceModel\AbstractResource;
use \Magento\Framework\Registry;
use \Magento\Framework\Stdlib\DateTime\DateTime;
use \Magento\Framework\Translate\Inline\StateInterface;
use \Magento\Newsletter\Helper\Data;
use \Magento\Newsletter\Model\SubscriptionManagerInterface;
use \Magento\Store\Model\StoreManagerInterface;
use \Troquer\Newsletter\Helper\Data as NewsletterData;

class Subscriber extends \Magento\Newsletter\Model\Subscriber
{
    /**
     * @var NewsletterData
     */
    protected NewsletterData $_newsletterTroquerData;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param Data $newsletterData
     * @param ScopeConfigInterface $scopeConfig
     * @param TransportBuilder $transportBuilder
     * @param StoreManagerInterface $storeManager
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $customerAccountManagement
     * @param StateInterface $inlineTranslation
     * @param NewsletterData $newsletterTroquerData
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     * @param DateTime|null $dateTime
     * @param CustomerInterfaceFactory|null $customerFactory
     * @param DataObjectHelper|null $dataObjectHelper
     * @param SubscriptionManagerInterface|null $subscriptionManager
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Data $newsletterData,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        StoreManagerInterface $storeManager,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $customerAccountManagement,
        StateInterface $inlineTranslation,
        NewsletterData $newsletterTroquerData,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = [],
        DateTime $dateTime = null,
        CustomerInterfaceFactory $customerFactory = null,
        DataObjectHelper $dataObjectHelper = null,
        SubscriptionManagerInterface $subscriptionManager = null
    )
    {
        $this->_newsletterTroquerData = $newsletterTroquerData;
        parent::__construct($context, $registry, $newsletterData, $scopeConfig, $transportBuilder, $storeManager, $customerSession, $customerRepository, $customerAccountManagement, $inlineTranslation, $resource, $resourceCollection, $data, $dateTime, $customerFactory, $dataObjectHelper, $subscriptionManager);
    }

    /**
     * @param int $customerId
     * @return Subscriber
     */
    public function subscribeCustomerById($customerId)
    {
        try {
            $customer = $this->getCustomer($customerId);
            $this->_newsletterTroquerData->subscribeActiveCampaign($customer->getEmail(), $customer->getFirstname(), $customer->getLastname());
        } catch (NoSuchEntityException $exception) {
        } catch (LocalizedException $exception) {
        }

        return $this->_updateCustomerSubscription($customerId, true);
    }

    /**
     * @param int $customerId
     * @return Subscriber
     */
    public function unsubscribeCustomerById($customerId)
    {
        try {
            $customer = $this->getCustomer($customerId);
            $this->_newsletterTroquerData->unsubscribeActiveCampaign($customer->getEmail());
        } catch (NoSuchEntityException $exception) {
        } catch (LocalizedException $exception) {
        }

        return $this->_updateCustomerSubscription($customerId, false);
    }

    /**
     * @param $customerId
     * @return CustomerInterface
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function getCustomer($customerId)
    {
        return $this->customerRepository->getById($customerId);
    }
}
