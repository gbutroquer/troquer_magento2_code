<?php
/** @noinspection PhpUndefinedMethodInspection */

namespace Troquer\Newsletter\Helper;

use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\Encryption\EncryptorInterface;
use \Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $_scopeConfig;

    /**
     * @var EncryptorInterface
     */
    protected EncryptorInterface $_encryptor;

    /**
     * Data constructor.
     * @param Context $context
     * @param EncryptorInterface $encryptor
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        EncryptorInterface $encryptor,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->_encryptor = $encryptor;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * @param $email
     * @param $firstname
     * @param $lastname
     */
    public function subscribeActiveCampaign($email, $firstname, $lastname)
    {
        $contact = $this->createOrUpdateContact($email, $firstname, $lastname);
        if ($contact && property_exists($contact, 'contact') && property_exists($contact->contact, 'id')) {
            $this->addContactToList((int)$contact->contact->id);
        }
    }

    /**
     * @param $email
     */
    public function unsubscribeActiveCampaign($email)
    {
        $contact = $this->getContact($email);
        if ($contact && property_exists($contact, 'contacts') && count($contact->contacts) == 1 && property_exists($contact->contacts[0], 'id')) {
            $this->removeContactFromList($email);
        }
    }

    /**
     * @param $email
     * @param $firstname
     * @param $lastname
     * @return mixed
     */
    protected function createOrUpdateContact($email, $firstname, $lastname)
    {
        $requestData = [
            "contact" => [
                "email" => $email
            ]
        ];
        if ($firstname) $requestData["contact"]["firstName"] = $firstname;
        if ($lastname) $requestData["contact"]["lastName"] = $lastname;

        $ch = curl_init("https://troquer.api-us1.com/api/3/contact/sync");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Api-Token: " . $this->getToken()));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @param $email
     * @return mixed
     */
    protected function getContact($email)
    {
        $ch = curl_init("https://troquer.api-us1.com/api/3/contacts/?email=" . $email);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Api-Token: " . $this->getToken()));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @param $contactId
     * @return mixed
     */
    protected function addContactToList($contactId)
    {
        $requestData = [
            "contactList" => [
                "list" => (int)$this->getMasterList(),
                "contact" => $contactId,
                "status" => 1
            ]
        ];

        $ch = curl_init("https://troquer.api-us1.com/api/3/contactLists");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Api-Token: " . $this->getToken()));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    protected function removeContactFromList($contactId)
    {
        $requestData = [
            "contactList" => [
                "list" => (int)$this->getMasterList(),
                "contact" => $contactId,
                "status" => 2
            ]
        ];

        $ch = curl_init("https://troquer.api-us1.com/api/3/contactLists");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestData));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Api-Token: " . $this->getToken()));
        $result = curl_exec($ch);
        return json_decode($result);
    }

    /**
     * @return string
     */
    protected function getToken()
    {
        $token = $this->_scopeConfig->getValue("troquer_newsletter/configuration/token", ScopeInterface::SCOPE_STORE);
        return $this->_encryptor->decrypt($token);
    }

    /**
     * @return mixed
     */
    protected function getMasterList()
    {
        return $this->_scopeConfig->getValue("troquer_newsletter/configuration/list", ScopeInterface::SCOPE_STORE);
    }
}
