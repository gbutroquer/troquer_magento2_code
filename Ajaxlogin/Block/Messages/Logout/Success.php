<?php

namespace Troquer\Ajaxlogin\Block\Messages\Logout;

use \Magento\Framework\View\Element\Template;

class Success extends Template
{
    /**
     * Success constructor.
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, array $data)
    {
        parent::__construct($context, $data);
    }
}
