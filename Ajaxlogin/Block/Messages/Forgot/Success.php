<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Ajaxlogin\Block\Messages\Forgot;

use \Magento\Framework\Registry;
use \Magento\Framework\View\Element\Template;

/**
 * Class Success
 * @package Troquer\Ajaxlogin\Block\Messages\Forgot
 */
class Success extends Template
{
    /**
     * @var Registry
     */
    protected Registry $_coreRegistry;

    /**
     * Success constructor.
     * @param Template\Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(Template\Context $context, Registry $registry, array $data)
    {
        parent::__construct($context, $data);
        $this->_coreRegistry = $registry;
    }

    /**
     * @return mixed
     */
    public function getEmailFromLayout()
    {
        return $this->_coreRegistry->registry('email');
    }
}
