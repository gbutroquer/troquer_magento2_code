<?php

namespace Troquer\Ajaxlogin\Block;

use \Magento\Framework\Data\Form\FormKey;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Troquer\Ajaxlogin\Helper\Data;

class Js extends Template
{
    /**
     * Ajaxsuite helper
     */
    protected Data $_ajaxLoginHelper;

    /**
     * @var FormKey
     */
    protected FormKey $_formKey;

    /**
     * @param Context $context
     * @param FormKey $formKey
     * @param Data $ajaxLoginHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        FormKey $formKey,
        Data $ajaxLoginHelper,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_formKey = $formKey;
        $this->_ajaxLoginHelper = $ajaxLoginHelper;
        $this->_template = 'js/main.phtml';
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    public function getAjaxLoginInitOptions()
    {
        return $this->_ajaxLoginHelper->getAjaxLoginInitOptions();
    }
}
