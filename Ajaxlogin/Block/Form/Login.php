<?php

namespace Troquer\Ajaxlogin\Block\Form;

use \Magento\Customer\Model\Session;
use \Magento\Customer\Model\Url;
use \Magento\Framework\View\Element\Template\Context;
use \Troquer\Ajaxlogin\Helper\Data;

class Login extends \Magento\Customer\Block\Form\Login
{
    /**
     * @var Data
     */
    protected Data $_ajaxloginHelper;

    /**
     * Login constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param Url $customerUrl
     * @param Data $ajaxloginHelper
     * @param array $data
     */
    public function __construct
    (
        Context $context,
        Session $customerSession,
        Url $customerUrl,
        Data $ajaxloginHelper,
        array $data
    )
    {
        parent::__construct($context, $customerSession, $customerUrl, $data);
        $this->_ajaxloginHelper = $ajaxloginHelper;
    }

    /**
     * @return mixed
     */
    public function isEnableSocialLogin()
    {
        return $this->_ajaxloginHelper->getScopeConfig('ajaxlogin/social_login/enable');
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->_ajaxloginHelper->isLoggedIn();
    }

}
