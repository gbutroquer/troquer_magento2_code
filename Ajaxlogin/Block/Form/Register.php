<?php

namespace Troquer\Ajaxlogin\Block\Form;

class Register extends \Magento\Customer\Block\Form\Register
{
    /**
     * Get login URL
     *
     * @return string
     */
    public function getLoginUrl()
    {
        return $this->_customerUrl->getLoginUrl();
    }
}
