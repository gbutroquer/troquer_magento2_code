<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Ajaxlogin\Block\Login;

use \Magento\Framework\Registry;
use \Magento\Framework\View\Element\Template;

class Twitter extends Template
{
    /**
     * @var Registry
     */
    protected Registry $_coreRegistry;

    /**
     * Twitter constructor.
     * @param Template\Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(Template\Context $context, Registry $registry, array $data)
    {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getUrlLgoin()
    {
        return $this->_coreRegistry->registry('url');
    }

    /**
     *
     */
    public function createWindown()
    {

    }
}
