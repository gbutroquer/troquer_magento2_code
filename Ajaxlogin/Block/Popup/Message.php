<?php

namespace Troquer\Ajaxlogin\Block\Popup;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Troquer\Ajaxlogin\Helper\Data;

class Message extends Template
{
    /**
     * @var Data
     */
    protected Data $_ajaxLoginHelper;

    /**
     * Message constructor.
     * @param Context $context
     * @param Data $ajaxLoginHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $ajaxLoginHelper,
        array $data = []
    )
    {
        $this->_ajaxLoginHelper = $ajaxLoginHelper;
        parent::__construct($context, $data);
    }

}
