<?php

namespace Troquer\Ajaxlogin\Block\Popup;

use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Troquer\Ajaxlogin\Helper\Data;

class Login extends Template
{
    /**
     * @var Data
     */
    protected Data $_ajaxLoginHelper;

    /**
     * Login constructor.
     * @param Context $context
     * @param Data $ajaxLoginHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $ajaxLoginHelper,
        array $data = []
    )
    {
        $this->_ajaxLoginHelper = $ajaxLoginHelper;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    public function getHtml()
    {
        return $this->_ajaxLoginHelper->getLoginPopupHtml();
    }

}
