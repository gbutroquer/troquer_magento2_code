<?php

namespace Troquer\Ajaxlogin\Block\Popup;

use \Magento\Customer\Model\Session;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;

class Wrapper extends Template
{
    /**
     * @var Session
     */
    protected Session $_customerSession;

    /**
     * Wrapper constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_customerSession = $customerSession;
    }

    /**
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        return $this->_customerSession->isLoggedIn();
    }

}
