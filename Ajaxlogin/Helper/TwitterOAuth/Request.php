<?php
/** @noinspection PhpUnused */

namespace Troquer\Ajaxlogin\Helper\TwitterOAuth;

class Request
{
    /**
     * @var string
     */
    public static string $_version = '1.0';

    /**
     * @var array
     */
    protected array $_parameters;

    /**
     * @var string
     */
    protected string $_httpMethod;

    /**
     * @var string
     */
    protected string $_httpUrl;

    /**
     * Constructor
     *
     * @param string $httpMethod
     * @param string $httpUrl
     * @param array|null $parameters
     */
    public function __construct(string $httpMethod, string $httpUrl, array $parameters = [])
    {
        $parameters = array_merge(Util::parseParameters(parse_url($httpUrl, PHP_URL_QUERY)), $parameters);
        $this->_parameters = $parameters;
        $this->_httpMethod = $httpMethod;
        $this->_httpUrl = $httpUrl;
    }

    /**
     * pretty much a helper function to set up the request
     *
     * @param Consumer $consumer
     * @param string $httpMethod
     * @param string $httpUrl
     * @param array $parameters
     *
     * @param Token|null $token
     * @return Request
     */
    public static function fromConsumerAndToken(
        Consumer $consumer,
        string $httpMethod,
        string $httpUrl,
        array $parameters = [],
        Token $token = null
    )
    {
        $defaults = [
            "oauth_version" => Request::$_version,
            "oauth_nonce" => Request::generateNonce(),
            "oauth_timestamp" => time(),
            "oauth_consumer_key" => $consumer->_key
        ];
        if (null !== $token) {
            $defaults['oauth_token'] = $token->_key;
        }

        $parameters = array_merge($defaults, $parameters);

        return new Request($httpMethod, $httpUrl, $parameters);
    }

    /**
     * @return string
     */
    public static function generateNonce()
    {
        return md5(microtime() . mt_rand());
    }

    /**
     * @param $name
     *
     * @return string|null
     */
    public function getParameter($name)
    {
        return isset($this->_parameters[$name]) ? $this->_parameters[$name] : null;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->_parameters;
    }

    /**
     * @param $name
     */
    public function removeParameter($name)
    {
        unset($this->_parameters[$name]);
    }

    /**
     * Returns the base string of this request
     *
     * The base string defined as the method, the url
     * and the parameters (normalized), each urlencoded
     * and the concated with &.
     *
     * @return string
     */
    public function getSignatureBaseString()
    {
        $parts = [
            $this->getNormalizedHttpMethod(),
            $this->getNormalizedHttpUrl(),
            $this->getSignableParameters()
        ];

        $parts = Util::urlencodeRfc3986($parts);

        return implode('&', $parts);
    }

    /**
     * Returns the HTTP Method in uppercase
     *
     * @return string
     */
    public function getNormalizedHttpMethod()
    {
        return strtoupper($this->_httpMethod);
    }

    /**
     * parses the url and rebuilds it to be
     * scheme://host/path
     *
     * @return string
     */
    public function getNormalizedHttpUrl()
    {
        $parts = parse_url($this->_httpUrl);

        $scheme = $parts['scheme'];
        $host = strtolower($parts['host']);
        $path = $parts['path'];

        return "$scheme://$host$path";
    }

    /**
     * The request parameters, sorted and concatenated into a normalized string.
     *
     * @return string
     */
    public function getSignableParameters()
    {
        // Grab all parameters
        $params = $this->_parameters;

        // Remove oauth_signature if present
        // Ref: Spec: 9.1.1 ("The oauth_signature parameter MUST be excluded.")
        if (isset($params['oauth_signature'])) {
            unset($params['oauth_signature']);
        }

        return Util::buildHttpQuery($params);
    }

    /**
     * Builds the Authorization: header
     *
     * @return string
     * @throws TwitterOAuthException
     */
    public function toHeader()
    {
        $first = true;
        $out = 'Authorization: OAuth';
        foreach ($this->_parameters as $k => $v) {
            if (mb_substr($k, 0, 5) != "oauth") {
                continue;
            }
            if (is_array($v)) {
                throw new TwitterOAuthException('Arrays not supported in headers');
            }
            $out .= ($first) ? ' ' : ', ';
            $out .= Util::urlencodeRfc3986($k) . '="' . Util::urlencodeRfc3986($v) . '"';
            $first = false;
        }
        return $out;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toUrl();
    }

    /**
     * Builds a url usable for a GET request
     *
     * @return string
     */
    public function toUrl()
    {
        $postData = $this->toPostdata();
        $out = $this->getNormalizedHttpUrl();
        if ($postData) {
            $out .= '?' . $postData;
        }
        return $out;
    }

    /**
     * Builds the data one would send in a POST request
     *
     * @return string
     */
    public function toPostdata()
    {
        return Util::buildHttpQuery($this->_parameters);
    }

    /**
     * @param SignatureMethod $signatureMethod
     * @param Consumer $consumer
     * @param Token|null $token
     */
    public function signRequest(SignatureMethod $signatureMethod, Consumer $consumer, Token $token = null)
    {
        $this->setParameter("oauth_signature_method", $signatureMethod->getName());
        $signature = $this->buildSignature($signatureMethod, $consumer, $token);
        $this->setParameter("oauth_signature", $signature);
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function setParameter(string $name, string $value)
    {
        $this->_parameters[$name] = $value;
    }

    /**
     * @param SignatureMethod $signatureMethod
     * @param Consumer $consumer
     * @param Token|null $token
     *
     * @return string
     */
    public function buildSignature(SignatureMethod $signatureMethod, Consumer $consumer, Token $token = null)
    {
        return $signatureMethod->buildSignature($this, $consumer, $token);
    }
}
