<?php

namespace Troquer\Ajaxlogin\Helper\TwitterOAuth;

class HmacSha1 extends SignatureMethod
{
    /**
     * @return string
     */
    public function getName()
    {
        return "HMAC-SHA1";
    }

    /**
     * @param Request $request
     * @param Consumer $consumer
     * @param Token|null $token
     * @return string
     */
    public function buildSignature(Request $request, Consumer $consumer, Token $token = null)
    {
        $signatureBase = $request->getSignatureBaseString();

        $parts = [$consumer->_secret, null !== $token ? $token->_secret : ""];

        $parts = Util::urlencodeRfc3986($parts);
        $key = implode('&', $parts);

        return base64_encode(hash_hmac('sha1', $signatureBase, $key, true));
    }
}
