<?php

namespace Troquer\Ajaxlogin\Helper\TwitterOAuth;

class Token
{
    /** @var string */
    public string $_key;

    /** @var string */
    public string $_secret;

    /**
     * @param string $key The OAuth Token
     * @param string $secret The OAuth Token Secret
     */
    public function __construct(string $key,string $secret)
    {
        $this->_key = $key;
        $this->_secret = $secret;
    }

    /**
     * Generates the basic string serialization of a token that a server
     * would respond to request_token and access_token calls with
     *
     * @return string
     */
    public function __toString()
    {
        return sprintf("oauth_token=%s&oauth_token_secret=%s",
            Util::urlencodeRfc3986($this->_key),
            Util::urlencodeRfc3986($this->_secret)
        );
    }
}
