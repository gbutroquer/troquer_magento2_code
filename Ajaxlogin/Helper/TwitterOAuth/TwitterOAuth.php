<?php
/** @noinspection PhpUnused */

namespace Troquer\Ajaxlogin\Helper\TwitterOAuth;

use \Phar;
use \Troquer\Ajaxlogin\Helper\TwitterOAuth\Util\JsonDecoder;

class TwitterOAuth extends Config
{
    /**
     *
     */
    const API_VERSION = '1.1';

    /**
     *
     */
    const API_HOST = 'https://api.twitter.com';

    /**
     *
     */
    const UPLOAD_HOST = 'https://upload.twitter.com';

    /**
     *
     */
    const UPLOAD_CHUNK = 40960; // 1024 * 40

    /** @var Response details about the result of the last request */
    protected Response $_response;

    /** @var string|null Application bearer token */
    protected ?string $_bearer;

    /** @var Consumer Twitter application details */
    protected Consumer $_consumer;

    /** @var Token|null User access token details */
    protected ?Token $_token;

    /** @var HmacSha1 OAuth 1 signature type used by Twitter */
    protected HmacSha1 $_signatureMethod;

    /**
     * Constructor
     *
     * @param string $consumerKey The Application Consumer Key
     * @param string $consumerSecret The Application Consumer Secret
     * @param string|null $oauthToken The Client Token (optional)
     * @param string|null $oauthTokenSecret The Client Token Secret (optional)
     */
    public function __construct(string $consumerKey, string $consumerSecret, $oauthToken = null, $oauthTokenSecret = null)
    {
        $this->resetLastResponse();
        $this->_signatureMethod = new HmacSha1();
        $this->_consumer = new Consumer($consumerKey, $consumerSecret);
        if (!empty($oauthToken) && !empty($oauthTokenSecret)) {
            $this->_token = new Token($oauthToken, $oauthTokenSecret);
        }
        if (empty($oauthToken) && !empty($oauthTokenSecret)) {
            $this->_bearer = $oauthTokenSecret;
        }
    }

    /**
     * Resets the last response cache.
     */
    public function resetLastResponse()
    {
        $this->_response = new Response();
    }

    /**
     * @param string $oauthToken
     * @param string $oauthTokenSecret
     */
    public function setOauthToken(string $oauthToken, string $oauthTokenSecret)
    {
        $this->_token = new Token($oauthToken, $oauthTokenSecret);
    }

    /**
     * @return string|null
     */
    public function getLastApiPath()
    {
        return $this->_response->getApiPath();
    }

    /**
     * @return array
     */
    public function getLastXHeaders()
    {
        return $this->_response->getXHeaders();
    }

    /**
     * @return array|object|null
     */
    public function getLastBody()
    {
        return $this->_response->getBody();
    }

    /**
     * Make URLs for user browser navigation.
     *
     * @param string $path
     * @param array $parameters
     *
     * @return string
     */
    public function url(string $path, array $parameters)
    {
        $this->resetLastResponse();
        $this->_response->setApiPath($path);
        $query = http_build_query($parameters);
        return sprintf('%s/%s?%s', self::API_HOST, $path, $query);
    }

    /**
     * Make /oauth/* requests to the API.
     *
     * @param string $path
     * @param array $parameters
     *
     * @return array
     * @throws TwitterOAuthException
     */
    public function oauth(string $path, array $parameters = [])
    {
        $response = [];
        $this->resetLastResponse();
        $this->_response->setApiPath($path);
        $url = sprintf('%s/%s', self::API_HOST, $path);
        $result = $this->oAuthRequest($url, 'POST', $parameters);

        if ($this->getLastHttpCode() != 200) {
            throw new TwitterOAuthException($result);
        }

        parse_str($result, $response);
        $this->_response->setBody($response);

        return $response;
    }

    /**
     * Format and sign an OAuth / API request
     *
     * @param string $url
     * @param string $method
     * @param array $parameters
     *
     * @return string
     * @throws TwitterOAuthException
     */
    protected function oAuthRequest(string $url, string $method, array $parameters)
    {
        $request = Request::fromConsumerAndToken($this->_consumer, $method, $url, $parameters, $this->_token);
        if (array_key_exists('oauth_callback', $parameters)) {
            // Twitter doesn't like oauth_callback as a parameter.
            unset($parameters['oauth_callback']);
        }
        if ($this->_bearer === null) {
            $request->signRequest($this->_signatureMethod, $this->_consumer, $this->_token);
            $authorization = $request->toHeader();
            if (array_key_exists('oauth_verifier', $parameters)) {
                // Twitter doesn't always work with oauth in the body and in the header
                // and it's already included in the $authorization header
                unset($parameters['oauth_verifier']);
            }
        } else {
            $authorization = 'Authorization: Bearer ' . $this->_bearer;
        }
        return $this->request($request->getNormalizedHttpUrl(), $method, $authorization, $parameters);
    }

    /**
     * Make an HTTP request
     *
     * @param string $url
     * @param string $method
     * @param string $authorization
     * @param array $postfields
     *
     * @return string
     * @throws TwitterOAuthException
     */
    protected function request(string $url, string $method, string $authorization, array $postfields)
    {
        /* Curl settings */
        $options = [
            // CURLOPT_VERBOSE => true,
            CURLOPT_CAINFO => __DIR__ . DIRECTORY_SEPARATOR . 'cacert.pem',
            CURLOPT_CONNECTTIMEOUT => $this->_connectionTimeout,
            CURLOPT_HEADER => true,
            CURLOPT_HTTPHEADER => ['Accept: application/json', $authorization, 'Expect:'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_TIMEOUT => $this->_timeout,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => $this->_userAgent,
        ];

        /* Remove CACert file when in a PHAR file. */
        if ($this->pharRunning()) {
            unset($options[CURLOPT_CAINFO]);
        }

        if ($this->_gzipEncoding) {
            $options[CURLOPT_ENCODING] = 'gzip';
        }

        if (!empty($this->_proxy)) {
            $options[CURLOPT_PROXY] = $this->_proxy['CURLOPT_PROXY'];
            $options[CURLOPT_PROXYUSERPWD] = $this->_proxy['CURLOPT_PROXYUSERPWD'];
            $options[CURLOPT_PROXYPORT] = $this->_proxy['CURLOPT_PROXYPORT'];
            $options[CURLOPT_PROXYAUTH] = CURLAUTH_BASIC;
            $options[CURLOPT_PROXYTYPE] = CURLPROXY_HTTP;
        }

        switch ($method) {
            case 'GET':
                break;
            case 'POST':
                $options[CURLOPT_POST] = true;
                $options[CURLOPT_POSTFIELDS] = Util::buildHttpQuery($postfields);
                break;
            case 'DELETE':
                $options[CURLOPT_CUSTOMREQUEST] = 'DELETE';
                break;
            case 'PUT':
                $options[CURLOPT_CUSTOMREQUEST] = 'PUT';
                break;
        }

        if (in_array($method, ['GET', 'PUT', 'DELETE']) && !empty($postfields)) {
            $options[CURLOPT_URL] .= '?' . Util::buildHttpQuery($postfields);
        }


        $curlHandle = curl_init();
        curl_setopt_array($curlHandle, $options);
        $response = curl_exec($curlHandle);

        // Throw exceptions on cURL errors.
        if (curl_errno($curlHandle) > 0) {
            throw new TwitterOAuthException(curl_error($curlHandle), curl_errno($curlHandle));
        }

        $this->_response->setHttpCode(curl_getinfo($curlHandle, CURLINFO_HTTP_CODE));
        $parts = explode("\r\n\r\n", $response);
        $responseBody = array_pop($parts);
        $responseHeader = array_pop($parts);
        $this->_response->setHeaders($this->parseHeaders($responseHeader));

        curl_close($curlHandle);

        return $responseBody;
    }

    /**
     * Is the code running from a Phar module.
     *
     * @return boolean
     */
    protected function pharRunning()
    {
        return class_exists('Phar') && Phar::running(false) !== '';
    }

    /**
     * Get the header info to store.
     *
     * @param string $header
     *
     * @return array
     */
    protected function parseHeaders(string $header)
    {
        $headers = [];
        foreach (explode("\r\n", $header) as $line) {
            if (strpos($line, ':') !== false) {
                list ($key, $value) = explode(': ', $line);
                $key = str_replace('-', '_', strtolower($key));
                $headers[$key] = trim($value);
            }
        }
        return $headers;
    }

    /**
     * @return int
     */
    public function getLastHttpCode()
    {
        return $this->_response->getHttpCode();
    }

    /**
     * Make /oauth2/* requests to the API.
     *
     * @param string $path
     * @param array $parameters
     *
     * @return array|object
     * @throws TwitterOAuthException
     */
    public function oauth2(string $path, array $parameters = [])
    {
        $method = 'POST';
        $this->resetLastResponse();
        $this->_response->setApiPath($path);
        $url = sprintf('%s/%s', self::API_HOST, $path);
        $request = Request::fromConsumerAndToken($this->_consumer, $method, $url, $parameters, $this->_token);
        $authorization = 'Authorization: Basic ' . $this->encodeAppAuthorization($this->_consumer);
        $result = $this->request($request->getNormalizedHttpUrl(), $method, $authorization, $parameters);
        $response = JsonDecoder::decode($result, $this->_decodeJsonAsArray);
        $this->_response->setBody($response);
        return $response;
    }

    /**
     * Encode application authorization header with base64.
     *
     * @param Consumer $consumer
     *
     * @return string
     */
    protected function encodeAppAuthorization(Consumer $consumer)
    {
        $key = rawurlencode($consumer->_key);
        $secret = rawurlencode($consumer->_secret);
        return base64_encode($key . ':' . $secret);
    }

    /**
     * Make GET requests to the API.
     *
     * @param string $path
     * @param array $parameters
     *
     * @return array|object
     * @throws TwitterOAuthException
     */
    public function get(string $path, array $parameters = [])
    {
        return $this->http('GET', self::API_HOST, $path, $parameters);
    }

    /**
     * @param string $method
     * @param string $host
     * @param string $path
     * @param array $parameters
     *
     * @return array|object
     * @throws TwitterOAuthException
     */
    protected function http(string $method, string $host, string $path, array $parameters)
    {
        $this->resetLastResponse();
        $url = sprintf('%s/%s/%s.json', $host, self::API_VERSION, $path);
        $this->_response->setApiPath($path);
        $result = $this->oAuthRequest($url, $method, $parameters);
        $response = JsonDecoder::decode($result, $this->_decodeJsonAsArray);
        $this->_response->setBody($response);
        return $response;
    }

    /**
     * Make POST requests to the API.
     *
     * @param string $path
     * @param array $parameters
     *
     * @return array|object
     * @throws TwitterOAuthException
     */
    public function post(string $path, array $parameters = [])
    {
        return $this->http('POST', self::API_HOST, $path, $parameters);
    }

    /**
     * Make DELETE requests to the API.
     *
     * @param string $path
     * @param array $parameters
     *
     * @return array|object
     * @throws TwitterOAuthException
     */
    public function delete(string $path, array $parameters = [])
    {
        return $this->http('DELETE', self::API_HOST, $path, $parameters);
    }

    /**
     * Make PUT requests to the API.
     *
     * @param string $path
     * @param array $parameters
     *
     * @return array|object
     * @throws TwitterOAuthException
     */
    public function put(string $path, array $parameters = [])
    {
        return $this->http('PUT', self::API_HOST, $path, $parameters);
    }

    /**
     * Upload media to upload.twitter.com.
     *
     * @param string $path
     * @param array $parameters
     * @param boolean $chunked
     *
     * @return array|object
     * @throws TwitterOAuthException
     */
    public function upload(string $path, array $parameters = [], $chunked = false)
    {
        if ($chunked) {
            return $this->uploadMediaChunked($path, $parameters);
        } else {
            return $this->uploadMediaNotChunked($path, $parameters);
        }
    }

    /**
     * Protected method to upload media (chunked) to upload.twitter.com.
     *
     * @param string $path
     * @param array $parameters
     *
     * @return array|object
     * @throws TwitterOAuthException
     */
    protected function uploadMediaChunked(string $path, array $parameters)
    {
        $init = $this->http('POST', self::UPLOAD_HOST, $path, $this->mediaInitParameters($parameters));
        // Append
        $segment_index = 0;
        $media = fopen($parameters['media'], 'rb');
        while (!feof($media)) {
            $this->http('POST', self::UPLOAD_HOST, 'media/upload', [
                'command' => 'APPEND',
                'media_id' => $init->media_id_string,
                'segment_index' => $segment_index++,
                'media_data' => base64_encode(fread($media, self::UPLOAD_CHUNK))
            ]);
        }
        fclose($media);
        // Finalize
        return $this->http('POST', self::UPLOAD_HOST, 'media/upload', [
            'command' => 'FINALIZE',
            'media_id' => $init->media_id_string
        ]);
    }

    /**
     * Protected method to get params for upload media chunked init.
     * Twitter docs: https://dev.twitter.com/rest/reference/post/media/upload-init.html
     *
     * @param array $parameters
     *
     * @return array
     */
    protected function mediaInitParameters(array $parameters)
    {
        $return = [
            'command' => 'INIT',
            'media_type' => $parameters['media_type'],
            'total_bytes' => filesize($parameters['media'])
        ];
        if (isset($parameters['additional_owners'])) {
            $return['additional_owners'] = $parameters['additional_owners'];
        }
        if (isset($parameters['media_category'])) {
            $return['media_category'] = $parameters['media_category'];
        }
        return $return;
    }

    /**
     * Protected method to upload media (not chunked) to upload.twitter.com.
     *
     * @param string $path
     * @param array $parameters
     *
     * @return array|object
     * @throws TwitterOAuthException
     */
    protected function uploadMediaNotChunked(string $path, array $parameters)
    {
        $file = file_get_contents($parameters['media']);
        $base = base64_encode($file);
        $parameters['media'] = $base;
        return $this->http('POST', self::UPLOAD_HOST, $path, $parameters);
    }
}
