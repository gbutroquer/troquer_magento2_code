<?php

namespace Troquer\Ajaxlogin\Helper\TwitterOAuth;

class Consumer
{
    /** @var string */
    public string $_key;

    /** @var string */
    public string $_secret;

    /** @var string|null */
    public ?string $_callbackUrl;

    /**
     * @param string $key
     * @param string $secret
     * @param null $callbackUrl
     */
    public function __construct(string $key, string $secret, $callbackUrl = null)
    {
        $this->_key = $key;
        $this->_secret = $secret;
        $this->_callbackUrl = $callbackUrl;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "Consumer[key=$this->_key,secret=$this->_secret]";
    }
}
