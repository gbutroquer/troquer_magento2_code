<?php

/** @noinspection PhpUnused */

namespace Troquer\Ajaxlogin\Helper\TwitterOAuth;

class Config
{
    /** @var int How long to wait for a response from the API */
    protected int $_timeout = 5;
    /** @var int how long to wait while connecting to the API */
    protected int $_connectionTimeout = 5;
    /**
     * Decode JSON Response as associative Array
     *
     * @see http://php.net/manual/en/function.json-decode.php
     *
     * @var bool
     */
    protected bool $_decodeJsonAsArray = false;
    /** @var string User-Agent header */
    protected string $_userAgent = 'TwitterOAuth (+https://twitteroauth.com)';
    /** @var array Store proxy connection details */
    protected array $_proxy = [];

    /** @var bool Whether to encode the curl requests with gzip or not */
    protected bool $_gzipEncoding = true;

    /**
     * Set the connection and response timeouts.
     *
     * @param int $connectionTimeout
     * @param int $timeout
     */
    public function setTimeouts(int $connectionTimeout, int $timeout)
    {
        $this->_connectionTimeout = (int)$connectionTimeout;
        $this->_timeout = (int)$timeout;
    }

    /**
     * @param bool $value
     */
    public function setDecodeJsonAsArray(bool $value)
    {
        $this->_decodeJsonAsArray = (bool)$value;
    }

    /**
     * @param string $_userAgent
     */
    public function setUserAgent(string $_userAgent)
    {
        $this->_userAgent = (string)$_userAgent;
    }

    /**
     * @param array $_proxy
     */
    public function setProxy(array $_proxy)
    {
        $this->_proxy = $_proxy;
    }

    /**
     * Whether to encode the curl requests with gzip or not.
     *
     * @param boolean $_gzipEncoding
     */
    public function setGzipEncoding(bool $_gzipEncoding)
    {
        $this->_gzipEncoding = (bool)$_gzipEncoding;
    }
}
