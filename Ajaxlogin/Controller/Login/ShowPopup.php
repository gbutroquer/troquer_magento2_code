<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Ajaxlogin\Controller\Login;

use \Exception;
use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\ResultInterface;
use \Troquer\Ajaxlogin\Helper\Data;

class ShowPopup extends Action
{
    /**
     * @var Data
     */
    protected Data $_ajaxLoginHelper;

    /**
     * ShowPopup constructor.
     * @param Context $context
     * @param Data $ajaxLoginHelper
     */
    public function __construct(
        Context $context,
        Data $ajaxLoginHelper
    )
    {
        parent::__construct($context);
        $this->_ajaxLoginHelper = $ajaxLoginHelper;
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $result = [];
        $params = $this->_request->getParams();

        if (!empty($params['isLogin'])) {
            try {
                $htmlPopup = $this->_ajaxLoginHelper->getLoginPopupHtml();
                $result['success'] = true;
                $result['html_popup'] = $htmlPopup;
            } catch (Exception $e) {
                $this->messageManager->addException($e, __('You can\'t login right now.'));
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $result['success'] = false;
            }
        }

        if (!empty($params['isRegister'])) {
            try {
                $htmlPopup = $this->_ajaxLoginHelper->getRegisterPopupHtml();
                $result['success'] = true;
                $result['html_popup'] = $htmlPopup;
            } catch (Exception $e) {
                $this->messageManager->addException($e, __('You can\'t login right now.'));
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $result['success'] = false;
            }
        }

        if (!empty($params['isForgotPassword'])) {
            try {
                $htmlPopup = $this->_ajaxLoginHelper->getForgotPasswordPopupHtml();
                $result['success'] = true;
                $result['html_popup'] = $htmlPopup;
            } catch (Exception $e) {
                $this->messageManager->addException($e, __('You can\'t login right now.'));
                $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
                $result['success'] = false;
            }
        }
        $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
        );
    }
}
