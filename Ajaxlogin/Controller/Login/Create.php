<?php
/** @noinspection PhpDeprecationInspection */
/** @noinspection PhpUndefinedMethodInspection */
/** @noinspection PhpUndefinedClassInspection */

namespace Troquer\Ajaxlogin\Controller\Login;

use \Magento\Customer\Api\AccountManagementInterface;
use \Magento\Customer\Api\Data\AddressInterface;
use \Magento\Customer\Api\Data\AddressInterfaceFactory;
use \Magento\Customer\Api\Data\RegionInterfaceFactory;
use \Magento\Customer\Controller\AbstractAccount;
use \Magento\Customer\Model\CustomerExtractor;
use \Magento\Customer\Model\Metadata\FormFactory;
use \Magento\Customer\Model\Registration;
use \Magento\Customer\Model\Session as CustomerSession;
use \Magento\Customer\Model\Url as CustomerUrl;
use \Magento\Framework\Api\DataObjectHelper;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\ObjectManager;
use \Magento\Framework\Escaper;
use \Magento\Framework\Exception\InputException;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\StateException;
use \Magento\Framework\Json\Helper\Data as JsonHelper;
use \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use \Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use \Magento\Framework\UrlFactory;
use \Magento\Framework\UrlInterface;
use \Magento\Newsletter\Model\SubscriberFactory;
use \Troquer\Ajaxlogin\Helper\Data;

class Create extends AbstractAccount
{
    /**
     * @var AccountManagementInterface
     */
    protected AccountManagementInterface $_accountManagement;

    /**
     * @var FormFactory
     */
    protected FormFactory $_formFactory;

    /**
     * @var SubscriberFactory
     */
    protected SubscriberFactory $_subscriberFactory;

    /**
     * @var RegionInterfaceFactory
     */
    protected RegionInterfaceFactory $_regionDataFactory;

    /**
     * @var AddressInterfaceFactory
     */
    protected AddressInterfaceFactory $_addressDataFactory;

    /**
     * @var Registration
     */
    protected Registration $_registration;

    /**
     * @var CustomerUrl
     */
    protected CustomerUrl $_customerUrl;

    /**
     * @var Escaper
     */
    protected Escaper $_escaper;

    /**
     * @var CustomerExtractor
     */
    protected CustomerExtractor $_customerExtractor;

    /**
     * @var UrlInterface
     */
    protected UrlInterface $_urlModel;

    /**
     * @var DataObjectHelper
     */
    protected DataObjectHelper $_dataObjectHelper;

    /**
     * @var CustomerSession
     */
    protected CustomerSession $_customerSession;

    /**
     * @var JsonHelper
     */
    protected JsonHelper $_jsonHelper;

    /**
     * @var Data
     */
    protected Data $_ajaxLoginHelper;

    /**
     * @var PhpCookieManager|null
     */
    protected ?PhpCookieManager $_cookieMetadataManager = null;

    /**
     * @var CookieMetadataFactory|null
     */
    protected ?CookieMetadataFactory $_cookieMetadataFactory = null;

    /**
     * Create constructor.
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param AccountManagementInterface $accountManagement
     * @param FormFactory $formFactory
     * @param SubscriberFactory $subscriberFactory
     * @param RegionInterfaceFactory $regionDataFactory
     * @param AddressInterfaceFactory $addressDataFactory
     * @param CustomerUrl $customerUrl
     * @param Registration $registration
     * @param Escaper $escaper
     * @param CustomerExtractor $customerExtractor
     * @param UrlFactory $urlFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param JsonHelper $jsonHelper
     * @param Data $ajaxLoginHelper
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        AccountManagementInterface $accountManagement,
        FormFactory $formFactory,
        SubscriberFactory $subscriberFactory,
        RegionInterfaceFactory $regionDataFactory,
        AddressInterfaceFactory $addressDataFactory,
        CustomerUrl $customerUrl,
        Registration $registration,
        Escaper $escaper,
        CustomerExtractor $customerExtractor,
        UrlFactory $urlFactory,
        DataObjectHelper $dataObjectHelper,
        JsonHelper $jsonHelper,
        Data $ajaxLoginHelper
    )
    {
        $this->_customerSession = $customerSession;
        $this->_accountManagement = $accountManagement;
        $this->_formFactory = $formFactory;
        $this->_subscriberFactory = $subscriberFactory;
        $this->_regionDataFactory = $regionDataFactory;
        $this->_addressDataFactory = $addressDataFactory;
        $this->_customerUrl = $customerUrl;
        $this->_registration = $registration;
        $this->_escaper = $escaper;
        $this->_customerExtractor = $customerExtractor;
        $this->_urlModel = $urlFactory->create();
        $this->_dataObjectHelper = $dataObjectHelper;
        $this->_jsonHelper = $jsonHelper;
        $this->_ajaxLoginHelper = $ajaxLoginHelper;
        parent::__construct($context);
    }

    /**
     * Create customer account action
     *
     * @return void
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        $result = array();
        if (!$this->_registration->isAllowed()) {
            $result['error'] = 'Registration is not allow.';
        } else {
            if ($this->_customerSession->isLoggedIn()) {
                $result['error'] = 'You have already logged in.';
            } else {
                $this->_customerSession->regenerateId();
                try {
                    $address = $this->extractAddress();
                    $addresses = $address === null ? [] : [$address];

                    $customer = $this->_customerExtractor->extract('customer_account_create', $this->_request);
                    $customer->setAddresses($addresses);

                    $password = $this->getRequest()->getParam('password');
                    $confirmation = $this->getRequest()->getParam('password_confirmation');

                    if (!$this->checkPasswordConfirmation($password, $confirmation)) {
                        $result['error'] = __('Please make sure your passwords match.');
                    }
                    if (empty($result['error'])) {

                        $customer = $this->_accountManagement
                            ->createAccount($customer, $password);

                        if ($this->getRequest()->getParam('is_subscribed', false)) {
                            $this->_subscriberFactory->create()->subscribeCustomerById($customer->getId());
                        }

                        $this->_eventManager->dispatch(
                            'customer_register_success', ['account_controller' => $this, 'customer' => $customer]
                        );

                        $confirmationStatus = $this->_accountManagement->getConfirmationStatus($customer->getId());
                        if ($confirmationStatus === AccountManagementInterface::ACCOUNT_CONFIRMATION_REQUIRED) {
                            $email = $this->_customerUrl->getEmailConfirmationUrl($customer->getEmail());
                            $result['success'] = __(
                                'You must confirm your account. Please check your email for the confirmation link or <a href="' . $email . '">click here</a> for a new link.'
                            );
                            $result['reload'] = false;
                        } else {
                            $result['success'] = __('You have created account successfully.');
                            $result['reload'] = true;
                            $this->_customerSession->setCustomerDataAsLoggedIn($customer);
                        }
                        if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                            $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                            $metadata->setPath('/');
                            $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
                        }
                    }
                } catch (StateException $e) {
                    $url = $this->_urlModel->getUrl('customer/account/forgotpassword');
                    $result['error'] = __(
                        'There is already an account with this email address. If you are sure that it is your email address, <a href="' . $url . '">click here</a> to get your password and access your account.'
                    );
                } catch (InputException $e) {
                    $result['error'] = $this->_escaper->escapeHtml($e->getMessage());
                } catch (Exception $e) {
                    $result['error'] = $this->_escaper->escapeHtml($e->getMessage());
                }
            }
        }

        if (!empty($result['error'])) {
            $htmlPopup = $this->_ajaxLoginHelper->getErrorMessageRegisterPopupHtml();
            $result['html_popup'] = $htmlPopup;
        } else {
            $htmlPopup = $this->_ajaxLoginHelper->getSuccessMessageRegisterPopupHtml();
            $result['html_popup'] = $htmlPopup;
        }
        $this->getResponse()->representJson($this->_jsonHelper->jsonEncode($result));
    }

    /**
     * Add address to customer during create account
     *
     * @return AddressInterface|null
     */
    protected function extractAddress()
    {
        if (!$this->getRequest()->getPost('create_address')) {
            return null;
        }

        $addressForm = $this->_formFactory->create('customer_address', 'customer_register_address');
        $allowedAttributes = $addressForm->getAllowedAttributes();

        $addressData = [];

        $regionDataObject = $this->_regionDataFactory->create();
        foreach ($allowedAttributes as $attribute) {
            $attributeCode = $attribute->getAttributeCode();
            $value = $this->getRequest()->getParam($attributeCode);
            if ($value === null) {
                continue;
            }
            switch ($attributeCode) {
                case 'region_id':
                    $regionDataObject->setRegionId($value);
                    break;
                case 'region':
                    $regionDataObject->setRegion($value);
                    break;
                default:
                    $addressData[$attributeCode] = $value;
            }
        }
        $addressDataObject = $this->_addressDataFactory->create();
        $this->_dataObjectHelper->populateWithArray(
            $addressDataObject, $addressData, '\Magento\Customer\Api\Data\AddressInterface'
        );
        $addressDataObject->setRegion($regionDataObject);

        $addressDataObject->setIsDefaultBilling(
            $this->getRequest()->getParam('default_billing', false)
        )->setIsDefaultShipping(
            $this->getRequest()->getParam('default_shipping', false)
        );

        return $addressDataObject;
    }

    /**
     * @param $password
     * @param $confirmation
     * @return bool
     */
    protected function checkPasswordConfirmation($password, $confirmation)
    {
        return $password == $confirmation;
    }

    /**
     * @return mixed
     */
    protected function getCookieManager()
    {
        if (!$this->_cookieMetadataManager) {
            $this->_cookieMetadataManager = ObjectManager::getInstance()->get(
                PhpCookieManager::class
            );
        }
        return $this->_cookieMetadataManager;
    }

    /**
     * @return mixed
     */
    protected function getCookieMetadataFactory()
    {
        if (!$this->_cookieMetadataFactory) {
            $this->_cookieMetadataFactory = ObjectManager::getInstance()->get(
                CookieMetadataFactory::class
            );
        }
        return $this->_cookieMetadataFactory;
    }
}
