<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUndefinedMethodInspection */

namespace Troquer\Ajaxlogin\Controller\Login;

use \Exception;
use \Magento\Customer\Api\AccountManagementInterface;
use \Magento\Customer\Model\AccountManagement;
use \Magento\Customer\Model\Session;
use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\Controller\Result\Redirect;
use \Magento\Framework\Escaper;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Json\Helper\Data as JsonHelper;
use \Troquer\Ajaxlogin\Helper\Data;
use \Zend_Validate;
use \Zend_Validate_Exception;

class Forgot extends Action
{
    /**
     * @var AccountManagementInterface
     */
    protected AccountManagementInterface $_customerAccountManagement;

    /**
     * @var Escaper
     */
    protected Escaper $_escaper;

    /**
     * @var Session
     */
    protected Session $_session;

    /**
     * @var JsonHelper
     */
    protected JsonHelper $_jsonHelper;

    /**
     * @var Data
     */
    protected Data $_ajaxLoginHelper;

    /**
     * Forgot constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     * @param Escaper $escaper
     * @param JsonHelper $jsonHelper
     * @param Data $ajaxLoginHelper
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement,
        Escaper $escaper,
        JsonHelper $jsonHelper,
        Data $ajaxLoginHelper
    )
    {
        $this->_session = $customerSession;
        $this->_customerAccountManagement = $customerAccountManagement;
        $this->_escaper = $escaper;
        $this->_jsonHelper = $jsonHelper;
        $this->_ajaxLoginHelper = $ajaxLoginHelper;
        parent::__construct($context);
    }

    /**
     * Forgot customer password action
     *
     * @return void
     * @throws LocalizedException
     * @throws Zend_Validate_Exception
     */
    public function execute()
    {
        $result = array();
        $captchaStatus = $this->_session->getResultCaptcha();
        if ($captchaStatus) {
            if (isset($captchaStatus['error'])) {
                $this->_session->setResultCaptcha(null);
                $this->getResponse()->setBody($this->_jsonHelper->jsonEncode($captchaStatus));
                return;
            }
            $result['imgSrc'] = $captchaStatus['imgSrc'];
        }

        /** @var Redirect $resultRedirect */
        $email = (string)$this->getRequest()->getPost('email');
        if ($email) {
            if (!Zend_Validate::is($email, 'EmailAddress')) {
                $this->_session->setForgottenEmail($email);
                $result['error'] = __('Please correct the email address.');
            }

            try {
                $this->_customerAccountManagement->initiatePasswordReset(
                    $email,
                    AccountManagement::EMAIL_RESET
                );
                $result['success'] = __(
                    'We have sent a message to your email. Please check your inbox and click on the link to reset your password.'
                );
            } catch (Exception $exception) {
                $result['error'] = __('We\'re unable to send the password reset email.');
            }
        }

        if (!empty($result['error'])) {
            $htmlPopup = $this->_ajaxLoginHelper->getErrorMessageForgotPasswordPopupHtml();
            $result['html_popup'] = $htmlPopup;
        } else {
            $htmlPopup = $this->_ajaxLoginHelper->getSuccessMessageForgotPasswordPopupHtml();
            $result['html_popup'] = $htmlPopup;
        }
        $this->getResponse()->representJson($this->_jsonHelper->jsonEncode($result));
    }
}
