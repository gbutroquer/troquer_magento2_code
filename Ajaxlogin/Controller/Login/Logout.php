<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Ajaxlogin\Controller\Login;

use \Exception;
use \Magento\Customer\Model\Session as CustomerSession;
use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\ObjectManager;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Json\Helper\Data as JsonHelper;
use \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use \Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use \Troquer\Ajaxlogin\Helper\Data;

class Logout extends Action
{
    /**
     * @var CustomerSession
     */
    protected CustomerSession $_customerSession;

    /**
     * @var JsonHelper
     */
    protected JsonHelper $_jsonHelper;

    /**
     * @var Data
     */
    protected Data $_ajaxLoginHelper;

    /**
     * @var PhpCookieManager|null
     */
    protected ?PhpCookieManager $cookieMetadataManager = null;

    /**
     * @var CookieMetadataFactory|null
     */
    protected ?CookieMetadataFactory $cookieMetadataFactory = null;

    /**
     * Logout constructor.
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param JsonHelper $jsonHelper
     * @param Data $ajaxloginHelper
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        JsonHelper $jsonHelper,
        Data $ajaxloginHelper
    )
    {
        $this->_ajaxLoginHelper = $ajaxloginHelper;
        parent::__construct($context);
        $this->_customerSession = $customerSession;
        $this->_jsonHelper = $jsonHelper;
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     * @throws LocalizedException
     */
    public function execute()
    {
        $result = [];
        try {
            $this->_customerSession->logout();
            if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                $metadata->setPath('/');
                $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
            }
            $result['success'] = __('You have signed out and will go to our homepage. Please wait ...');
        } catch (Exception $e) {
            $result['error'] = $e->getMessage();
        }

        if (!empty($result['error'])) {
            $htmlPopup = $this->_ajaxLoginHelper->getErrorMessageLogoutPopupHtml();
            $result['html_popup'] = $htmlPopup;
        } else {
            $htmlPopup = $this->_ajaxLoginHelper->getSuccessMessageLogoutPopupHtml();
            $result['html_popup'] = $htmlPopup;
        }
        $this->getResponse()->representJson($this->_jsonHelper->jsonEncode($result));
    }

    /**
     * Retrieve cookie manager
     *
     * @return PhpCookieManager
     * @deprecated
     */
    protected function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = ObjectManager::getInstance()->get(
                PhpCookieManager::class
            );
        }
        return $this->cookieMetadataManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @return CookieMetadataFactory
     * @deprecated
     */
    protected function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = ObjectManager::getInstance()->get(
                CookieMetadataFactory::class
            );
        }
        return $this->cookieMetadataFactory;
    }

}
