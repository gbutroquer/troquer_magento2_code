<?php
/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUnused */

namespace Troquer\Ajaxlogin\Controller\Login;

use \Exception;
use \Magento\Customer\Api\AccountManagementInterface;
use \Magento\Customer\Model\Session as CustomerSession;
use \Magento\Customer\Model\Url;
use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\App\ObjectManager;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Exception\AuthenticationException;
use \Magento\Framework\Exception\EmailNotConfirmedException;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\State\UserLockedException;
use \Magento\Framework\Json\Helper\Data as JsonHelper;
use \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use \Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use \Troquer\Ajaxlogin\Helper\Data;

class Login extends Action
{
    /**
     * @var AccountManagementInterface
     */
    protected AccountManagementInterface $_accountManagement;

    /**
     * @var CustomerSession
     */
    protected CustomerSession $_customerSession;

    /**
     * @var JsonHelper
     */
    protected JsonHelper $_jsonHelper;

    /**
     * @var Data
     */
    protected Data $_ajaxLoginHelper;

    /**
     * @var PhpCookieManager|null
     */
    protected ?PhpCookieManager $_cookieMetadataManager = null;

    /**
     * @var CookieMetadataFactory|null
     */
    protected ?CookieMetadataFactory $_cookieMetadataFactory = null;

    /**
     * @var ScopeConfigInterface|null
     */
    protected ?ScopeConfigInterface $_scopeConfig = null;

    /**
     * @var Url
     */
    protected Url $_customerUrl;

    /**
     * Login constructor.
     * @param Context $context
     * @param AccountManagementInterface $accountManagement
     * @param CustomerSession $customerSession
     * @param JsonHelper $jsonHelper
     * @param Data $ajaxloginHelper
     * @param Url $customerUrl
     */
    public function __construct(
        Context $context,
        AccountManagementInterface $accountManagement,
        CustomerSession $customerSession,
        JsonHelper $jsonHelper,
        Data $ajaxloginHelper,
        Url $customerUrl
    )
    {
        $this->_ajaxLoginHelper = $ajaxloginHelper;
        parent::__construct($context);
        $this->_accountManagement = $accountManagement;
        $this->_customerSession = $customerSession;
        $this->_jsonHelper = $jsonHelper;
        $this->_customerUrl = $customerUrl;
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     * @throws LocalizedException
     */
    public function execute()
    {
        $result = array();
        if ($this->_customerSession->isLoggedIn()) {
            $result['error'] = __('You have already logged in.');
        } else {
            $login = $this->getRequest()->getPost('login');
            if (!empty($login['username']) && !empty($login['password'])) {
                try {
                    $customer = $this->_accountManagement->authenticate($login['username'], $login['password']);
                    $this->_customerSession->setCustomerDataAsLoggedIn($customer);
                    $this->_customerSession->regenerateId();
                    $result['success'] = __('You have logged in successfully. Please wait ...');
                    if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                        $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                        $metadata->setPath('/');
                        $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
                    }
                } catch (LocalizedException $e) {
                    $result['error'] = $e->getMessage();
                } catch (Exception $e) {
                    $result['error'] = $e->getMessage();
                }
            } else {
                $result['error'] = 'A login and a password are required.';
            }
        }

        if (!empty($result['error'])) {
            $htmlPopup = $this->_ajaxLoginHelper->getErrorMessageLoginPopupHtml();
            $result['html_popup'] = $htmlPopup;
        } else {
            $htmlPopup = $this->_ajaxLoginHelper->getSuccessMessageLoginPopupHtml();
            $result['html_popup'] = $htmlPopup;
        }
        $this->getResponse()->representJson($this->_jsonHelper->jsonEncode($result));
    }

    /**
     * Retrieve cookie manager
     *
     * @return PhpCookieManager
     * @deprecated
     */
    protected function getCookieManager()
    {
        if (!$this->_cookieMetadataManager) {
            $this->_cookieMetadataManager = ObjectManager::getInstance()->get(
                PhpCookieManager::class
            );
        }
        return $this->_cookieMetadataManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @return CookieMetadataFactory
     * @deprecated
     */
    protected function getCookieMetadataFactory()
    {
        if (!$this->_cookieMetadataFactory) {
            $this->_cookieMetadataFactory = ObjectManager::getInstance()->get(
                CookieMetadataFactory::class
            );
        }
        return $this->_cookieMetadataFactory;
    }

    /**
     * Get scope config
     *
     * @return ScopeConfigInterface
     * @deprecated
     */
    protected function getScopeConfig()
    {
        if (!($this->_scopeConfig instanceof ScopeConfigInterface)) {
            return ObjectManager::getInstance()->get(
                ScopeConfigInterface::class
            );
        } else {
            return $this->_scopeConfig;
        }
    }

}
