<?php
/** @noinspection PhpDeprecationInspection */

namespace Troquer\Ajaxlogin\Controller\Login;

use \Magento\Customer\Model\Registration;
use \Magento\Customer\Model\Session;
use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Framework\Controller\ResultInterface;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Store\Model\StoreManagerInterface;
use \Troquer\Ajaxlogin\Helper\Data;
use \Troquer\Ajaxlogin\Helper\TwitterOAuth\TwitterOAuth;
use \Troquer\Ajaxlogin\Helper\TwitterOAuth\TwitterOAuthException;

class Callback extends Action
{
    /**
     * @var Data
     */
    protected Data $_ajaxLoginHelper;

    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $_storeManager;

    /**
     * @var Registration
     */
    protected Registration $_registration;

    /**
     * @var Session
     */
    protected Session $_customerSession;

    /**
     * Callback constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param Registration $registration
     * @param Session $customerSession
     * @param Data $ajaxLoginHelper
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Registration $registration,
        Session $customerSession,
        Data $ajaxLoginHelper
    )
    {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->_registration = $registration;
        $this->_customerSession = $customerSession;
        $this->_ajaxLoginHelper = $ajaxLoginHelper;
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws TwitterOAuthException
     */
    public function execute()
    {
        $consumerKey = $this->_ajaxLoginHelper->getTwitterConsumerKey();
        $consumerSecret = $this->_ajaxLoginHelper->getTwitterConsumerSecret();
        $loginUrl = $this->_ajaxLoginHelper->getTwitterLoginUrl();

        // get and filter oauth verifier
        $oauth_verifier = filter_input(INPUT_GET, 'oauth_verifier');

        // check tokens
        if (empty($oauth_verifier) ||
            empty($_SESSION['oauth_token']) ||
            empty($_SESSION['oauth_token_secret'])
        ) {
            // something's missing, go and login again
            header('Location: ' . $loginUrl);
        }

        // connect with application token
        $connection = new TwitterOAuth(
            $consumerKey,
            $consumerSecret,
            $_SESSION['oauth_token'],
            $_SESSION['oauth_token_secret']
        );

        // request user token
        $token = $connection->oauth(
            'oauth/access_token', [
                'oauth_verifier' => $oauth_verifier
            ]
        );

        // connect with user token
        $twitter = new TwitterOAuth(
            $consumerKey,
            $consumerSecret,
            $token['oauth_token'],
            $token['oauth_token_secret']
        );

        $params = array('include_email' => 'true', 'include_entities' => 'false', 'skip_status' => 'true');

        $user = $twitter->get('account/verify_credentials', $params);
        $tmp = json_decode(json_encode($user), true);
        $params = [
            'firstname' => $tmp['name'],
            'lastname' => $tmp['screen_name'],
            'email' => $tmp['email'],
            'social_type' => 'twitter',
            'password' => 'Troquer123!'
        ];

        $result = array();
        if (!$this->_registration->isAllowed()) {
            $result['error'] = __('Registration is not allow.');
        } else {
            if ($this->_customerSession->isLoggedIn()) {
                $result['error'] = __('You have already logged in.');
            } else {
                $this->_customerSession->regenerateId();
                $socialType = $params['social_type'];
                if ($params) {
                    $storeId = $this->_storeManager->getStore()->getStoreId();
                    $websiteId = $this->_storeManager->getStore()->getWebsiteId();
                    $data = array(
                        'firstname' => $params['firstname'],
                        'lastname' => $params['lastname'],
                        'email' => $params['email'],
                        'password' => $params['password']
                    );
                    if ($data['email']) {
                        $customer = $this->_ajaxLoginHelper->getCustomerByEmail($data['email'], $websiteId);
                        if (!$customer || !$customer->getId()) {
                            $customer = $this->_ajaxLoginHelper->createCustomerMultiWebsite($data, $websiteId,
                                $storeId);
                            if ($this->_ajaxLoginHelper->getScopeConfig('ajaxlogin/social_login/send_pass')) {
                                $customer->sendPasswordReminderEmail();
                            }
                        }
                        $this->_customerSession->setCustomerAsLoggedIn($customer);
                    } else {
                        $result['error'] = __('Something wrong with getting your email of your ') . $socialType;
                    }
                } else {
                    $result['error'] = __('Something wrong when processing Ajax.');
                }
            }
        }

        if (!empty($result['error'])) {
            echo $result['error'];
        } else {
            echo __('You have logged in with twitter succesfully.');
            $this->redirect();
        }
    }

    /**
     * @throws NoSuchEntityException
     */
    public function redirect()
    {
        echo "<script type='text/javascript'>";
        echo "window.close();";
        $urlRedirect = $this->_ajaxLoginHelper->getScopeConfig('ajaxlogin/general/login_destination');
        $baseUrl = $this->_ajaxLoginHelper->getBaseUrl();
        $customerPageUrl = $baseUrl . 'customer/account';
        $cartUrl = $baseUrl . 'checkout/cart';
        $wishlistUrl = $baseUrl . 'wishlist';
        if ($urlRedirect == 0) {
            echo "window.opener.location.reload();";
        } else {
            if ($urlRedirect == 1) {
                echo "window.opener.location.replace('" . $customerPageUrl . "');";
            } else {
                if ($urlRedirect == 2) {
                    echo "window.opener.location.replace('" . $baseUrl . "');";
                } else {
                    if ($urlRedirect == 3) {
                        echo "window.opener.location.replace('" . $cartUrl . "');";
                    } else {
                        if ($urlRedirect == 4) {
                            echo "window.opener.location.replace('" . $wishlistUrl . "');";
                        } else {
                            echo "window.opener.location.replace('" . $customerPageUrl . "');";
                        }
                    }
                }
            }
        }
        echo "</script>";
    }
}
