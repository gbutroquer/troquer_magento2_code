<?php

namespace Troquer\Queue\Plugin\Catalog\Product;

use \Troquer\Queue\Model\Catalog\Product\DeletePublisher;
use \Magento\Catalog\Model\ResourceModel\Product;
use \Magento\Catalog\Model\Product as ProductModel;

class DeletePlugin
{
    /**
     * @var DeletePublisher
     */
    protected DeletePublisher $_deletePublisher;

    /**
     * @param DeletePublisher $deletePublisher
     */
    public function __construct(DeletePublisher $deletePublisher)
    {
        $this->_deletePublisher = $deletePublisher;
    }

    /**
     * @param Product $subject
     * @param Product $result
     * @param ProductModel $product
     * @return Product
     */
    public function afterDelete(/** @noinspection PhpUnusedParameterInspection */ Product $subject, Product $result, ProductModel $product)
    {
        $this->_deletePublisher->execute($product);
        return $result;
    }
}
