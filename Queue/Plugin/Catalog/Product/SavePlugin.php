<?php

namespace Troquer\Queue\Plugin\Catalog\Product;

use \Troquer\Queue\Model\Catalog\Product\CreatePublisher;
use \Magento\Catalog\Model\ResourceModel\Product;
use \Magento\Catalog\Model\Product as ProductModel;
use \Troquer\Queue\Model\Catalog\Product\UpdatePublisher;

class SavePlugin
{
    /**
     * @var CreatePublisher
     */
    protected CreatePublisher $_createPublisher;

    /**
     * @var UpdatePublisher
     */
    protected UpdatePublisher $_updatePublisher;

    /**
     * @var bool
     */
    protected bool $_isNew = false;

    /**
     * SavePlugin constructor.
     * @param CreatePublisher $createPublisher
     * @param UpdatePublisher $updatePublisher
     */
    public function __construct(CreatePublisher $createPublisher, UpdatePublisher $updatePublisher)
    {
        $this->_createPublisher = $createPublisher;
        $this->_updatePublisher = $updatePublisher;
    }

    /**
     * @param Product $subject
     * @param ProductModel $product
     */
    public function beforeSave(/** @noinspection PhpUnusedParameterInspection */ Product $subject, ProductModel $product)
    {
        $this->_isNew = $product->isObjectNew();
    }

    /**
     * @param Product $subject
     * @param Product $result
     * @param ProductModel $product
     * @return Product
     */
    public function afterSave(/** @noinspection PhpUnusedParameterInspection */ Product $subject, Product $result, ProductModel $product)
    {
        if ($this->_isNew) {
            $this->_createPublisher->execute($product);
        } else {
            $this->_updatePublisher->execute($product);
        }
        return $result;
    }
}
