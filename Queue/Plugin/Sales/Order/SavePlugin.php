<?php

namespace Troquer\Queue\Plugin\Sales\Order;

use \Troquer\Queue\Model\Sales\Order\SavePublisher;
use \Magento\Sales\Model\Order as OrderModel;
use \Magento\Sales\Model\ResourceModel\Order;

class SavePlugin
{
    /**
     * @var SavePublisher
     */
    protected SavePublisher $_savePublisher;

    /**
     * @var bool
     */
    protected bool $_isNew = false;

    /**
     * @param SavePublisher $savePublisher
     */
    public function __construct(SavePublisher $savePublisher)
    {
        $this->_savePublisher = $savePublisher;
    }

    /**
     * @param Order $subject
     * @param Order $result
     * @param OrderModel $order
     * @return Order
     */
    public function afterSave(/** @noinspection PhpUnusedParameterInspection */ Order $subject, Order $result, OrderModel $order)
    {
        $this->_savePublisher->execute($order);
        return $result;
    }
}
