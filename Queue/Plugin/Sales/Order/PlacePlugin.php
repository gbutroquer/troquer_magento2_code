<?php

namespace Troquer\Queue\Plugin\Sales\Order;

use \Troquer\Queue\Model\Sales\Order\PlacePublisher;
use \Magento\Sales\Model\Order;

class PlacePlugin
{
    /**
     * @var PlacePublisher
     */
    protected PlacePublisher $_placePublisher;

    /**
     * @param PlacePublisher $placePublisher
     */
    public function __construct(PlacePublisher $placePublisher)
    {
        $this->_placePublisher = $placePublisher;
    }

    /**
     * @param Order $subject
     * @param Order $result
     * @return Order
     */
    public function afterPlace(Order $subject, Order $result): Order
    {
        $this->_placePublisher->execute($result);
        return $subject;
    }
}
