<?php

namespace Troquer\Queue\Plugin\Customer;

use \Magento\Customer\Model\Customer as CustomerModel;
use \Troquer\Queue\Model\Customer\DeletePublisher;
use \Magento\Customer\Model\Customer;

class DeletePlugin
{
    /**
     * @var DeletePublisher
     */
    protected DeletePublisher $deletePublisher;

    /**
     * @param DeletePublisher $deletePublisher
     */
    public function __construct(DeletePublisher $deletePublisher)
    {
        $this->deletePublisher = $deletePublisher;
    }

    /**
     * @param Customer $subject
     * @param Customer $result
     * @param CustomerModel $customer
     * @return Customer
     */
    public function afterDelete(/** @noinspection PhpUnusedParameterInspection */ Customer $subject, Customer $result, CustomerModel $customer)
    {
        $this->deletePublisher->execute($customer);
        return $result;
    }
}
