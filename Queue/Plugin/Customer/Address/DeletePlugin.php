<?php

namespace Troquer\Queue\Plugin\Customer\Address;

use Magento\Customer\Model\Address as AddressModel;
use \Troquer\Queue\Model\Customer\Address\DeletePublisher;
use \Magento\Customer\Model\Address;

class DeletePlugin
{
    /**
     * @var DeletePublisher
     */
    protected DeletePublisher $_deletePublisher;

    /**
     * @param DeletePublisher $deletePublisher
     */
    public function __construct(DeletePublisher $deletePublisher)
    {
        $this->_deletePublisher = $deletePublisher;
    }

    /**
     * @param Address $subject
     * @param Address $result
     * @param AddressModel $address
     * @return Address
     */
    public function afterAfterDelete(/** @noinspection PhpUnusedParameterInspection */ Address $subject, Address $result, AddressModel $address)
    {
        $this->_deletePublisher->execute($address);
        return $result;
    }
}
