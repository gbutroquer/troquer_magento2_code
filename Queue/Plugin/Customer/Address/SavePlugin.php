<?php

namespace Troquer\Queue\Plugin\Customer\Address;

use \Troquer\Queue\Model\Customer\Address\CreatePublisher;
use \Troquer\Queue\Model\Customer\Address\UpdatePublisher;
use \Magento\Customer\Model\Address as AddressModel;
use \Magento\Customer\Model\ResourceModel\Address;

class SavePlugin
{
    /**
     * @var CreatePublisher
     */
    protected CreatePublisher $_createPublisher;

    /**
     * @var UpdatePublisher
     */
    protected UpdatePublisher $_updatePublisher;

    /**
     * @var bool
     */
    protected bool $isNew = false;

    /**
     * @param CreatePublisher $createPublisher
     * @param UpdatePublisher $updatePublisher
     */
    public function __construct(CreatePublisher $createPublisher, UpdatePublisher $updatePublisher)
    {
        $this->_createPublisher = $createPublisher;
        $this->_updatePublisher = $updatePublisher;
    }

    /**
     * @param Address $subject
     * @param AddressModel $address
     */
    public function beforeSave(/** @noinspection PhpUnusedParameterInspection */ Address $subject, AddressModel $address)
    {
        $this->isNew = $address->isObjectNew();
    }

    /**
     * @param Address $subject
     * @param Address $result
     * @param AddressModel $address
     * @return Address
     */
    public function afterSave(/** @noinspection PhpUnusedParameterInspection */ Address $subject, Address $result, AddressModel $address)
    {
        if ($this->isNew) {
            $this->_createPublisher->execute($address);
        } else {
            $this->_updatePublisher->execute($address);
        }
        return $result;
    }
}
