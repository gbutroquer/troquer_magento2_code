<?php
/** @noinspection PhpUnused */

namespace Troquer\Queue\Plugin\Customer;

use \Troquer\Queue\Model\Customer\CreatePublisher;
use \Troquer\Queue\Model\Customer\UpdatePublisher;
use \Magento\Customer\Model\Customer as CustomerModel;
use \Magento\Customer\Model\ResourceModel\Customer;
use \Troquer\Inbound\Model\Notification;
use Zend_Http_Client_Exception;

class SavePlugin
{
    /**
     * @var CreatePublisher
     */
    protected CreatePublisher $_createPublisher;

    /**
     * @var UpdatePublisher
     */
    protected UpdatePublisher $_updatePublisher;

    /**
     * @var bool
     */
    protected bool $_isNew = false;

    /**
     * @var Notification
     */
    protected Notification $_notification;

    /**
     * @param CreatePublisher $createPublisher
     * @param UpdatePublisher $updatePublisher
     * @param Notification $notification
     */
    public function __construct(CreatePublisher $createPublisher, UpdatePublisher $updatePublisher, Notification $notification)
    {
        $this->_createPublisher = $createPublisher;
        $this->_updatePublisher = $updatePublisher;
        $this->_notification = $notification;
    }

    /**
     * @param Customer $subject
     * @param CustomerModel $customer
     */
    public function beforeSave(/** @noinspection PhpUnusedParameterInspection */ Customer $subject, CustomerModel $customer)
    {
        $this->_isNew = $customer->isObjectNew();
    }

    /**
     * @param Customer $subject
     * @param Customer $result
     * @param CustomerModel $customer
     * @return Customer
     * @throws Zend_Http_Client_Exception
     */
    public function afterSave(/** @noinspection PhpUnusedParameterInspection */ Customer $subject, Customer $result, CustomerModel $customer): Customer
    {
        if ($this->_isNew) {
            $this->_notification->send($customer);
            $this->_createPublisher->execute($customer);
        } else {
            $this->_updatePublisher->execute($customer);
        }
        return $result;
    }
}
