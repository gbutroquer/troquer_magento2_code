<?php

namespace Troquer\Queue\Model\Sales\Order;

use \Troquer\Queue\MessageQueue\PublisherInterface;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Sales\Model\Order;

class PlacePublisher
{
    const TOPIC_NAME = 'sales.order.place';

    /**
     * @var PublisherInterface
     */
    protected PublisherInterface $_publisher;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @param PublisherInterface $publisher
     * @param Json $json
     */
    public function __construct(PublisherInterface $publisher, Json $json)
    {
        $this->_json = $json;
        $this->_publisher = $publisher;
    }

    /**
     * @param Order $order
     */
    public function execute(Order $order)
    {
        $orderData = $order->getData();
        $orderData["items"] = [];
        foreach ($order->getAllItems() as $item) {
            $orderData["items"][] = $item->getData();
        }
        $orderData["payment"] = $order->getPayment()->getData();
        $orderData["status_histories"] = [];
        foreach ($order->getStatusHistories() as $statusHistory) {
            $orderData["status_histories"][] = $statusHistory->getData();
        }
        $orderData["addresses"] = [];
        foreach ($order->getAddresses() as $address) {
            $orderData["addresses"][] = $address->getData();
        }
        $orderData["extension_attributes"] = [];
        foreach ($order->getExtensionAttributes() as $extensionAttribute) {
            $orderData["extension_attributes"][] = $extensionAttribute->getData();
        }
        $this->_publisher->publish(self::TOPIC_NAME, $this->_json->serialize($orderData));
    }
}
