<?php

namespace Troquer\Queue\Model\Catalog\Product;

use \Troquer\Queue\MessageQueue\PublisherInterface;
use \Magento\Catalog\Model\Product;
use \Magento\Framework\Serialize\Serializer\Json;

class CreatePublisher
{
    const TOPIC_NAME = 'catalog.product.create';

    /**
     * @var PublisherInterface
     */
    protected PublisherInterface $_publisher;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @param PublisherInterface $publisher
     * @param Json $json
     */
    public function __construct(PublisherInterface $publisher, Json $json)
    {
        $this->_json = $json;
        $this->_publisher = $publisher;
    }

    /**
     * @param Product $product
     */
    public function execute(Product $product)
    {
        $this->_publisher->publish(self::TOPIC_NAME, $this->_json->serialize($product->getData()));
    }
}
