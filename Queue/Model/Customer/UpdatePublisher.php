<?php

namespace Troquer\Queue\Model\Customer;

use \Troquer\Queue\MessageQueue\PublisherInterface;
use \Magento\Framework\Serialize\Serializer\Json;
use \Magento\Customer\Model\Customer;

class UpdatePublisher
{
    const TOPIC_NAME = 'customer.update';

    /**
     * @var PublisherInterface
     */
    protected PublisherInterface $_publisher;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @param PublisherInterface $publisher
     * @param Json $json
     */
    public function __construct(PublisherInterface $publisher, Json $json)
    {
        $this->_json = $json;
        $this->_publisher = $publisher;
    }

    /**
     * @param Customer $customer
     */
    public function execute(Customer $customer)
    {
        $this->_publisher->publish(self::TOPIC_NAME, $this->_json->serialize($customer->getData()));
    }
}
