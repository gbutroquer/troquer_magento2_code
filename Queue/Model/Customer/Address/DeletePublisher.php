<?php

namespace Troquer\Queue\Model\Customer\Address;

use \Magento\Framework\Serialize\Serializer\Json;
use \Troquer\Queue\MessageQueue\PublisherInterface;
use \Magento\Customer\Model\Address;

class DeletePublisher
{
    const TOPIC_NAME = 'customer.address.delete';

    /**
     * @var PublisherInterface
     */
    protected PublisherInterface $_publisher;

    /**
     * @var Json
     */
    protected Json $_json;

    /**
     * @param PublisherInterface $publisher
     * @param Json $json
     */
    public function __construct(PublisherInterface $publisher, Json $json)
    {
        $this->_json = $json;
        $this->_publisher = $publisher;
    }

    /**
     * @param Address $address
     */
    public function execute(Address $address)
    {
        $this->_publisher->publish(self::TOPIC_NAME, $this->_json->serialize($address->getData()));
    }
}
