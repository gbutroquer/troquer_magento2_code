<?php

namespace Troquer\Queue\MessageQueue;

interface PublisherInterface
{
    /**
     * @param string $topicName
     * @param string $data
     * @return mixed
     */
    public function publish(string $topicName, string $data);
}
