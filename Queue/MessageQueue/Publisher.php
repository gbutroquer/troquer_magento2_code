<?php

namespace Troquer\Queue\MessageQueue;

use \Magento\Framework\Amqp\Config as AmqpConfig;
use \Magento\Framework\App\ObjectManager;
use \Magento\Framework\Exception\LocalizedException;
use \Magento\Framework\MessageQueue\EnvelopeFactory;
use \Magento\Framework\MessageQueue\ExchangeRepository;
use \Magento\Framework\MessageQueue\MessageEncoder;
use \Magento\Framework\MessageQueue\MessageValidator;
use \Magento\Framework\MessageQueue\Publisher\ConfigInterface as PublisherConfig;

class Publisher implements PublisherInterface
{
    /**
     * @var ExchangeRepository
     */
    protected ExchangeRepository $exchangeRepository;

    /**
     * @var EnvelopeFactory
     */
    protected EnvelopeFactory $envelopeFactory;

    /**
     * @var MessageEncoder
     */
    protected MessageEncoder $messageEncoder;

    /**
     * @var MessageValidator
     */
    protected MessageValidator $messageValidator;

    /**
     * @var PublisherConfig|null
     */
    protected ?PublisherConfig $publisherConfig = null;

    /**
     * Help check whether Amqp is configured.
     *
     * @var AmqpConfig|null
     */
    protected ?AmqpConfig $amqpConfig = null;

    /**
     * Publisher constructor.
     * @param ExchangeRepository $exchangeRepository
     * @param EnvelopeFactory $envelopeFactory
     * @param MessageEncoder $messageEncoder
     * @param MessageValidator $messageValidator
     */
    public function __construct(
        ExchangeRepository $exchangeRepository,
        EnvelopeFactory $envelopeFactory,
        MessageEncoder $messageEncoder,
        MessageValidator $messageValidator
    ) {
        $this->exchangeRepository = $exchangeRepository;
        $this->envelopeFactory = $envelopeFactory;
        $this->messageEncoder = $messageEncoder;
        $this->messageValidator = $messageValidator;
    }

    /**
     * @param string $topicName
     * @param string $data
     * @return mixed|null
     * @throws LocalizedException
     */
    public function publish(string $topicName, string $data)
    {
        $this->messageValidator->validate($topicName, $data);
        $data = $this->messageEncoder->encode($topicName, $data);
        $envelope = $this->envelopeFactory->create(
            [
                'body' => $data,
                'properties' => [
                    'delivery_mode' => 2,
                    'message_id' => md5(uniqid($topicName))
                ]
            ]
        );
        $connectionName = $this->getPublisherConfig()->getPublisher($topicName)->getConnection()->getName();
        $connectionName = ($connectionName === 'amqp' && !$this->isAmqpConfigured()) ? 'db' : $connectionName;
        $exchange = $this->exchangeRepository->getByConnectionName($connectionName);
        $exchange->enqueue($topicName, $envelope);
        return null;
    }

    /**
     * @return bool
     */
    protected function isAmqpConfigured()
    {
        return $this->getAmqpConfig()->getValue(AmqpConfig::HOST) ? true : false;
    }

    /**
     * @return PublisherConfig|mixed
     */
    protected function getPublisherConfig()
    {
        if ($this->publisherConfig === null) {
            $this->publisherConfig = ObjectManager::getInstance()->get(PublisherConfig::class);
        }
        return $this->publisherConfig;
    }

    /**
     * @return AmqpConfig|mixed
     */
    protected function getAmqpConfig()
    {
        if ($this->amqpConfig === null) {
            $this->amqpConfig = ObjectManager::getInstance()->get(AmqpConfig::class);
        }

        return $this->amqpConfig;
    }
}
